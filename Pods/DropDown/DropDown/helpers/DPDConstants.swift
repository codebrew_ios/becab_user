//
//  Constants.swift
//  DropDown
//
//  Created by Kevin Hirsch on 28/07/15.
//  Copyright (c) 2015 Kevin Hirsch. All rights reserved.
//

import UIKit

internal struct DPDConstant {

	internal struct KeyPath {

		static let Frame = "frame"

	}

	internal struct ReusableIdentifier {
		static let DropDownCell = "DropDownCell"

	}

	internal struct UI {

		static let TextColor = UIColor.black
		static let TextFont = UIFont.init(name: "Mont-Regular", size: 15)!
		static let BackgroundColor = UIColor(white: 0.94, alpha: 1)
		static let SelectionBackgroundColor = UIColor(white: 0.89, alpha: 1)
		static let SeparatorColor = UIColor.clear
		static let CornerRadius: CGFloat = 2
		static let RowHeight: CGFloat = 40
		static let HeightPadding: CGFloat = 10

		struct Shadow {
            
			static let Color = UIColor.black
			static let Offset = CGSize.zero
			static let Opacity: Float = 0.5
			static let Radius: CGFloat = 6
		}

	}

	internal struct Animation {

		static let Duration = 0.15
		static let EntranceOptions: UIViewAnimationOptions = [.allowUserInteraction, .curveEaseOut]
		static let ExitOptions: UIViewAnimationOptions = [.allowUserInteraction, .curveEaseIn]
		static let DownScaleTransform = CGAffineTransform(scaleX: 0.9, y: 0.9)

	}

}
