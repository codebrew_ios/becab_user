//
//  Validation.swift
//  Grintafy
//
//  Created by Sierra 4 on 14/07/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import Foundation
import SwiftMessages

enum FieldType : String {
  case name
  case email
  case phone
    case cardNumber       = "Card Number"
    case cvv              = "CVV"
    case expiry           = "Expiry"
    case cardName        = "Card Name"
}

enum Valid {
  case success
  case failure(String)
}

class Validations {
  
  static let sharedInstance = Validations()
    
    var errorMessage = ""
    
    func isValidAddCardInfo(cardNumber : String?, expiry : String?, cvv: String?, firstName : String?)  -> Valid {
        
        if isValid(type: .cardNumber
            , info: cardNumber) &&  isValid(type: .expiry, info: expiry) &&  isValid(type: .cvv, info: cvv) &&  isValid(type: .cardName, info: firstName) {
            return .success
        }
        return .failure(errorMessage)
    }
    
    private func isValid(type : FieldType , info: String?) -> Bool {
        guard let validStatus = info?.handleStatus(fieldType : type) else {
            return true
        }
        errorMessage = validStatus
        return false
    }
    
    func validateSchedulingDate(date : Date, minDate: Date = Date().addHours(hoursToAdd: 1)) -> Bool {
        
        let oneHourAdded = minDate.addingTimeInterval(-10)
        if date.isGreaterThanDate(dateToCompare: oneHourAdded) {
            return true
        }
        return false
    }
    
    
    func validateCancellingReason(strReason : String) -> Bool {
        if strReason.isBlank {
            
            Alerts.shared.show(alert: "AppName".localizedString, message: "cancellation_reason_validation_text".localizedString , type: .error )

            return false
        }
    return true
    }
    
    
    func validateContactUS(strReason : String) -> Bool {
        if strReason.isBlank {
            Alerts.shared.show(alert: "AppName".localizedString, message: "please_enter_message".localizedString , type: .error )

            return false
        }
        return true
    }
  
  func validateEmail(email: String) -> Bool {
    if email.isBlank {
        Alerts.shared.show(alert: "AppName".localizedString, message: "Please enter email." , type: .error )

      return false
    } else {
      let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
      let status = NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: email)
      if status {
        return true
      } else {
        Alerts.shared.show(alert: "AppName".localizedString, message: "Please enter valid email." , type: .error )

        return false
      }
    }
  }
  
    func validateUserName(userName: String) -> Bool {
    if userName.isEmpty {
        
        Alerts.shared.show(alert: "AppName".localizedString, message: "name_empty_validation_message".localizedString , type: .error )

      return false
    }
    else {
         return true
//      let regEx = "^([a-zA-Z]{1,}\\s?[a-zA-z]{1,}'?-?[a-zA-Z]{1,}\\s?([a-zA-Z]{1,})?)"
//      let emailTest = NSPredicate(format:"SELF MATCHES %@", regEx)
//      let status = emailTest.evaluate(with: userName)
//      if status {
//        return true
//      } else {
//        Toast.show(text: "name_empty_validation_message".localizedString , type: .error)
//        return false
//      }
    }
  }
    
    func validateSignUpData(userName: String,imgCount: Int) -> Bool {
      if userName.isEmpty {
          
          Alerts.shared.show(alert: "AppName".localizedString, message: "name_empty_validation_message".localizedString , type: .error )

        return false
      }else if imgCount == 0 {
          Alerts.shared.show(alert: "AppName".localizedString, message: "imgUpload_message".localizedString , type: .error )

          return false
          
      }
      
      else {
           return true
  //      let regEx = "^([a-zA-Z]{1,}\\s?[a-zA-z]{1,}'?-?[a-zA-Z]{1,}\\s?([a-zA-Z]{1,})?)"
  //      let emailTest = NSPredicate(format:"SELF MATCHES %@", regEx)
  //      let status = emailTest.evaluate(with: userName)
  //      if status {
  //        return true
  //      } else {
  //        Toast.show(text: "name_empty_validation_message".localizedString , type: .error)
  //        return false
  //      }
      }
    }
 
  
  func validatePhoneNumber(phone: String) -> Bool {
    if phone.count <= 5 {
        Alerts.shared.show(alert: "AppName".localizedString, message: "phone_validation_message".localizedString , type: .error )
         return false
    }
    if phone.isEmpty {
        Alerts.shared.show(alert: "AppName".localizedString, message: "phone_validation_message".localizedString , type: .error )

      return false
    } else {
        if phone.isNumeric {
            return true
        } else if /Int(phone) == 0 {
            Alerts.shared.show(alert: "AppName".localizedString, message: "phone_validation_message".localizedString , type: .error )
            return false
        } else {
            Alerts.shared.show(alert: "AppName".localizedString, message: "phone_validation_message".localizedString , type: .error )
            return false
        }
    }
  }
  
  
}


extension String {
    var isNumeric: Bool {
        guard self.count > 0 else { return false }
        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        return Set(self).isSubset(of: nums)
    }
    
    enum Status : String {
        
        case chooseEmpty        = "Please choose "
        case selectEmpty        = "Please select "
        case empty              = "Please enter your "
        case emptyDescription   = "Please add "
        case allSpaces          = " field should not be blank "
        case singleDot          = "Only single period allowed for "
        case singleDash         = "Only single dash allowed for "
        case singleSpace        = "Only single space allowed for "
        case singleApostrophes  = "Only single apostrophes allowed for "
        case valid
        case inValid            = "Please enter a valid "
        case allZeros           = " Please enter a valid "
        case inValidCardNumber  = "Please enter valid "
        
        
        case hasSpecialCharacter = " must not contain special character."
        case hasAlphabets        = " must contain numeric digits only."
        case hasNotOnlyAlphabets = " must contain alphabets only."
        case notANumber          = " must be a number"
        case passwrdLength       = " has to be at least 6 characters."
        case mobileNumberLength  = "Please enter a 5-15 digit phone number."
        case passcodeNumberLength = "Passcode length Must be 4 to 6 digits."
        
        case postalcodeNumberLength = " length Must be 3 to 10 digits."
        case emptyCountrCode        = " Choose country code."
        case containsSpace          = " field should not contain space."
        
        case containsAtTheRateCharacter = " field should not contain @ character"
        case minimumCharacters          = " field should have atleast two characters"
        case minimumUsernameCharacters  = " field should have atleast six characters"
        case passwordFormat             = " "
        case usernameFormat             = " field should have alphabetic characters, numeric characters, underscores, periods, and dashes only"
        case passwordValidation         = " must meet complexity requirements as follows: \n• Contains at least 8 characters\n• Contains a combination of Uppercase and Lowercase\n• Contains at least 1 number\n• Contains at least 1 special character"
        case emptyMaritalStatus         = "Please select a relationship status."
        case emptyGender                = "Please select your gender."
        case emptyRelation              = "Sorry! Must select relationship."
        
        case invalidExpiry              = "Please enter valid card expiry date."
        case emptyExpiry                = "Please enter your card expiry date"
        case emptyCardname              = "Please enter name on your card."
        
        case expiryNotNumber            = "Expiry date must be number"
        
        
        func message(type : FieldType) -> String? {
            
            switch self {
            case .passwrdLength, .passwordValidation :
                return "Your " + type.rawValue.lowercased() + rawValue
            case .hasSpecialCharacter , .hasAlphabets, .hasNotOnlyAlphabets, .allSpaces , .passwordFormat ,.usernameFormat, .postalcodeNumberLength , .notANumber:
                return type.rawValue.lowercased() + rawValue
                
            case .valid:
                return nil
                
            case .mobileNumberLength , .emptyCountrCode , .passcodeNumberLength, .emptyGender, .emptyMaritalStatus, .emptyRelation, .invalidExpiry, .expiryNotNumber, .emptyCardname, .emptyExpiry :
                return  rawValue
                
            case .containsSpace:
                return type.rawValue.lowercased() + rawValue
                
            case .containsAtTheRateCharacter ,  .minimumCharacters , .minimumUsernameCharacters :
                return type.rawValue.lowercased() + rawValue
                
            default:
                return rawValue + type.rawValue.lowercased()
            }
        }
    }
    
    func handleStatus(fieldType : FieldType) -> String? {
        
        switch fieldType {
        case .cardNumber:
            return  isValidCardNumber(length: 19).message(type: fieldType)
        case .cvv:
            return  isValidCVV.message(type: fieldType)
        case .expiry :
            return isValidExpiry.message(type: fieldType)
        case .cardName :
            return isValidCardName.message(type:fieldType)
        default:
            return ""
        }
    }
    
    
    var isValidCardName : Status {
        if length <= 0 { return .emptyCardname }
        if isBlank { return .emptyCardname }
        if hasSpecialCharcters { return .hasSpecialCharacter }
        if notOnlyString {return .hasSpecialCharacter}
        return .valid
    }
    
    
    func isValidCardNumber(length max:Int ) -> Status {
        if length <= 0 { return .empty }
        if isBlank { return .allSpaces }
        if hasSpecialCharcters { return .hasSpecialCharacter }
        if isEveryCharcterZero { return .allZeros }
        if self.count >= 12 && self.count <= max{
            return .valid
        }
        return .inValidCardNumber
    }
    
    var isValidCVV : Status {
        if length == 0 { return .empty }
        if hasSpecialCharcters { return .hasSpecialCharacter }
        if isEveryCharcterZero { return .allZeros }
        if isNumberStr{
            if self.count >= 3 && self.count <= 4{
                return .valid
            }else{ return .inValid }
        }else { return .notANumber }
    }
    
    var isValidExpiryYear : Status {
        if hasSpecialCharcters { return .hasSpecialCharacter }
        if isEveryCharcterZero { return .allZeros }
        if isNumberStr{
            if self.count == 2 {
                return .valid
            }else{ return .inValid }
        }else { return .notANumber }
    }
    
    var isValidExpiry : Status {
        if length == 0 { return .emptyExpiry }
        if hasSpecialCharcters { return .invalidExpiry }
        if isEveryCharcterZero { return .invalidExpiry }
        if isNumberStr{
            if self.count == 4 {
                return .valid
            }else{ return .invalidExpiry }
        }else { return .invalidExpiry }
        
        return .valid
    }
    
    var isNumberStr : Bool {
        if let _ = NumberFormatter().number(from: self) {
            return true
        }
        return false
    }
    
    public var notOnlyString : Bool {
        
        let set = CharacterSet(charactersIn: "*=+[]\\|;:'\",<>{}()/?%")
        let inverted = set.inverted
        let filtered = self.components(separatedBy: inverted).joined(separator: "")
        return !filtered.isEmpty
    }
    
    var hasSpecialCharcters : Bool {
        return rangeOfCharacter(from: CharacterSet.alphanumerics.inverted) != nil
    }
    
    var isEveryCharcterZero : Bool{
        var count = 0
        self.forEach {
            if $0 == "0"{
                count += 1
            }
        }
        if count == self.count{
            return true
        }else{
            return false
        }
    }
    
    func applyPatternOnNumbers(replacmentCharacter: Character, pattern : String) -> String {
        
        //let pattern = getPattern()
        
        var pureNumber = self.replacingOccurrences( of: "[^0-9]", with: "", options: .regularExpression)
        for index in 0 ..< pattern.count {
            
            guard index < pureNumber.count else { return pureNumber }
            
            let stringIndex = String.Index(encodedOffset: index)
            let patternCharacter = pattern[stringIndex]
            
            guard patternCharacter != replacmentCharacter else { continue }
            pureNumber.insert(patternCharacter, at: stringIndex)
        }
        return pureNumber
    }
}
