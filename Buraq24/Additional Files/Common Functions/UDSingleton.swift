//
//  UserDefaultsManager.swift
//  InTheNight
//
//  Created by OSX on 19/02/18.
//  Copyright © 2018 InTheNight. All rights reserved.
//

import UIKit
import ObjectMapper
import CoreLocation



class UDSingleton: NSObject {
    
    static let shared = UDSingleton()
    private var loginDetail:LoginDetail?
   //Change Virat
    enum UserDefaultKeys: String {
        case walkThrough = "WalkThrough"
        
        func save(value: Any) {
            UserDefaults.standard.set(value, forKey: rawValue)
            UserDefaults.standard.synchronize()
            
        }
        
        func get() -> Any? {
            guard let value = UserDefaults.standard.value(forKey: rawValue) else { return nil}
            return value
            
        }
        
        func remove() {
            UserDefaults.standard.removeObject(forKey: rawValue)
            
        }
    }
    
    //MARK: -----> Walk Through
    var walkThrough: Bool? {
        get {
            guard let val = UserDefaultKeys.walkThrough.get() as? Bool else { return nil }
            return val
            
        }
        set{
            guard let val = newValue else { return UserDefaultKeys.walkThrough.remove() }
            UserDefaultKeys.walkThrough.save(value: val)
            
        }
    }
    
    //MARK:- User Data
    var userData:LoginDetail? {
        get{
            
            if let obj = self.loginDetail {
                return obj
            }
            
            if let userString = UserDefaults.standard.object(forKey: UDKeys.user) as? String {
                let userJson = Mapper<LoginDetail>.parseJSONStringIntoDictionary(JSONString: userString)
                self.loginDetail = Mapper<LoginDetail>().map(JSON: userJson ?? [:])
                return self.loginDetail
            }
            return nil
        }
        set{
            if let newVal = newValue {
                self.loginDetail = newVal
                let userString:String = /(newVal.toJSONString())
                UserDefaults.standard.set(userString, forKey: UDKeys.user)

            }else{
                UserDefaults.standard.removeObject(forKey: UDKeys.user)
            }
        }
    }
    
    
    
  
    //MARK: - Static Methods
     var isOnBoardingDone: Bool {
        
        set{
            UserDefaults.standard.set(newValue, forKey: UDKeys.onboardingRun)
        }
        get{
            return (UserDefaults.standard.value(forKey: UDKeys.onboardingRun) as? Bool)  ?? false
        }
    }
    
    static var deviceToken: String? {
        get {
            return UserDefaults.standard.value(forKey: UDKeys.deviceToken) as? String
        }
        set {
            if let value = newValue {
                UserDefaults.standard.set(value, forKey: UDKeys.deviceToken)
            }
            else {
                UserDefaults.standard.removeObject(forKey: UDKeys.deviceToken)
            }
        }
    }
    
    //MARK:- ======== Functions ========
    func clearData() {
       
        //    UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
    }
    
    func tokenExpired() {
        
          removeAppData()
        
    }
        
        
        func removeAppData() {
             // needs to unregister notifcations
            self.loginDetail = nil
            UserDefaults.standard.removeObject(forKey: UDKeys.user)
            UserDefaults.standard.synchronize()
            SocketIOManager.shared.closeConnection()
            UDSingleton.shared.clearData()
            AppDelegate.shared().setLoginAsRootVC()
            
        }
        
 
    //MARK: - Class Methods
    class func isGuestUser()->Bool {
        return /(UserDefaults.standard.value(forKey:UDKeys.user) as? String) == ""
    }
    
    func getService(categoryId : Int?) -> Service? {

        guard let services = userData?.services else{ return nil}
        let objCategory = services.filter{($0.serviceCategoryId == categoryId)}
        if !objCategory.isEmpty{
          return  objCategory.first
        }
        return nil
    }
}
