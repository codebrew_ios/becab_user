//
//  LanguageFile.swift
//  Buraq24
//
//  Created by MANINDER on 12/09/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class LanguageFile: NSObject {
    
    static let shared = LanguageFile()
   
    
    func setLanguage(languageID : Int) {
        
        switch languageID {
            
        case 1:
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            BundleLocalization.sharedInstance().language = Languages.English
            UserDefaultsManager.localeIdentifierCustom = "en_US"
//        case 2:
//            BundleLocalization.sharedInstance().language = Languages.Hindi
//            UIView.appearance().semanticContentAttribute = .forceLeftToRight
//            UserDefaultsManager.localeIdentifierCustom = "hi_IN"
//
//        case 3:
//            BundleLocalization.sharedInstance().language = Languages.Urdu
//            UIView.appearance().semanticContentAttribute = .forceRightToLeft
//            UserDefaultsManager.localeIdentifierCustom = "ur"
//
//        case 4:
//            BundleLocalization.sharedInstance().language = Languages.Chinese
//            UIView.appearance().semanticContentAttribute = .forceLeftToRight
//            UserDefaultsManager.localeIdentifierCustom = "zh_Hans_SG"
//
//
//        case 5:
//            BundleLocalization.sharedInstance().language = Languages.Arabic
//            UIView.appearance().semanticContentAttribute = .forceRightToLeft
//                   UserDefaultsManager.localeIdentifierCustom = "ar_OM"
//
        case 6:
            BundleLocalization.sharedInstance().language = Languages.Spanish
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            UserDefaultsManager.localeIdentifierCustom = "es_416"
            
        default:
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            BundleLocalization.sharedInstance().language = Languages.English
            UserDefaultsManager.localeIdentifierCustom = "en_US"

        }
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "done".localized
        UserDefaultsManager.languageId = "\(languageID)"
        UserDefaults.standard.set(BundleLocalization.sharedInstance().language, forKey: "language")
    }
    
    
    func getLanguage() -> String {
        
        if  let languageCode = UserDefaultsManager.languageId{
          return languageCode
        }
           return LanguageCode.English.rawValue
    }
    
    func isLanguageRightSemantic() -> Bool {
        
        if let languageCode = UserDefaultsManager.languageId {
            if languageCode == "3" ||  languageCode == "5" {
               return true
            }
        }
        return false
    }
    
}


extension UIButton{
    func setAlignment() {
//        if BundleLocalization.sharedInstance().language == Languages.Arabic || BundleLocalization.sharedInstance().language == Languages.Urdu{
//            self.contentHorizontalAlignment = .right
//        }else {
//            self.contentHorizontalAlignment = .left
//        }
    }
    
    func setLeftAlign(){
        self.contentHorizontalAlignment = .left
    }
}
