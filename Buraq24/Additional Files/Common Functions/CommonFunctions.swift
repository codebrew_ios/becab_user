//
//  CommonFunctions.swift
//  Idea
//
//  Created by Dhan Guru Nanak on 2/15/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import Foundation
import UIKit
import SafariServices
import DropDown


typealias  dropDownSelectedIndex = (_ index: Int , _ strVal: String) -> ()


class Utility: NSObject {
  
  static let shared = Utility()
 var localTimeZoneName: String { return TimeZone.current.identifier }
  
    override init() {
    super.init()
  }
  
  
  func getAttributedTextWithFont(text:String,font:UIFont) -> NSMutableAttributedString{
    let myAttribute = [NSAttributedStringKey.font: font]
    let myString = NSMutableAttributedString(string: text, attributes: myAttribute )
    return myString
  }
  
  func resetUserDefaultKeys() {
    let defaults = UserDefaults.standard
    let dictionary = defaults.dictionaryRepresentation()
    dictionary.keys.forEach { key in
      defaults.removeObject(forKey: key)
    }
  }
  
  func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int) {
    return ((seconds % 3600) / 60, (seconds % 3600) % 60)
  }
  
  func subtractSets(newSet:[String],oldSet:[String])->Set<String>{
    
    let newHydrantsIds:Set<String> = Set(newSet)
    let oldHydrantsIds:Set<String> = Set(oldSet)
    let removeMarkersID = newHydrantsIds.subtracting(oldHydrantsIds)
    
    return removeMarkersID
    
  }
  
  class func GetAttributedString(arrStrings : [String],arrColor : [UIColor], arrFont: [String] , arrSize: [CGFloat], arrNextLineCheck: [Bool]) -> NSMutableAttributedString {
    
    var attriString : NSMutableAttributedString?
    let combination = NSMutableAttributedString()
    
    for index in 0..<arrStrings.count {
      
      let yourAttributes = [NSAttributedStringKey.foregroundColor: arrColor[index], NSAttributedStringKey.font:UIFont.init(name: arrFont[index], size: arrSize[index])]
      
      if arrNextLineCheck[index] == true {
        
        attriString = NSMutableAttributedString(string:"\n\(arrStrings[index])" , attributes: yourAttributes)
        
      }else{
        
        attriString = NSMutableAttributedString(string:" \(arrStrings[index])"  , attributes: yourAttributes) }
      combination.append(attriString!)
    }
    
    return combination
  }
  
  func getDateFromFormat(date:String,time:String)->Date?{
    let dateFormatter = DateFormatter()
    dateFormatter.timeZone = NSTimeZone.default
    dateFormatter.dateFormat = "MMMM dd, yyyy 'at' h:mm a"
    let string = date + " at " + time                       // "March 24, 2017 at 7:00 AM"
    let finalDate = dateFormatter.date(from: string)
    return finalDate ?? Date()
    
  }
//     func getGoogleMapImageURL(long : String , lati : String , width : Int , height : Int ) -> String {
//
//        return "http://maps.google.com/maps/api/staticmap?markers=\(lati),\(long)&\("zoom=15&size=\(2*width)x\(2*height)")&sensor=true&key=\(APIBasePath.googleApiKey)"
//    }
    
    func getStaticMapWithMarker(latitude: Double, longitude: Double) -> String
    {
        let key = APIBasePath.googleApiKey
        return  "https://maps.googleapis.com/maps/api/staticmap?center=\(latitude),\(longitude)&zoom=16&scale=1&size=600x300&maptype=roadmap&key=\(key)&format=png&visual_refresh=true&markers=size:mid%7Ccolor:0x002F66%7Clabel:D%7C\(latitude),\(longitude)"
    }
    
    func getStaticMapWithPolyLine(pickUpLat: Double,pickUpLng: Double,dropLat: Double,dropLng: Double) -> String{
        let key = APIBasePath.googleApiKey
        let strURL = "https://maps.googleapis.com/maps/api/staticmap?center=\(pickUpLat),\(pickUpLng)&size=600x300&markers=size:mid|color:0xDAB37F|label:S|\(pickUpLat),\(pickUpLng)&markers=size:mid|color:0x002F66|label:D|\(dropLat),\(dropLng)&key=\(key)&path=color:0x1E2E4D|weight:2|\(pickUpLat),\(pickUpLng)|\(dropLat),\(dropLng)"
        return strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
    }
    
    func currentTimeZone() -> String{
        return String (TimeZone.current.identifier)
    }
    
    
    func showDropDown(anchorView : UIView , dataSource : [String] , width : CGFloat , handler : @escaping dropDownSelectedIndex ) {
        
        let dropDown = DropDown()
        
        dropDown.anchorView = anchorView // UIView or UIBarButtonItem
        
        dropDown.dataSource = dataSource
        
        dropDown.selectionAction = { (index: Int, item: String) in
            handler(index, item)
        }
        dropDown.width = width
        dropDown.show()
    }
  
  
}



