//
//  ThemeLabel.swift
//  Sneni
//
//  Created by MAc_mini on 23/01/19.
//  Copyright © 2019 Taran. All rights reserved.
//

import UIKit

class SKThemeLabel: UILabel {
    
    @IBInspectable public var textColorId: String = "" {
        didSet {
            
            guard let id = SKThemeButtonColorId(rawValue: textColorId) else { return }
            self.textColor = id.color
            
        }
    }
}

class ThemeLabel: SKThemeLabel {
    
    @IBInspectable public var isVisibleNavLblTxtColor: Bool = true{
        didSet{
            if isVisibleNavLblTxtColor == true{
                
            
                 self.textColor = #colorLiteral(red: 0.07843137255, green: 0.2156862745, blue: 0.3607843137, alpha: 1)
                
                 //self.font = UIFont(openSans: .bold, size: 15.0)
             
            }
        }
    }
    
    
    @IBInspectable public var themeLbl: Bool = true{
        didSet{
            if themeLbl == true{
                
                
                self.textColor = #colorLiteral(red: 0.07843137255, green: 0.2156862745, blue: 0.3607843137, alpha: 1)
                
                //self.font = UIFont(openSans: .bold, size: 15.0)
                
            }
        }
    }

    @IBInspectable public var isVisibleLblTextColor: Bool = true{
        didSet{
            if isVisibleLblTextColor == true{
                 self.textColor = #colorLiteral(red: 0.07843137255, green: 0.2156862745, blue: 0.3607843137, alpha: 1)
                 // self.font = UIFont(openSans: .semiboldItalic, size: 15.0)
            }
          }
        }
    
    @IBInspectable public var isLblTilte: Bool = true{
        didSet{
            if isLblTilte == true{
                
                self.textColor = #colorLiteral(red: 0.07843137255, green: 0.2156862745, blue: 0.3607843137, alpha: 1)
               // self.font = UIFont(openSans: .boldItalic, size: 13.0)
                 
            }
        }
    }
    
    @IBInspectable public var isLblSubTilte: Bool = true{
        didSet{
            if isLblSubTilte == true{
                self.textColor = #colorLiteral(red: 0.07843137255, green: 0.2156862745, blue: 0.3607843137, alpha: 1)
                // self.font = UIFont(openSans: .semibold, size: 11.0)
            }
        }
    }
    
    @IBInspectable public var isLblLightTxt: Bool = true{
        didSet{
            if isLblLightTxt == true{
                self.textColor = #colorLiteral(red: 0.07843137255, green: 0.2156862745, blue: 0.3607843137, alpha: 1)
                // self.font = UIFont(openSans: .boldItalic, size: 11.0)
            }
        }
    }
    
    
    @IBInspectable public var isLblStatusColor: Bool = true{
        didSet{
            if isLblStatusColor == true{
                self.textColor = #colorLiteral(red: 0.07843137255, green: 0.2156862745, blue: 0.3607843137, alpha: 1)
               // self.font = UIFont(openSans: .lightItalic, size: 11.0)
            }
        }
    }
    
    
    @IBInspectable public var isLblSectionHeader: Bool = true{
        didSet{
            if isLblSectionHeader == true{
                self.textColor = #colorLiteral(red: 0.07843137255, green: 0.2156862745, blue: 0.3607843137, alpha: 1)
               //  self.font = UIFont(openSans: .bold, size: 15.0)
            }
        }
    }
    
    
    @IBInspectable public var isTableTitleLbl: Bool = true{
        didSet{
            if isTableTitleLbl == true{
                self.textColor = #colorLiteral(red: 0.07843137255, green: 0.2156862745, blue: 0.3607843137, alpha: 1)
              //  self.font = UIFont(openSans: .boldItalic, size: 11.0)
//                self.textColor = ColorTheme.shared.lblSectionHeaderClr
//                self.font = UIFont(openSans: .bold, size: 15.0)
            }
        }
    }
    
    @IBInspectable public var isTableSubTitleLbl: Bool = true{
        didSet{
            if isTableSubTitleLbl == true{
                self.textColor = #colorLiteral(red: 0.07843137255, green: 0.2156862745, blue: 0.3607843137, alpha: 1)
                //self.font = UIFont(openSans: .semibold, size: 11.0)
//                self.textColor = ColorTheme.shared.lblSectionHeaderClr
//                self.font = UIFont(openSans: .bold, size: 15.0)
            }
        }
    }
    
    @IBInspectable public var isTableDescLbl: Bool = true{
        didSet{
            if isTableDescLbl == true{
                
                self.textColor = #colorLiteral(red: 0.07843137255, green: 0.2156862745, blue: 0.3607843137, alpha: 1)
                //self.font = UIFont(openSans: .boldItalic, size: 11.0)
//                self.textColor = ColorTheme.shared.lblSectionHeaderClr
//                self.font = UIFont(openSans: .bold, size: 15.0)
            }
        }
    }
    
    @IBInspectable public var isCollectionTitleLbl: Bool = true{
        didSet{
            if isCollectionTitleLbl == true{
                self.textColor = #colorLiteral(red: 0.07843137255, green: 0.2156862745, blue: 0.3607843137, alpha: 1)
               // self.font = UIFont(openSans: .bold, size: 11.0)
            }
        }
    }
    
    @IBInspectable public var isCollectionSubTitleLbl: Bool = true{
        didSet{
            if isCollectionSubTitleLbl == true{
                self.textColor = #colorLiteral(red: 0.07843137255, green: 0.2156862745, blue: 0.3607843137, alpha: 1)
               // self.font = UIFont(openSans: .bold, size: 11.0)
            }
        }
    }
    
    
    @IBInspectable public var isCollectionDescLbl: Bool = true{
        didSet{
            if isCollectionDescLbl == true{
                
                self.textColor = #colorLiteral(red: 0.07843137255, green: 0.2156862745, blue: 0.3607843137, alpha: 1)
               // self.font = UIFont(openSans: .bold, size: 11.0)
            }
        }
    }
    
    @IBInspectable public var leftMenuTitleLbl: Bool = true{
        didSet{
            if leftMenuTitleLbl == true{
                
                self.textColor = #colorLiteral(red: 0.07843137255, green: 0.2156862745, blue: 0.3607843137, alpha: 1)
                //self.font = UIFont(openSans: .bold, size: 11.0)
            }
        }
    }
    
    @IBInspectable public var leftMenuSubTitleLbl: Bool = true{
        didSet{
            if leftMenuSubTitleLbl == true{
                
                self.textColor = #colorLiteral(red: 0.07843137255, green: 0.2156862745, blue: 0.3607843137, alpha: 1)
                //self.font = UIFont(openSans: .bold, size: 11.0)
            }
        }
    }
    
    



}
