//
//  ThemeButton.swift
//  Sneni
//
//  Created by MAc_mini on 22/01/19.
//  Copyright © 2019 Taran. All rights reserved.
//

import UIKit

enum SKThemeButtonColorId: String {
    
    case appColor = "AppColor"
    case gray32 = "Gray32"
    case gray90 = "Gray90"
    case white = "White"

    var color: UIColor {
        switch self {
        case .appColor:
            return #colorLiteral(red: 0.07843137255, green: 0.2156862745, blue: 0.3607843137, alpha: 1)
            
        case .gray32:
            return #colorLiteral(red: 0.3176470588, green: 0.3176470588, blue: 0.3176470588, alpha: 1)
            
        case .gray90:
            return #colorLiteral(red: 0.5647058824, green: 0.5647058824, blue: 0.5647058824, alpha: 1)
        case .white:
            return UIColor.white
        }
    }
}

class SKActivityIndicatorView: UIActivityIndicatorView {
    
    @IBInspectable public var colorId: String = "" {
        didSet {
            guard let id = SKThemeButtonColorId(rawValue: colorId) else { return }
            color = id.color
        }
    }
}

class SKThemeButton: UIButton {
    
    @IBInspectable public var bgColor: String = "" {
        didSet {
            guard let id = SKThemeButtonColorId(rawValue: bgColor) else { return }
            backgroundColor = id.color
        }
    }
    
    @IBInspectable public var imgColorId: String = "" {
        didSet {
            setImage(image(for: .normal)?.withRenderingMode(.alwaysTemplate), for: .normal)
            setImage(image(for: .selected)?.withRenderingMode(.alwaysTemplate), for: .selected)
            
            guard let id = SKThemeButtonColorId(rawValue: isSelected ? imgSelectedColorId : imgColorId) else { return }
            self.imageView?.tintColor = id.color

        }
    }
    
    @IBInspectable public var imgSelectedColorId: String = "" {
        didSet {
            
            setImage(image(for: .normal)?.withRenderingMode(.alwaysTemplate), for: .normal)
            setImage(image(for: .selected)?.withRenderingMode(.alwaysTemplate), for: .selected)
            guard let id = SKThemeButtonColorId(rawValue: isSelected ? imgSelectedColorId : imgColorId) else { return }
            self.imageView?.tintColor = id.color
            
        }
    }
    
    @IBInspectable public var titleColorId: String = "" {
        didSet {
            guard let id = SKThemeButtonColorId(rawValue: titleColorId) else { return }
            setTitleColor(id.color, for: .normal)
        }
    }
    
    @IBInspectable public var titleSelectedColorId: String = "" {
        didSet {
            guard let id = SKThemeButtonColorId(rawValue: titleSelectedColorId) else { return }
            setTitleColor(id.color, for: .normal)
        }
    }
    
    override var isSelected: Bool {
        didSet {
            guard let id = SKThemeButtonColorId(rawValue: isSelected ? imgSelectedColorId : imgColorId) else { return }
            self.imageView?.tintColor = id.color
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.001) {
            [weak self] in
            guard let self = self else { return }
            
            guard let id = SKThemeButtonColorId(rawValue: self.isSelected ? self.imgSelectedColorId : self.imgColorId) else { return }
            self.imageView?.tintColor = id.color
        }
    }
    
}

class SKThemeView: UIView {
    
    @IBInspectable public var bgColor: String = "" {
        didSet {
            guard let id = SKThemeButtonColorId(rawValue: bgColor) else { return }
            backgroundColor = id.color
        }
    }
}

class ThemeButton: SKThemeButton {
    
    @IBInspectable public var btnTextColor: Bool = true {
        didSet{
            
            if btnTextColor == true{
                
                self.setTitleColor(#colorLiteral(red: 0.07843137255, green: 0.2156862745, blue: 0.3607843137, alpha: 1), for: .normal)
                self.setTitleColor(#colorLiteral(red: 0.07843137255, green: 0.2156862745, blue: 0.3607843137, alpha: 1), for: .selected)

            }
            
        }
    }
    
    @IBInspectable public var btnTintFlag: Bool = true{
        didSet{
            
            if btnTintFlag == true {
                
                self.imageView?.image?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
                
                self.imageView?.tintColor = #colorLiteral(red: 0.07843137255, green: 0.2156862745, blue: 0.3607843137, alpha: 1)
                self.tintColor = #colorLiteral(red: 0.07843137255, green: 0.2156862745, blue: 0.3607843137, alpha: 1)
                
            }
        }
    }
    
    @IBInspectable public var btnbgFlag: Bool = true{
        
        didSet{
            
            if btnbgFlag == true{
                
                
                self.backgroundColor = #colorLiteral(red: 0.07843137255, green: 0.2156862745, blue: 0.3607843137, alpha: 1)
            }
        }
    }
    
    @IBInspectable public var isVisibleTitleColor: Bool = true{
        didSet{
            
            if isVisibleTitleColor == true{
                
               // self.titleLabel?.font =  UIFont(openSans: .boldItalic, size: 15.0)
                self.setTitleColor(#colorLiteral(red: 0.07843137255, green: 0.2156862745, blue: 0.3607843137, alpha: 1), for: .normal)
                self.setTitleColor(#colorLiteral(red: 0.07843137255, green: 0.2156862745, blue: 0.3607843137, alpha: 1), for: .selected)
            }
          
            
        }
    }
    
    
    @IBInspectable public var isBorderColor: Bool = true{
        didSet{
            
            if isBorderColor ==  true{
                 layer.borderColor = #colorLiteral(red: 0.07843137255, green: 0.2156862745, blue: 0.3607843137, alpha: 1).cgColor
            }
           
            
        }
    }
    
    @IBInspectable public var isSelectedColor: Bool = true{
        didSet{
            
            if isSelectedColor ==  true{
                layer.borderColor = #colorLiteral(red: 0.07843137255, green: 0.2156862745, blue: 0.3607843137, alpha: 1).cgColor
            }
            
            
        }
    }
    
    @IBInspectable public var isUnSelectedColor: Bool = true{
        didSet{
            
            if isSelectedColor ==  true{
                layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1).cgColor
            }
            
            
        }
    }
    
    @IBInspectable public var isbackImage: Bool = true{
        didSet{
            if isbackImage == false{
                 layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1).cgColor
            }
           
        }
    }
    
    
}
