//
//  Enums.swift
//  Buraq24
//
//  Created by MANINDER on 01/08/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import Foundation
import GoogleMaps
//MARK:- App Data based enums


enum LanguageCode : String {
    case English = "1"
    case Hindi = "2"
    case Urdu = "3"
    case Chinese = "4"
    case Arabic = "5"
    case Spanish = "6"
}


enum PaymentScreenMode : Int {
    case BookRequest = 1
    case SideMenu = 2
}

struct AllOrdersOngoing{
    
    static var ordersOngoing = [String:TrackingModel]()
    static var order : TrackingModel?
}


struct ServiceType {
    var serviceName: String
    var serviceImageSelected: UIImage
    var serviceImageUnSelected: UIImage
    var objService: Service
}

struct ServiceRequest {
    
    var latitude : Double?
    var longitude: Double?
    var locationName : String?
    
    var latitudeDest : Double?
    var longitudeDest : Double?
    var locationNameDest : String?
    
    var serviceSelected : Service?
    var selectedBrand : Brand?
    var selectedProduct :  Product?
    var quantity  = 0
    var distance : Float = 0
    var productName : String?
    var orderDateTime : Date = Date()
    var paymentMode : PaymentType = .Cash
    var finalPrice : Float = 0.0
    var requestType : BookingType = .Present
    var eToken : ETokenPurchased?
    
    var materialType : String?
    var weight : Double?
    var additionalInfo : String?
    var orderImages : [UIImage] = [UIImage]()
    
    var pickupPersonName : String?
    var pickupPersonPhone : String?
    var invoiceNumber : String?
    var deliveryPersonName : String?
    
}


enum ScrollDirection {
    case Top
    case Right
    case Bottom
    case Left
    
    func contentOffsetWith(scrollView: UIScrollView) -> CGPoint {
        var contentOffset = CGPoint.zero
        switch self {
        case .Top:
            contentOffset = CGPoint(x: 0, y: -scrollView.contentInset.top)
        case .Right:
            contentOffset = CGPoint(x: scrollView.contentSize.width - scrollView.bounds.size.width, y: 0)
        case .Bottom:
            contentOffset = CGPoint(x: 0, y: scrollView.contentSize.height - scrollView.bounds.size.height)
        case .Left:
            contentOffset = CGPoint(x: -scrollView.contentInset.left, y: 0)
        }
        return contentOffset
    }
}


enum MoveType  : Int {
    case Forward  = 1
    case Backward = 0
}

enum ServicePopUp  : Int {
    case Gas  = 1
    case WaterTanker = 2
}

enum ActionType : Int {
    case SelectLocation  = 0
    case Capacity  = 1
    case Quantity = 2
    case NextGasType = 3
    case Payment = 4
    case SubmitOrder = 5
    case CancelOrderOnSearch = 6
    case CancelTrackingOrder = 7
    case DoneInvoice = 8
    case SubmitRating = 9
    case BackFromFreightBrand = 10
    case BackFromFreightProduct = 11
}

struct MovingVehicle {
    var driver : HomeDriver
    var driverMarker : GMSMarker
}


enum LocationEditing : Int {
    
    case PickUp = 0
    case DropOff = 1
    
}

enum MapMode  {
    case ZeroMode
    case NormalMode
    case SelectingLocationMode
    case OrderDetail
    case ScheduleMode
    case OrderPricingMode
    case UnderProcessingMode
    case RequestAcceptedMode
    case OnTrackMode
    case ServiceDoneMode
    case ServiceFeedBackMode
    case SelectingFreightBrand
    case SelectingFreightProduct
    
}

struct ScreenType {
    
    var mapMode : MapMode = .ZeroMode
    var entryType : MoveType = .Forward
}

enum SideMenuOptions:String {
    
    case Bookings = "bookings"
    case ETokens = "E-Tokens"
    case Promotions = "promotions"
    case Payments = "payments"
    case Referral = "referral"
    case EmergencyContacts = "emergency_contacts"
    case Settings = "settings"
    case Contactus = "contact_us"
    case SignOut = "sign_out"
    
    func localString() -> String {
        return NSLocalizedString(self.rawValue, comment: "")
    }
}

internal struct Languages {
//    static let Arabic = "ar"
    static let English = "en"
//    static let Urdu = "ur"
//    static let Hindi = "hi"
//    static let Chinese = "zh-Hans"
    static let Spanish = "es"
}

enum PaymentType : String {
    case Cash = "Cash"
    case Card = "Card"
    case EToken = "eToken"
}

enum BookingType : String {
    case Present = "0"
    case Future = "1"
}

enum NumberType:String {
    case Old = "Old"
    case New = "New"
}

enum UserType : Int {
    case Customer = 1
    case Driver = 2
    case SupportProvider = 3
}

enum UserOnlineStatus : String {
    case Online = "1"
    case Offline = "0"
}

enum Locations : Double {
    
    case lat
    case longitude
    
    func getLoc () -> Double {
        
        switch self {
        case .lat:
            return LocationManager.shared.latitude
        case .longitude :
            return LocationManager.shared.longitude
        }
    }
}

enum PaymentStatus : String {
    case Pending = "Pending"
    case Paid = "Paid"
}

enum OrderStatus: String {
    
    case Searching = "Searching"
    case Ongoing = "Ongoing"
    case Confirmed = "Confirmed"
    case reached = "Onreached"
    case CustomerCancel = "CustCancel"
    case ServiceComplete = "SerComplete"
    case ServiceTimeout = "SerTimeout"
    case ServiceReject = "SerReject"
    case DriverCancel = "DriverCancel"
    case Scheduled = "Scheduled"
    case DriverApprovalPending = "DPending"
    case DriverApproval = "DApproved"
    case DriverSchCancelled = "DSchCancelled"
    case DriverSchTimeOut = "DSchTimeout"
    case SystyemSchCancelled = "SysSchCancelled"
    // User Current Ride ongoing but schedule cancelled
    case  etokenSerCustCancel = "SerCustCancel"
    case etokenTimeOut = "CTimeout"
    case etokenCustomerPending = "SerCustPending"
    case etokenCustomerConfirm = "SerCustConfirm"
//    case reached = "Reached"

}

//MARK:- Socket Enums
enum SocketEvents : String {
    
    case OrderEvent = "OrderEvent"
    case CommonEvent = "CommonEvent"
    
}

enum BuraqMapType : String {
    
    case Satellite = "Satellite"
    case Hybrid = "Hybrid"
}

enum CommonEventType : String {
    
    case CustomerHomeMap = "CustHomeMap"
    case CustomerOrderTrack = "CCurrentOrders"
    case ParticularOrder = "CustSingleOrder"
}

enum EmitterParams : String {
    
    case EmitterType = "type"
    case AccessToken = "access_token"
    case Latitude = "latitude"
    case Longitude = "longitude"
    case Distance = "distance"
    case CategoryId = "category_id"
    case OrderToken = "order_token"
}

enum OrderEventType : String {
    
    case serReached = "Onreached"
    case ServiceAccepted = "SerAccept"
    case DriverRatedCustomer = "DriverRatedService"
    case ServiceRejectedByAllDriver = "SerReject"
    case ServiceCompletedByDriver = "SerComplete"
    case ServiceTimeOut = "SerTimeout"
    case ServiceCurrentOrder = "CurrentOrders"
    case ServiceOngoing = "Ongoing"
    case DriverCancelrequest = "DriverCancel"
    case PaymentPending = "paymentPending"

    case EtokenConfirmed = "Confirmed"
    case EtokenStart = "eTokenSerStart"
    case EtokenSerCustPending = "SerCustPending"
    case EtokenCTimeout = "CTimeout"
    
}
