//
//  GooglePlacePicker.swift
//  Nizcare
//
//  Created by nizcare on 8/1/18.
//  Copyright © 2018 nizcare. All rights reserved.
//

import Foundation
import GoogleMaps
import GooglePlacePicker

typealias  DidPickPlace = (_ place: GMSPlace) -> ()
typealias  DidCancel = (_ placePickerCanceled : Bool) -> ()
typealias  DidTapMarkerr = (_ markerTapped : Bool) -> ()

class GooglePlacePickerManager: NSObject,GMSPlacePickerViewControllerDelegate,GMSMapViewDelegate, GMSAutocompleteViewControllerDelegate {
    
    var didPickPlace : DidPickPlace?
    var didCancel : DidCancel?
    var didTapMarker : DidTapMarkerr?
    
    init(didPickPlace : DidPickPlace? , didCancel : DidCancel? , didTapMarker : DidTapMarkerr?) {
        self.didPickPlace = didPickPlace
        self.didCancel = didCancel
        self.didTapMarker = didTapMarker
    }
    
    override init() {
        super.init()
    }
    
    
    //GOOGLE PLACE PICKER DELEGATES
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
        //        print("Place name \(place.name)")
        //        print("Place address \(place.formattedAddress)")
        //        print("Place attributions \(place.attributions)")
        
        if let block = self.didPickPlace {
            block(place)
        }
        
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
        if let block = self.didCancel {
            block(true)
        }
        print("No place selected")
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        return false
    }
    
    
    
    //GOOGLE AUTO COMPLETE DELEGATES
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        viewController.dismiss(animated: true, completion: nil)
        
        //        print("Place name \(place.name)")
        //        print("Place address \(place.formattedAddress)")
        //        print("Place attributions \(place.attributions)")
        
        if let block = self.didPickPlace {
            block(place)
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        
        print("Google Auto complete Error :-" ,error.localizedDescription)
        
        viewController.dismiss(animated: true, completion: nil)
        
        if let block = self.didCancel {
            block(true)
        }
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        
        viewController.dismiss(animated: true, completion: nil)
        
        if let block = self.didCancel {
            block(true)
        }
    }
    
    
}

