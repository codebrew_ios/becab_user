//
//  GooglePlaceDataSource.swift
//  Buraq24
//
//  Created by MANINDER on 03/08/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit

import GooglePlaces
import SwiftyJSON


typealias  responseAPI = (_ response : [GMSAutocompletePrediction]) -> ()
typealias  responsePlaceDetailsAPI = (_ response : GMSPlace?) -> ()


class GooglePlaceDataSource: NSObject {
    
    var responseListener : responseAPI?
    var searchTextField : UITextField?
    
    init (txtField: UITextField? ,resListener: responseAPI?) {
        
        super.init()
        
        searchTextField = txtField
        searchTextField?.addTarget(self, action:  #selector(getPlaces), for: .editingChanged)
        
        responseListener = resListener
    }
    
    override init() {
        super.init()
    }
    
    @objc func getPlaces() {
        
        let filter = GMSAutocompleteFilter()
        //        filter.type = .noFilter
        let locale = Locale.current
        filter.country = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as? String
//        filter.type = .establishment
        var bounds: GMSCoordinateBounds?
        if let corrd = LocationManager.shared.currentLoc?.coordinate {
            bounds = GMSCoordinateBounds.init(coordinate: corrd, coordinate: corrd)
        }
        GMSPlacesClient.shared().autocompleteQuery(/(searchTextField?.text), bounds: bounds, filter: filter) { [weak self](results, error) -> Void in
            if let newResults = results {
                self?.responseListener!(newResults)
            }else{
                debugPrint(error?.localizedDescription)
                self?.responseListener!([GMSAutocompletePrediction]())
            }
        }
    }
    
    class func placeDetails(placeID: String , responseListener: @escaping responsePlaceDetailsAPI) {
        
        GMSPlacesClient.shared().lookUpPlaceID(placeID, callback: { (place, error) -> Void in
            if let error = error {
                print("lookup place id query error: \(error.localizedDescription)")
                return
            }
            
            guard let place = place else {
                print("No place details for \(placeID)")
                return
            }
            //    print("Place address \(place.formattedAddress)")
            responseListener(place)
            
            
        })
        
        
    }
    
    
}
