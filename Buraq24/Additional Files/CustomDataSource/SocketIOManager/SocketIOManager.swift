//
//  SocketIOManager.swift
//  GasItUp
//
//  Created by cbl24_Mac_mini on 11/04/18.
//  Copyright © 2018 cbl24_Mac_mini. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Alamofire
import SocketIO
import ObjectMapper

typealias  OrderEventResponseBlock = (_ response : Any? , _ type : OrderEventType) -> ()
typealias  TrackResponseBlock = (_ response : Any?) -> ()


class SocketIOManager: NSObject {
    
    static let shared = SocketIOManager()
    
    private var manager: SocketManager?
    var socket: SocketIOClient?
    
    override init() {
        super.init()
//        let token = UDSingleton.shared.userData?.userDetails?.accessToken
//
//        guard let URL = URL(string: APIBasePath.basePath) else {
//            return
//        }
//
//        manager = SocketManager(socketURL: URL , config: [.log(false), .connectParams(["access_token" : /token])])
//        socket = manager?.defaultSocket
//        setupListeners()
        
    }
    
    func initialiseSocketManager(){
        let token = UDSingleton.shared.userData?.userDetails?.accessToken
        
        guard let URL = URL(string: APIBasePath.basePath) else {
            return
        }
        
        manager = SocketManager(socketURL: URL , config: [.log(false), .connectParams(["access_token" : /token])])
        socket = manager?.defaultSocket
        setupListeners()
        self.establishConnection()
    }
    
    
    //Server Methods
    
    func establishConnection() {
        
        let token = UDSingleton.shared.userData?.userDetails?.accessToken
        if (self.socket?.status == .disconnected || self.socket?.status == .notConnected ) {
            if (token != nil || token != "") {
                socket?.connect()
            }
        }
        else {
            debugPrint("======= Socket already connected =======")
        }
    }
    
    func closeConnection() {
        debugPrint("=======***** SocketClientEvent.disconnect called ****=======")
        socket?.disconnect()
    }
    
    func setupListeners() {
        socket?.on(SocketClientEvent.disconnect.rawValue) { [weak self] (array, emitter) in
            debugPrint("======= SocketClientEvent.disconnect listener=======")
          self?.establishConnection()
        }
        
        socket?.on(SocketClientEvent.error.rawValue) {[weak self] (array, emitter) in
            debugPrint("======= SocketClientEvent.error =======")
          self?.establishConnection()
        }
        
        socket?.on(SocketClientEvent.connect.rawValue) {  (array, emitter) in
            if self.socket?.status == .connected {
                debugPrint("======= userauth after connected =======")
            }
        }
    }
    
    
    func getStatus() -> SocketIOStatus? {
        guard let status = self.socket?.status else{ return nil }
        return status
    }
    
    //MARK:- Emitter Events
    //MARK:-
    
    func listenOrderEventConnected(_ completionHandler: @escaping OrderEventResponseBlock) {
        socket?.on(SocketEvents.OrderEvent.rawValue) {(arrData, socketAck) in
            
            guard let item = JSON(arrData[0]).dictionaryObject else {
                print("JSON not correct")
                return
            }
            
            print("socket data")
            
            guard  let type = item["type"] as? String else{return}
            print("type = \(type)")
            print(item)

            guard let typeSocket :  OrderEventType = OrderEventType(rawValue: type) else {return}
            
            switch typeSocket {
                
            case .serReached, .ServiceAccepted , .ServiceOngoing :
             
                guard  let orderModel = item["order"] as? [Any] else{return}
                let order =  Mapper<Order>().map(JSONObject: orderModel[0])
                completionHandler(order, typeSocket)
                
            case .DriverRatedCustomer:
                
                print("DriverRatedCustomer")
                
            case .ServiceCompletedByDriver:
                
                guard  let orderModel = item["order"] as? [Any] else{return}
                let order =  Mapper<Order>().map(JSONObject: orderModel[0])
                completionHandler(order, typeSocket)
                
            case .ServiceRejectedByAllDriver  ,.ServiceTimeOut :
              completionHandler(nil, typeSocket)
          
            case .ServiceCurrentOrder:
                
                let trackObj =  Mapper<TrackingModel>().map(JSONObject: item)
                completionHandler(trackObj, typeSocket)
                
            case .DriverCancelrequest:
                
                guard  let orderModel = item["order"] as? [Any] else{return}
                let order =  Mapper<Order>().map(JSONObject: orderModel[0])
                completionHandler(order, typeSocket)
                
            case .EtokenConfirmed:
                
                Alerts.shared.show(alert: "etoken.eToken".localizedString, message: R.string.localizable.eTokenOrderConfirmed(), type: .success)
                completionHandler(nil,typeSocket)
                
            case .EtokenStart:
                
                guard  let orderModel = item["order"] as? [Any] else{return}
                let order =  Mapper<Order>().map(JSONObject: orderModel[0])
                
                    NotificationCenter.default.post(name: Notification.Name("\(LocalNotifications.ETokenRefresh.rawValue)\(/order?.orderId)"), object: nil, userInfo: nil)
                
                Alerts.shared.show(alert: "etoken.eToken".localizedString, message: R.string.localizable.etokenOrderOutForDelivery(), type: .success)
                completionHandler(nil,typeSocket )
                
            case .EtokenSerCustPending :
                guard  let orderModel = item["order"] as? [Any] else{return}

                let order =  Mapper<Order>().map(JSONObject: orderModel[0])
                NotificationCenter.default.post(name: Notification.Name("\(LocalNotifications.ETokenRefresh.rawValue)\(/order?.orderId)"), object: nil, userInfo: nil)
                Alerts.shared.show(alert: "etoken.eToken".localizedString, message: R.string.localizable.eTokenOrderWaitingConfirmation(), type: .success)
                completionHandler(nil,typeSocket )

            case .EtokenCTimeout :
                guard  let orderModel = item["order"] as? [Any] else{return}
                
                let order =  Mapper<Order>().map(JSONObject: orderModel[0])
                NotificationCenter.default.post(name: Notification.Name("\(LocalNotifications.ETokenRefresh.rawValue)\(/order?.orderId)"), object: nil, userInfo: nil)
                Alerts.shared.show(alert: "etoken.eToken".localizedString, message: R.string.localizable.etokenOrderTimeOut(), type: .success)
                completionHandler(nil,typeSocket )
            case .PaymentPending:
                print("completed")
                completionHandler(item["payment"], typeSocket)

             }
           }
        }
    
    //MARK:- Listener Events
    /// It will get driver listing near you when user change service type at home screen view
    func emitMapLocation(_ userData : [String: Any] , _ completionHandler: @escaping TrackResponseBlock) {
        
        print("UserData CommonEvent : ")
        print(userData)

        socket?.emitWithAck(SocketEvents.CommonEvent.rawValue , userData).timingOut(after: 4.0, callback: { (response) in
            
            print("CommonEvent : ")
            print(response)
            
            guard  let item = JSON(response[0]).dictionaryObject else{return}
            let json = JSON(item)
             if json[APIConstants.statusCode.rawValue].stringValue == Validate.successCode.rawValue {
                 let objDriver = Mapper<ApiSucessData<DriverList>>().map(JSONObject: item)
                
              completionHandler( objDriver?.object)
                
             }
        })
    }
    
    func getParticularOrder(_ userData : [String: Any]  , _ completionHandler: @escaping TrackResponseBlock) {
        
        socket?.emitWithAck(SocketEvents.CommonEvent.rawValue, userData).timingOut(after: 2.0, callback: {
            (response) in
            let item = JSON(response[0]).dictionaryObject
           
            let json = JSON(item)
   
            if json[APIConstants.statusCode.rawValue].stringValue == Validate.successCode.rawValue {
                let objOrder = Mapper<ApiSucessData<Order>>().map(JSONObject: item)
                completionHandler( objOrder?.object)
            }
        })
    }
    
}
