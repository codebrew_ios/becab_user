//
//  PickerViewCustomDataSource.swift
//  Grintafy
//
//  Created by Sierra 4 on 20/07/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import Foundation
import UIKit

typealias  SelectedRowBlock = (_ selectedRow: Int , _ item: Any) -> ()
class PickerViewCustomDataSource: NSObject{
    
    var picker: UIPickerView?
    var pickerData:[Any]?
    var aSelectedBlock: SelectedRowBlock?
    var columns: Int?
    let toolBar = UIToolbar()
    
    
    init(picker: UIPickerView? , items: [Any]?, columns: Int? , aSelectedStringBlock: SelectedRowBlock?) {
        
        super.init()
        
        self.picker = picker
        self.pickerData = items
        self.aSelectedBlock = aSelectedStringBlock
        
        self.picker?.delegate = self
        self.picker?.dataSource = self
        
        self.columns = columns
    }
    
    override init() {
        super.init()
    }
}

extension PickerViewCustomDataSource:UIPickerViewDelegate , UIPickerViewDataSource{
    @available(iOS 2.0, *)
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return columns ?? 0
    }
    
    
    // The number of columns of data
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return columns ?? 0
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return pickerData?.count ?? 0
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
            guard let safeData = self.pickerData?[row] as? String else{return ""}
            return safeData

    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if let block = self.aSelectedBlock{
            block( row , pickerData?[row]  as Any )
        }
    }
}
