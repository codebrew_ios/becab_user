//
//  SelectBrandCell.swift
//  Buraq24
//
//  Created by MANINDER on 04/10/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit

class SelectBrandCell: UICollectionViewCell {
    
    
    //MARK:- Outlets
    
    @IBOutlet var imgViewBrand: UIImageView!
    @IBOutlet var viewImgBrand: UIView!
    
    @IBOutlet var lblBrandName: UILabel!
    
    //MARK:- properties
    
    
    
    //MARK:- Functions
    
    func markAsSelected(selected : Bool , model : Brand) {
        
        viewImgBrand.cornerRadius(radius: viewImgBrand.bounds.width/2)
        selected == true ? viewImgBrand.addBorder(width: 2, color: UIColor.appTheme) :  viewImgBrand.addBorder(width: 0, color: .gray)
        
        imgViewBrand.sd_setImage(with: model.imageURL, completed: nil)
        
//        if selected{
//             imgViewBrand.image = #imageLiteral(resourceName: "ic_car_0")
//        }else {
//            imgViewBrand.image = #imageLiteral(resourceName: "kg")
//        }
        
    
        lblBrandName.text = /model.brandName
        lblBrandName.textColor =  selected == true ? UIColor.appTheme : .colorDarkGrayPopUp
    }
    
    
    func markProductSelected(selected : Bool , model : Product) {
        
        viewImgBrand.cornerRadius(radius: viewImgBrand.bounds.width/2)
        selected ? viewImgBrand.addBorder(width: 2, color: UIColor.appTheme) :  viewImgBrand.addBorder(width: 0, color: .gray)
        
        imgViewBrand.addBorder(width: 0, color: .colorDefaultSkyBlue)
        
        imgViewBrand.image = #imageLiteral(resourceName: "ic_car_0")
        //        imgViewBrand.sd_setImage(with: model.imageURL, completed: nil)
        lblBrandName.text = /model.productName
        lblBrandName.textColor =  selected == true ? UIColor.appTheme : .colorDarkGrayPopUp
    }
    
}
