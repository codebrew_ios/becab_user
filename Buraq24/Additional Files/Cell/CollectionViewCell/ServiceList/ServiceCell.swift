//
//  ServiceCell.swift
//  Buraq24
//
//  Created by MANINDER on 01/08/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit

class ServiceCell: UICollectionViewCell {
    
     //MARK:- OUTLETS
    @IBOutlet var imgViewService: UIImageView!
    
     //MARK:- FUNCTIONS
    func configureCell(item : ServiceType , selected : Bool) {
        imgViewService.image = selected ? item.serviceImageSelected : item.serviceImageUnSelected
    }
    
}
