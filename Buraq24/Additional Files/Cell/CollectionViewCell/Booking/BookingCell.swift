//
//  BookingCell.swift
//  Buraq24
//
//  Created by MANINDER on 07/09/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit

class BookingCell: UICollectionViewCell {
    
    //MARK:- Outlets
    @IBOutlet var lblServicePrice: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblBookingStatus: UILabel!
    @IBOutlet var lblBookingId: UILabel!
    
    @IBOutlet var imgViewMapTrack: UIImageView!
    
    //MARK:- Properties
    
    //MARK:- FUnctions
    
    func assignData(model : Order) {
        
        guard let orderId = model.orderToken else {return}
        lblBookingId.text = "Id : " + orderId
        lblBookingStatus.textColor = UIColor.statusGreen
        if model.orderStatus == .Scheduled || model.orderStatus == .DriverApprovalPending || model.orderStatus == .DriverApproval || model.orderStatus == .DriverSchCancelled || model.orderStatus == .DriverSchTimeOut  || model.orderStatus == .SystyemSchCancelled {
            
            lblBookingStatus.text = "Scheduled".localizedString
            
        } else if  model.orderStatus == .CustomerCancel || model.orderStatus == .DriverCancel {
            
            lblBookingStatus.text = "cancelled".localizedString
            lblBookingStatus.textColor = UIColor.statusRed
        }else if model.orderStatus == .ServiceComplete {
            
            lblBookingStatus.text = "completed".localizedString
        }
        else if model.orderStatus == .etokenCustomerConfirm{
            lblBookingStatus.text = "OrderStatus.Confirmed".localizedString
        }
        else if model.orderStatus == .Confirmed {
            
            lblBookingStatus.text = "OrderStatus.Confirmed".localizedString
        }
        else if model.orderStatus == .reached {
            
            lblBookingStatus.text = "OrderStatus.Reached".localizedString
        }
        else if model.orderStatus == .ServiceTimeout || model.orderStatus == .etokenTimeOut{
            lblBookingStatus.text = "etoken.Timeout".localizedString
        }
            
        else if model.orderStatus == .etokenCustomerPending{
            lblBookingStatus.text = "etoken.ApprovalPending".localizedString
        }
            
        else if model.orderStatus == .etokenSerCustCancel{
            lblBookingStatus.text = "etoken.Rejected".localizedString
        }
        else{
            lblBookingStatus.text  = model.orderStatus.rawValue
        }
        
        guard let lati = model.dropOffLatitude else{return}
        guard let long = model.dropOffLongitude else{return}
        
        guard let orderDate = model.orderLocalDate else {return}
        lblDate.text = orderDate.getBookingDateStr()
        
        guard let payment = model.payment else{return}
        
        if model.organisationCouponUserId != 0{
            lblServicePrice.text = "etoken.eToken".localizedString + " · \(/model.payment?.productQuantity)"
        }
        else{
            lblServicePrice.text =  "cash".localizedString + " - " + (/payment.finalCharge).getTwoDecimalFloat() + " " + "currency".localizedString
        }
        
        updateSemantic()
        
        //let strURL = Utility.shared.getGoogleMapImageURL(long:  String(long), lati: String(lati), width: Int(imgViewMapTrack.size.width), height: Int(imgViewMapTrack.size.height))
        let strURL = Utility.shared.getStaticMapWithMarker(latitude: lati, longitude: long)

        if let url = NSURL(string: strURL) {
            imgViewMapTrack.sd_setImage(with:url as URL, placeholderImage: nil, options: .refreshCached, progress: nil, completed: nil)
        }
    }
    
    private  func updateSemantic() {
        if  LanguageFile.shared.isLanguageRightSemantic() {
            lblBookingStatus.textAlignment = .left
            lblServicePrice.textAlignment = .left
            lblDate.textAlignment  = .right
            lblBookingId.textAlignment = .right
        }
    }
}
