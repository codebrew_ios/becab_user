//
//  ContactCell.swift
//  Buraq24
//
//  Created by MANINDER on 07/09/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit

typealias  EContact = (_ model : EmergencyContact) -> ()


class ContactCell: UITableViewCell {
    
    //MARK:-  Outlets
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblContactNumber: UILabel!
    @IBOutlet var imgViewContact: UIImageView!
    
    
     //MARK:-  Outlets
    var callBackBtn : EContact?
    var model : EmergencyContact?
    
     //MARK:-  View Life Cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Actions
    @IBAction func actionBtnCallPressed(_ sender: Any) {
        
        guard let contact = model , let callback = callBackBtn else{return}
         callback(contact)
    }
    
    func assignCellData(model : EmergencyContact) {
        
        self.model = model
        lblName.text = /model.name
        guard let phNumber = model.phoneNumber else{return}
        lblContactNumber.text = "\(phNumber)"
    }
}
