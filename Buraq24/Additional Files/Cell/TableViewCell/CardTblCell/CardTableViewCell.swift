//
//  CardTableViewCell.swift
//  SideDrawer
//
//  Created by Apple on 16/11/18.
//  Copyright © 2018 Codebrew Labs. All rights reserved.
//

import UIKit
import Stripe
import Material

enum CardType: String {
    case Unknown, Amex = "American Express", Visa, MasterCard, Diners = "Diners Club", Discover, JCB, Elo, Hipercard, UnionPay
    
    static let allCards = [Amex, Visa, MasterCard, Diners, Discover, JCB, Elo, Hipercard, UnionPay]
    
    var regex : String {
        switch self {
        case .Amex:
            return "^3[47][0-9]{5,}$"
        case .Visa:
            return "^4[0-9]{6,}([0-9]{3})?$"
        case .MasterCard:
            return "^(5[1-5][0-9]{4}|677189)[0-9]{5,}$"
        case .Diners:
            return "^3(?:0[0-5]|[68][0-9])[0-9]{4,}$"
        case .Discover:
            return "^6(?:011|5[0-9]{2})[0-9]{3,}$"
        case .JCB:
            return "^(?:2131|1800|35[0-9]{3})[0-9]{3,}$"
        case .UnionPay:
            return "^(62|88)[0-9]{5,}$"
        case .Hipercard:
            return "^(606282|3841)[0-9]{5,}$"
        case .Elo:
            return "^((((636368)|(438935)|(504175)|(451416)|(636297))[0-9]{0,10})|((5067)|(4576)|(4011))[0-9]{0,12})$"
        default:
            return ""
        }
    }
    
    var cardImage : UIImage? {
        switch self {
        case .Amex:
            return #imageLiteral(resourceName: "ic_amex.png")
        case .Visa:
            return #imageLiteral(resourceName: "ic_visa")
        case .MasterCard:
            return #imageLiteral(resourceName: "ic_mastercard")
        case .Diners:
            return #imageLiteral(resourceName: "ic_diners.png")
        case .Discover:
            return #imageLiteral(resourceName: "ic_discover.png")
        case .JCB:
            return #imageLiteral(resourceName: "ic_jcb.png")
        case .UnionPay:
            return #imageLiteral(resourceName: "ic_up.png")
        case .Hipercard:
            return #imageLiteral(resourceName: "ic_hiper.png")
        case .Elo:
            return #imageLiteral(resourceName: "ic_elo.png")
        default:
            return nil
        }
    }
    
}

protocol CardTableViewCellDelegate : class {
    func buttonClicked(index : Int?)
}

class CardTableViewCell: UITableViewCell {

    //MARK:- Outlet
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelCardNumber: UILabel!
    @IBOutlet weak var labelExpiry: UILabel!
    
    @IBOutlet weak var imageViewDefault: UIImageView!
    @IBOutlet weak var imageViewCard: UIImageView!
    
    @IBOutlet weak var buttonDelete: Button!
    
    //MARK:- Properties
    weak var delegate : CardTableViewCellDelegate?
    
    var row : Int? {
        didSet {
            buttonDelete.tag = /row
        }
    }
    
    var obj : CardDataModal? {
        didSet {
            
            labelName.text = obj?.nameOnCard
            labelCardNumber.text = "**** ***** **** " + /obj?.lastFourDigit
            
            let month = /obj?.expirationMonth >= 10 ? "\(/obj?.expirationMonth)" : "0\(/obj?.expirationMonth)"
            let year = "\(/obj?.expirationYear)".suffix(2)
            
            labelExpiry.text = month  + "/" + year
            
            //imageViewDefault.isHidden = obj?.defaultStatus == 0
            imageViewCard.image = CardType(rawValue: /obj?.typeOfCard)?.cardImage
        }
    }
    
    
    //MARK:- Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Button Selector
    @IBAction func buttonDeleteClicked(_ sender: Any) {
        delegate?.buttonClicked(index: /row)
    }
}
