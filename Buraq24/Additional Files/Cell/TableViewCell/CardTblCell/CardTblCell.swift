//
//  CardTblCell.swift
//  Sneni
//
//  Created by Apple on 06/09/19.
//  Copyright © 2019 Taran. All rights reserved.
//

import UIKit

class CardTblCell: UITableViewCell {
    
    var blockSelect: ((_ dtaCard : CardsModel?) -> ())?
    @IBOutlet weak var lblCardNo: UILabel!
    
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var lblHolderName: UILabel!
    
    @IBOutlet weak var lblDeleteBtn: UIButton!
    @IBOutlet weak var lbldate: UILabel!
    var getCrd : CardsModel?{
        didSet{
            updateUI()
        }
    }
    
    private func updateUI(){
        
        defer {
            lblCardNo.text = "XXXX XXXX XXXX " + /getCrd?.last4
            lblHolderName.text = getCrd?.card_holder_name
            lbldate.text = /getCrd?.exp_month + "/" + /getCrd?.exp_year
            btnCheck.setImage( /getCrd?.isSelected ? #imageLiteral(resourceName: "ic_checkbox_active") : #imageLiteral(resourceName: "ic_checkbox_inactive"), for: .normal)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        //        if selected{
        //            self.contentView.backgroundColor = UIColor.groupTableViewBackground
        //            btnCheck.setImage(#imageLiteral(resourceName: "ic_checkbox_active"), for: .normal)
        //
        //        }else{
        //            self.contentView.backgroundColor = UIColor.clear
        //            btnCheck.setImage(#imageLiteral(resourceName: "ic_checkbox_inactive"), for: .normal)
        //
        //        }
        
        // Configure the view for the selected state
    }
    
    @IBAction func btnDeleteAct(_ sender: UIButton) {
        guard let block = blockSelect else { return }
        block(getCrd)
        
    }
}
