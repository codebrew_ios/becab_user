//
//  OTPVC.swift
//  Buraq24
//
//  Created by Maninder on 30/07/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//



/// RepeatingTimer mimics the API of DispatchSourceTimer but in a way that prevents
/// crashes that occur from calling resume multiple times on a timer that is
/// already resumed (noted by https://github.com/SiftScience/sift-ios/issues/52



import UIKit

class OTPVC: UIViewController, UITextFieldDelegate, MyTextFieldDelegate{
    
    //MARK:- OUTLETS
    @IBOutlet var lblMobileNo: UILabel!
    @IBOutlet var txtFieldFirstDigit: MyTextField!
    @IBOutlet var txtFieldSecondDigit: MyTextField!
    @IBOutlet var txtFieldThirdDigit: MyTextField!
    @IBOutlet var txtFieldFourthDigit: MyTextField!
    @IBOutlet var viewBottom: UIView!
    @IBOutlet var constraintBottomView: NSLayoutConstraint!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnNext: UIButton!
    @IBOutlet var btnResend: UIButton!
    @IBOutlet var lblTimer: UILabel!
    @IBOutlet var stackView: UIStackView!
    
    //MARK:- PROPERTIES
    var sendOTP : SendOtp?
    var timer : Timer?
   
    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    
    var totalSecondLeft = 120
    
    //MARK:- VIEW CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        setMobileNumber()
        
        stackView.semanticContentAttribute = .forceLeftToRight
//        NSString *time = [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
    }
    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        addKeyBoardObserver()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        txtFieldFirstDigit.becomeFirstResponder()
      
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        removeKeyBoardObserver()
    }
    
    
    //MARK:- FUNCTIONS
  
    
    func setMobileNumber() {
        
        txtFieldFirstDigit.myDelegate = self
        txtFieldSecondDigit.myDelegate = self
        txtFieldThirdDigit.myDelegate = self
        txtFieldFourthDigit.myDelegate = self
        
        txtFieldFirstDigit.delegate = self
        txtFieldSecondDigit.delegate = self
        txtFieldThirdDigit.delegate = self
        txtFieldFourthDigit.delegate = self
        
        lblMobileNo.text = "enter_otp_on".localizedString + "\n" + /sendOTP?.countryCode + "-" + /sendOTP?.mobileNumber
        btnNext.setImage(#imageLiteral(resourceName: "ic_next_r").setLocalizedImage(), for: .normal)
        btnBack.setImage(#imageLiteral(resourceName: "Back").setLocalizedImage(), for: .normal)
        startTimer()
        
    }
    
    
    
    func startTimer() {
        totalSecondLeft = 120
        registerBackgroundTask()
        self.timer?.invalidate()
        self.timer = nil
        lblTimer.text = totalSecondLeft.formattedTimer()
        btnResend.isHidden = true
        lblTimer.isHidden = false
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self , selector: #selector(OTPVC.updateTimer), userInfo: nil, repeats: true)
    }
    
    
   @objc func updateTimer() {
        
        if totalSecondLeft > 0 {
            totalSecondLeft = totalSecondLeft - 1
            lblTimer.text = totalSecondLeft.formattedTimer()
        }else {
            endBackgroundTask()
            
            btnResend.isUserInteractionEnabled = true
            self.timer?.invalidate()
            btnResend.isHidden = false
            lblTimer.isHidden = true
            
         clearTxtFields()
        }
    }
    
    func addKeyBoardObserver() {
        
        NotificationCenter.default.addObserver(self,selector: #selector(keyboardWillShow),name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(keyboardWillhide),name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func removeKeyBoardObserver() {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    func clearTxtFields() {
        self.view.endEditing(true)
        txtFieldFirstDigit.text = ""
        txtFieldThirdDigit.text = ""
        txtFieldSecondDigit.text = ""
        txtFieldFourthDigit.text = ""
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            animateBottomView(true, height: keyboardHeight)
        }
    }
    @objc func keyboardWillhide(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            animateBottomView(false, height: keyboardHeight)
        }
    }
    
    func animateBottomView(_ isToShown : Bool , height : CGFloat) {
        UIView.animate(withDuration: 0.2) { [weak self] in
                self?.constraintBottomView.constant =  isToShown ?  -(height) :  -CGFloat(40)
            self?.view.layoutIfNeeded()
        }
    }
    func registerBackgroundTask() {
        
        backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
            self?.endBackgroundTask()
        }
        assert(backgroundTask != UIBackgroundTaskInvalid)
    }
    
    func endBackgroundTask() {
        print("Background task ended.")
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = UIBackgroundTaskInvalid
    }
    
 
    //MARK:- ACTIONS
    
    @IBAction func actionBtnBackPressed(_ sender: UIButton) {
        self.popVC()
    }
    
    
    @IBAction func actionBtnNextPressed(_ sender: Any) {
        let otp = /txtFieldFirstDigit.text + /txtFieldSecondDigit.text + /txtFieldThirdDigit.text + /txtFieldFourthDigit.text
        otp.count != 4 ? Alerts.shared.show(alert: "AppName".localizedString, message: "otp_validation_message".localizedString, type: .error )  : verifyOtp(otp: otp)
      
    }
    
    @IBAction func actionBtnResentPressed(_ sender: UIButton) {
        
        resendOtp(code: /sendOTP?.countryCode, number: /sendOTP?.mobileNumber)
    }
    
    @IBAction func actionBtnEditThisNumber(_ sender: UIButton) {
         self.popVC()
    }
}


extension OTPVC{
    //MARK: - TextField Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var shouldProcess = false //default to reject
        var shouldMoveToNextField = false //default to remaining on the current field
        let  insertStrLength = string.count
        
        if insertStrLength == 0 {
            
            shouldProcess = true //Process if the backspace character was pressed
            
        } else {
            if /textField.text?.count <= 1 {
                shouldProcess = true //Process if there is only 1 character right now
            }
        }
        
        if shouldProcess {
            
            var mString = ""
            if mString.count == 0 {
                
                mString = string
                shouldMoveToNextField = true
                
            } else {
                //adding a char or deleting?
                if(insertStrLength > 0){
                    mString = string
                    
                } else {
                    //delete case - the length of replacement string is zero for a delete
                    mString = ""
                }
            }
            
            //set the text now
            textField.text = mString
            
            if (shouldMoveToNextField&&textField.text?.count == 1) {
                
                if (textField == txtFieldFirstDigit) {
                    txtFieldSecondDigit.becomeFirstResponder()
                    
                } else if (textField == txtFieldSecondDigit){
                    txtFieldThirdDigit.becomeFirstResponder()
                    
                } else if (textField == txtFieldThirdDigit){
                    txtFieldFourthDigit.becomeFirstResponder()
                    
                } else {
                    txtFieldFourthDigit.resignFirstResponder()
                }
            }
        }
        return false
    }
    
    
    //MARK: - MyTextField Delegate
    func textFieldDidDelete() {
        
        if (txtFieldFirstDigit.hasText ||  txtFieldSecondDigit.hasText || txtFieldThirdDigit.hasText || txtFieldFourthDigit.hasText) {
            
            if ( txtFieldSecondDigit.hasText || txtFieldThirdDigit.hasText || txtFieldFourthDigit.hasText) {
                
                if (txtFieldThirdDigit.hasText || txtFieldFourthDigit.hasText) {
                    
                    if (txtFieldFourthDigit.hasText) {
                        
                    }else{
                        txtFieldThirdDigit.becomeFirstResponder()
                    }
                }else{
                    txtFieldSecondDigit.becomeFirstResponder()
                }
            }else{
                txtFieldFirstDigit.becomeFirstResponder()
            }
        }
    }
}

//MARK:- API
extension OTPVC{
    
    func verifyOtp(otp:String) {
        
        self.view.endEditing(true)
        
        let objR = LoginEndpoint.verifyOTP(otpCode: otp)
        
        objR.request(header:  ["language_id" : LanguageFile.shared.getLanguage() , "access_token" : /sendOTP?.accessToken ]) {[weak self] (response) in
            
            switch response {
                
            case .success(let data):
                
                guard let model = data as? LoginDetail else { return }
                self?.endBackgroundTask()
                if model.userDetails?.user?.name != "" {
                    
                    UDSingleton.shared.userData = model
                    let appDelegate = UIApplication.shared.delegate as? AppDelegate
                    appDelegate?.setHomeAsRootVC()
                    
                } else {
                    
                    guard let vc = R.storyboard.main.userProfileVC() else{return}
                    vc.loginDetail = model
                    self?.pushVC(vc)
                }
                break
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )

            }
        }
    }
    
        func resendOtp(code:String, number:String) {
            clearTxtFields()
            let sendOTP = LoginEndpoint.sendOtp(countryCode: code, phoneNum: number)
            sendOTP.request( header: ["language_id" : LanguageCode.English.rawValue]) { [weak self] (response) in
                switch response {
                    
                case .success(let data):
                  guard let model = data as? SendOtp else { return }
                  self?.sendOTP = model
                  Alerts.shared.show(alert: "AppName".localizedString, message: "otp_resent_successfully".localizedString, type: .success )
                  
                  self?.startTimer()
                    break
                case .failure(let strError):
                    Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
                }
            }
        }
    
    
}
