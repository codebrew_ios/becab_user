//
//  SettingsVC.swift
//  Buraq24
//
//  Created by MANINDER on 14/09/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit

class SettingsVC: BaseVC {
    
    //MARK:- Outlets
    
    @IBOutlet var btnSwitchPushNotification: UISwitch!
    @IBOutlet var btnSelectedLanguage: UIButton!
    @IBOutlet var lblChangeLanguage: UILabel!
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpPreviousValues()
    }
    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
    
    //MARK:- Functions
    func setUpPreviousValues() {
        self.btnSelectedLanguage.setTitle(self.getLanguageFromCode(code: /LanguageFile.shared.getLanguage()), for: .normal)
        if let status = UDSingleton.shared.userData?.userDetails?.notificationStatus  {
                    let valBool = status == .Off ? false : true
            btnSwitchPushNotification.setOn(valBool, animated: true)
        }
 
//        if  let languageCode = UserDefaultsManager.languageId{
//                guard let intVal = Int(languageCode) else {return}
//            //btnSelectedLanguage.setTitle(languageArry[intVal - 1], for: .normal)
//            switch intVal{
//            case 1:
//                btnSelectedLanguage.setTitle(languageArry[0], for: .normal)
//
//            case 3:
//                btnSelectedLanguage.setTitle(languageArry[1], for: .normal)
//
//            case 4 :
//                btnSelectedLanguage.setTitle(languageArry[3], for: .normal)
//
//            case  5:
//                btnSelectedLanguage.setTitle(languageArry[2], for: .normal)
//
//            case  6:
//                btnSelectedLanguage.setTitle(languageArry[4], for: .normal)
//
//            default :
//                btnSelectedLanguage.setTitle(languageArry[0], for: .normal)
//             }
//          }
      }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Actions
    
    @IBAction func actionBtnChangeLanguage(_ sender: UIButton) {
        
        if  let languageCode = UserDefaultsManager.languageId{
            guard let intVal = Int(languageCode) else {return}
            
//            switch intVal{
//            case 1:
//                showDropDown(view: sender)
           // case 2:
                showDropDown(view: sender)
                
//            default :
//                break;
//            }
        }
    }
    
    
    @IBAction func actionSwitchStateChanged(_ sender: UISwitch) {
        changeNotification()
        
    }
    
    
    @IBAction func actionBtnTermsAndConditions(_ sender: Any) {
//        guard let bookingVC = R.storyboard.sideMenu.webViewController() else{return}
//        bookingVC.stringUrl = APIBasePath.termsOfUse
//        self.navigationController?.pushViewController(bookingVC, animated: true)
    }
    
    
    @IBAction func actionBtnPrivacyPressed(_ sender: Any) {
//        guard let bookingVC = R.storyboard.sideMenu.webViewController() else{return}
//        bookingVC.stringUrl = APIBasePath.privacyUrl
//        self.navigationController?.pushViewController(bookingVC, animated: true)
    }
    
    @IBAction func actionBtnAboutUs(_ sender: Any) {
        
//        guard let bookingVC = R.storyboard.sideMenu.webViewController() else{return}
//        bookingVC.stringUrl = APIBasePath.aboutUs
//        self.navigationController?.pushViewController(bookingVC, animated: true)
        
//        guard let aboutUs = R.storyboard.sideMenu.aboutUsVC() else {
//            return
//        }
//        self.pushVC(aboutUs)
//
    }
    
    
    //MARK:- Functions
    
    func showDropDown(view : UIView) {
        
        Utility.shared.showDropDown(anchorView: view, dataSource: languageArry , width: 85, handler: {[weak self] (index, strValu) in
            
//            [String] = ["english".localizedString , "urdu".localizedString ,"arabic".localizedString ,"chinese".localizedString, "spanish".localizedString]
            
            if index == 1  {
                LanguageFile.shared.setLanguage(languageID: 6)
            }else  if index == 0 {
                LanguageFile.shared.setLanguage(languageID: 1)
            }
            self?.updatedAppData()
            
        })
        
    }
    
    //MARK:- API
    
    func changeNotification() {
        
        guard let status = UDSingleton.shared.userData?.userDetails?.notificationStatus  else{return}
        let statusString = status == .Off ? "1" : "0"
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        let objNoti = LoginEndpoint.updateNotifications(value: statusString)
        
        objNoti.request(header: ["access_token" :  token]) { [weak self] (response) in
             switch response {
             case .success(let data):
                
                if let userData = UDSingleton.shared.userData {
                    
                    userData.userDetails = data as? UserDetail
                    UDSingleton.shared.userData = userData
                    self?.setUpPreviousValues()
                    
                }
                
             case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )

            }
        }
    }
    
    
    func updatedAppData() {
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        let updateDataVC = LoginEndpoint.updateData(fcmID: UserDefaultsManager.fcmId)
        
        updateDataVC.request(isLoaderNeeded: true, header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token]) {  (response) in
            switch response {
                
            case .success(let data):
                guard let model = data as? Home else { return }
                
                if let userData = UDSingleton.shared.userData {
                    userData.userDetails = model.userDetail
                    userData.support = model.support
                    userData.services = model.services
                    UDSingleton.shared.userData = userData
                }
                
                ez.runThisInMainThread {
                    AppDelegate.shared().setHomeAsRootVC()
                }
                
            //self.lblLanguage.text = self.getLanguageFromCode(code: /selectedLanguage)
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
            }
        }
    }
    
}
