//
//  LandingVC.swift
//  Buraq24
//
//  Created by Maninder on 30/07/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit

let languageArry : [String] = ["LanguageName.English".localized, "LanguageName.Spanish".localized]

class LandingVC: UIViewController {
    
    //MARK:- OUTLETS
    
    @IBOutlet var viewGradient: UIView!
    @IBOutlet var txtFieldMobileNo: UITextField!
    @IBOutlet var imgViewCountryCode: UIImageView!
    @IBOutlet var lblCountryCode: UILabel!
    @IBOutlet var btnLanguage: UIButton!
    @IBOutlet var btnCheckBoxTerms: UIButton!
    @IBOutlet var btnNext: UIButton!
    
    @IBOutlet var lblChangeLanguage: UILabel!
    @IBOutlet var btnTermsAndConditions: UIButton!
    //MARK:- Properties
    
    
    //MARK:- VIEW CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        
    }
    
    
    //MARK:- ACTIONS
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        popVC()
    }
    
    @IBAction func actionBtnNextPressed(_ sender: UIButton) {
        let code = /lblCountryCode.text
        let number = /txtFieldMobileNo.text?.trimmed()
        
        if Validations.sharedInstance.validatePhoneNumber(phone: number) {
//            btnCheckBoxTerms.isSelected  == true ? self.sendOtp(code: code, number: number) : Alerts.shared.show(alert: "AppName".localizedString, message: "terms_and_conditions_validation_message".localizedString , type: .error )
// Toast.show(text: "terms_and_conditions_validation_message".localizedString, type: .error,changeBackground:true)
             self.sendOtp(code: code, number: number)
            }
    }
    
    
    @IBAction func actionBtnChangeLanguage(_ sender: UIButton) {
        
        if  let languageCode = UserDefaultsManager.languageId{
            guard let intVal = Int(languageCode) else {return}
            
            switch intVal{
            case 1:
                showDropDown(view: sender)
                
            case 3,4, 5, 6:
                showDropDown(view: lblChangeLanguage)
                
            default :
                break;
            }
        }
    }
    
    @IBAction func actionBtnCountryCode(_ sender: UIButton) {
        guard let countryPicker = R.storyboard.main.countryCodeSearchViewController() else{return}
        countryPicker.delegate = self
        self.presentVC(countryPicker)
    }
    
    @IBAction func actionBtnTermsAndConditions(_ sender: UIButton) {
//        guard let webView = R.storyboard.sideMenu.webViewController() else{return}
//        webView.stringUrl = APIBasePath.termsOfUse
//        pushVC(webView)
    }
    
    @IBAction func actionBtnCheckBox(_ sender: UIButton) {
        btnCheckBoxTerms.isSelected =  !btnCheckBoxTerms.isSelected
        
    }
 
    
    //MARK:- FUNCTIONS
    
    
    func setUpUI() {
        txtFieldMobileNo.delegate = self
        if  let languageCode = UserDefaultsManager.languageId{
            guard let intVal = Int(languageCode) else {return}
           // btnLanguage.setTitle(languageArry[intVal-1], for: .normal)
            /* switch intVal{
            case 1:
                 btnLanguage.setTitle(languageArry[0], for: .normal)
            case 3:
                 btnLanguage.setTitle(languageArry[1], for: .normal)
                
            case 4:
                btnLanguage.setTitle(languageArry[3], for: .normal)
            case  5:
                 btnLanguage.setTitle(languageArry[2], for: .normal)
            default :
                 btnLanguage.setTitle(languageArry[0], for: .normal)
            }*/
        }
        btnNext.setImage(#imageLiteral(resourceName: "ic_next_r").setLocalizedImage(), for: .normal)
        txtFieldMobileNo.setAlignment()
        //btnTermsAndConditions.setAttributedTitle( ("terms_and_conditions".localizedString).underlineWithColor(color: UIColor.colorDefaultSkyBlue) , for: .normal)
    }
    
    private  func showDropDown(view : UIView) {
        
        Utility.shared.showDropDown(anchorView: view, dataSource: languageArry , width: 85, handler: { (index, strValu) in
            
            print(languageArry)
            
            if index == 1  {
                LanguageFile.shared.setLanguage(languageID: 6)
            }else  if index == 0 {
                LanguageFile.shared.setLanguage(languageID: 1)
            }
            
            AppDelegate.shared().setWalkThroughAsRootVC()
        })
        
    }
    
//    @objc func createGradient(){
//        viewGradient.createCustomGradient(colors: [UIColor.white.cgColor , UIColor.colorGrayGredient.cgColor ], startPoint:  CGPoint(x: 0, y: 0), endPoint: CGPoint(x: 0, y: 0.5))
//    }
    
}

//MARK: - Country Picker Delegates
extension LandingVC: CountryCodeSearchDelegate {
    
    func didTap(onCode detail: [AnyHashable : Any]!) {
        imgViewCountryCode.image = UIImage(named:/(detail["code"] as? String)?.lowercased())
        lblCountryCode.text = /(detail["dial_code"] as? String)
        
    }
    
    func didSuccessOnOtpVerification(){
        
    }
}


extension LandingVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return true }
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        
        if  string == numberFiltered {
            if text == "" && string == "0" {
                return false
            }
            let newLength = text.length + string.length - range.length
            return newLength <= 15
        } else {
            return false
        }
    }
}

//MARK:- API
extension LandingVC {
    
    func sendOtp(code:String, number:String) {
        
        let sendOTP = LoginEndpoint.sendOtp(countryCode: code, phoneNum: number)
        sendOTP.request( header: ["language_id" : LanguageFile.shared.getLanguage()]) { (response) in
            switch response {
                
            case .success(let data):
                
                guard let model = data as? SendOtp else { return }
                model.countryCode = code
                model.mobileNumber = number
                
                guard let vc = R.storyboard.main.otpvC() else{return}
                vc.sendOTP = model
                self.pushVC(vc)
                
                break
                
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
                //Toast.show(text: strError, type: .error)
            }
        }
    }
}
