//
//  ContactUsVC.swift
//  Buraq24
//
//  Created by MANINDER on 07/09/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit
import MessageUI

class ContactUsVC: BaseVC,MFMailComposeViewControllerDelegate,UINavigationControllerDelegate {

    //MARK:- Outlets
    @IBOutlet var txtViewContactUS: PlaceholderTextView!
    
    //MARK:- Properties
    
    var mailComposer : MFMailComposeViewController?
    
    //MARK:- View life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
    
     //MARK:- Action
   
    @IBAction func actionBtnContactUsByphone(_ sender: Any) {
        
        self.callToNumber(number: customerCare1)
    }
    
    @IBAction func actionBtnEmail(_ sender: Any) {
        
        if MFMailComposeViewController .canSendMail() {
            
            mailComposer = MFMailComposeViewController()
            mailComposer?.setToRecipients([customerSupportEmail])
            mailComposer?.mailComposeDelegate = self
            guard let composer = mailComposer else { return }
             self.presentVC(composer)
        }else{
          Alerts.shared.show(alert: "AppName".localizedString, message:"email_not_configured".localizedString , type: .error )
        }
    }
    
    @IBAction func actionBtnSharePressed(_ sender: UIButton) {
        if (Validations.sharedInstance.validateContactUS(strReason: txtViewContactUS.text)) {
            let strTrimmed = txtViewContactUS.text.trimmed()
            self.view.endEditing(true)
            sendMessage(message: strTrimmed)
        }
    }
    
    //MARK:- Functions
    
    
    func setupUI() {
       
        txtViewContactUS.placeholder = "contact_us_msg_hint".localizedString as NSString
        
        txtViewContactUS.setAlignment()
    }
    
    
    //MARK:- Mail Composer Delegates
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismissVC(completion: nil)
    }
    
    
}

extension ContactUsVC {
    
    func sendMessage(message : String) {
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        let objContact = LoginEndpoint.contactUs(message: message)
        objContact.request(header: ["access_token" :  token]) {  [weak self] (response) in
            switch response {
            case .success(_):
                 Alerts.shared.show(alert: "", message: "alert.success".localizedString , type: .success )
                self?.popVC()
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
            }
        }
    }
}


