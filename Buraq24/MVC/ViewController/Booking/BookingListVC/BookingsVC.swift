//
//  BookingsVC.swift
//  Buraq24
//
//  Created by MANINDER on 25/09/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit


enum BookingListType : String {
    
    case Past = "Past"
    case Upcoming = "Upcoming"
    
    
}

class BookingsVC : BaseVC , RequestCancelDelegate {

    //MARK:-  Outlets
    @IBOutlet var btnPast: UIButton!
    @IBOutlet var btnUpcoming: UIButton!
    @IBOutlet var viewScroll: UIScrollView!
    @IBOutlet var viewMovingLine: UIView!
    
    @IBOutlet var btnBackBase: UIButton!
    @IBOutlet var stackView: UIStackView!
    @IBOutlet var constraintCentreMovingLine: NSLayoutConstraint!
    
    @IBOutlet var viewPast: UIView!
    @IBOutlet var viewUpcoming: UIView!
    
    @IBOutlet var collectionViewPast: UICollectionView!
    @IBOutlet var collectionViewUpcoming: UICollectionView!
    
    @IBOutlet var lblNoBookingsPast: UILabel!
    @IBOutlet var lblNoBookingsUpcoming: UILabel!
    
    //MARK:- Properties
    var collectionViewPastDataSource : CollectionViewDataSource?
  lazy  var arrPastOrder : [Order] = [Order]()
    
    var collectionViewUpComingDataSource : CollectionViewDataSource?
   lazy  var arrComingOrder : [Order] = [Order]()
    lazy var refreshControlPast = UIRefreshControl()
    lazy var refreshControlUpcoming = UIRefreshControl()
    
    //MARK:- view Life cycle
    var listType : BookingListType = .Past
    var isRightToLeft = false
    
    
    var pagingPast : Int = 0
    var pagingUpcoming : Int = 0
    var isAllItemPastFetched : Bool = false
    var isAllItemUpcomingFetched : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
          setUpUI()
        swapStackViews()
      configurePastCollectionView()
       configureUpcomingCollectionView()
        getPastBookingList()
        getUpcomingBookingList()
        
    }
    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
    
    //MARK:- Actions
    
    @IBAction func actionBackPressed(_ sender: Any) {
        popVC()
    }
    
    @IBAction func actionBtnPastPressed(_ sender: Any) {
        viewScroll.scrollRectToVisible(CGRect(x: 0 , y: 0, width: ez.screenWidth, height: viewScroll.bounds.height), animated: true)
        listType = .Past
        animateSwipeControl(type: listType)
    }
    
    @IBAction func actionBtnUpcomingPressed(_ sender: Any) {
        
        viewScroll.scrollRectToVisible(CGRect(x: ez.screenWidth , y: 0, width: ez.screenWidth, height: viewScroll.bounds.height), animated: true)
//         listType = .Upcoming
//        animateSwipeControl(type: listType)
    }

    //MARK:- Functions
    
    func setUpUI() {
        viewScroll.delegate = self
        var imgBack = #imageLiteral(resourceName: "Back")
        if  let languageCode = UserDefaultsManager.languageId{
            guard let intVal = Int(languageCode) else {return}
            switch intVal{
            case 3, 5:
                imgBack = #imageLiteral(resourceName: "Back_New")
            default :
                imgBack = #imageLiteral(resourceName: "Back")
            }
        }
        btnBackBase.setImage(imgBack.setLocalizedImage(), for: .normal)
        configureRefreshControl()
    }
    
    func configureRefreshControl() {
        
        refreshControlPast.addTarget(self, action: #selector(BookingsVC.refreshList(refresh:)), for: UIControlEvents.valueChanged)
        refreshControlPast.tintColor = #colorLiteral(red: 0.9607843137, green: 0.568627451, blue: 0, alpha: 1)
        collectionViewPast.refreshControl = refreshControlPast
        
        refreshControlUpcoming.addTarget(self, action: #selector(BookingsVC.refreshList(refresh:)), for: UIControlEvents.valueChanged)
        refreshControlUpcoming.tintColor = #colorLiteral(red: 0.9607843137, green: 0.568627451, blue: 0, alpha: 1)
        collectionViewUpcoming.refreshControl = refreshControlUpcoming
    }
    
    
    func toggleBtnStates() {
        
        btnPast.isSelected = listType == .Past ? true : false
        btnUpcoming.isSelected = listType == .Upcoming ? true : false
        
    }
    
    func animateSwipeControl(type : BookingListType) {
        UIView.animate(withDuration: 0.4, animations: { [weak self] in
            if  self?.isRightToLeft == true {
                self?.constraintCentreMovingLine.constant = self?.listType == .Past ? 0 : -120
            }else{
                self?.constraintCentreMovingLine.constant = self?.listType == .Past ? 0 : 120
            }
            self?.toggleBtnStates()
            self?.view.layoutIfNeeded()
        }) { (success) in
            
        }
    }
    
    func swapStackViews() {
        if isRightToLeft == true {
            if let myView = stackView.subviews.first {
                stackView.removeArrangedSubview(myView)
                stackView.setNeedsLayout()
                stackView.layoutIfNeeded()
                stackView.insertArrangedSubview(myView, at: 1)
                stackView.setNeedsLayout()
            }
        }
    }
    
    func showBookingDetails(order : Order , type : TabType) {
        guard let detailsVC = R.storyboard.sideMenu.bookingDetailVC() else{return}
          detailsVC.order = order
         detailsVC.type = type
        detailsVC.delegateCancellation = self
         self.navigationController?.pushViewController(detailsVC, animated: true)
    }
    func showEtokenBookingDetails(order : Order , type : TabType) {
        guard let detailsVC = R.storyboard.drinkingWater.drinkingWaterETokenDeliver() else {return}
        detailsVC.order = order
        detailsVC.type = type
        self.navigationController?.pushViewController(detailsVC, animated: true)
    }
    
    //MARK:-  Delegates
    func didSuccessOnCancelRequest() {
        isAllItemUpcomingFetched = false
        pagingUpcoming = 0
        getUpcomingBookingList()
    }
}

//MARK:- Scroll View Delegates

extension  BookingsVC : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        listType = pageNumber == 0 ? .Past : .Upcoming
        animateSwipeControl(type: listType)
    }
}
//MARK:- API

extension BookingsVC {

  func configurePastCollectionView() {
    
    let configureCellBlock : ListCellConfigureBlock = {(cell, item, indexPath) in
        if let cell = cell as? BookingCell , let model = item as? Order {
            cell.assignData(model: model)
        }
    }
    
    let willDisplayCell : WillDisplay = { [weak self]  (indexPath) in
        if indexPath.row + 1 == self?.arrPastOrder.count && !(/self?.isAllItemPastFetched)  {
            self?.pagingPast =   (/self?.pagingPast + 1) * 10
            self?.getPastBookingList()
        }
    }
    
    let didSelectBlock : DidSelectedRow = { [weak self] (indexPath, cell, item) in
        if let _ = cell as? BookingCell , let item = item as? Order {
            self?.showBookingDetails(order: item, type: .Past)
        }
    }
   
     let height = ez.screenWidth*62/100
    
     collectionViewPastDataSource =  CollectionViewDataSource(items:  arrPastOrder, collectionView: collectionViewPast, cellIdentifier: R.reuseIdentifier.bookingCell.identifier, cellHeight: height, cellWidth: ez.screenWidth , configureCellBlock: configureCellBlock )
    
     collectionViewPastDataSource?.willDisplay = willDisplayCell
    collectionViewPastDataSource?.aRowSelectedListener = didSelectBlock
    
    collectionViewPast.delegate = collectionViewPastDataSource
    collectionViewPast.dataSource = collectionViewPastDataSource
    collectionViewPast.reloadData()
 }
    
    @objc func refreshList(refresh : UIRefreshControl) {

        refresh.beginRefreshing()
        if refresh == refreshControlPast {
            isAllItemPastFetched = false
            pagingPast = 0
            getPastBookingList()
        }else{
            isAllItemUpcomingFetched = false
            pagingUpcoming = 0
             getUpcomingBookingList()
        }
    }
    
    func getPastBookingList() {
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        
        let bookingList = BookServiceEndPoint.history(skip: pagingPast, take: 10, type: 1)
        
        bookingList.request(header: ["access_token" :  token]) { [weak self] (response) in
            switch response {
            case .success(let data):
                if let arrBookings = data as? [Order] {
                    
                    if self?.pagingPast == 0  {
                        self?.refreshControlPast.endRefreshing()
                         self?.isAllItemPastFetched = false
                        self?.arrPastOrder.removeAll()
                    }
                    
                    if arrBookings.count == 0 || arrBookings.count < 10 {
                        self?.isAllItemPastFetched = true
                    }
                    
                    self?.arrPastOrder.append(contentsOf: arrBookings)
                    
                    ez.runThisInMainThread {
                        self?.collectionViewPastDataSource?.items = self?.arrPastOrder
                        self?.collectionViewPast.reloadData()
                        self?.lblNoBookingsPast.isHidden =  self?.arrPastOrder.count == 0 ? false : true
                    }
                 
                }
            case .failure(let strError):
                
                self?.isAllItemPastFetched = false
                self?.refreshControlPast.endRefreshing()

                if  self?.pagingPast != 0 {
                    self?.pagingPast =  /self?.pagingPast - 1
                }
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )

            }
        }
    }
}

extension BookingsVC {
    
    func configureUpcomingCollectionView() {
        
        let configureCellBlock : ListCellConfigureBlock = {(cell, item, indexPath) in
            if let cell = cell as? BookingCell , let model = item as? Order {
                cell.assignData(model: model)
            }
        }
        
        let didSelectBlock : DidSelectedRow = { [weak self] (indexPath, cell, item) in
            if let _ = cell as? BookingCell , let item = item as? Order {
                item.organisationCouponUserId == 0 ?  self?.showBookingDetails(order: item, type: .Upcoming) : self?.showEtokenBookingDetails(order: item, type: .Upcoming)
            }
        }
        let willDisplayCell : WillDisplay = { [weak self]  (indexPath) in
       
            if indexPath.row + 1 == self?.arrComingOrder.count && !(/self?.isAllItemUpcomingFetched)  {
                self?.pagingUpcoming =   /self?.pagingUpcoming + 1
                self?.getPastBookingList()
            }
        }
        
        let height = ez.screenWidth*62/100
        
        collectionViewUpComingDataSource =  CollectionViewDataSource(items:  arrComingOrder, collectionView: collectionViewUpcoming, cellIdentifier: R.reuseIdentifier.bookingCell.identifier, cellHeight: height, cellWidth: ez.screenWidth , configureCellBlock: configureCellBlock )
      
        collectionViewUpComingDataSource?.aRowSelectedListener = didSelectBlock
        collectionViewUpComingDataSource?.willDisplay = willDisplayCell

        collectionViewUpcoming.delegate = collectionViewUpComingDataSource
        collectionViewUpcoming.dataSource = collectionViewUpComingDataSource
        
        collectionViewUpcoming.reloadData()
    }
    
    func getUpcomingBookingList() {
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        
        let bookingList = BookServiceEndPoint.history(skip: pagingUpcoming, take: 10, type: 2)
        bookingList.request(header: ["access_token" :  token]) { [weak self] (response) in
            switch response {
                
            case .success(let data):
                
                if let arrBookings = data as? [Order] {
                  
                    if self?.pagingUpcoming == 0  {
                        self?.refreshControlUpcoming.endRefreshing()

                        self?.isAllItemUpcomingFetched = false
                        self?.arrComingOrder.removeAll()
                    }
                    
                    if arrBookings.count == 0 || arrBookings.count < 10 {
                         self?.isAllItemUpcomingFetched = true
//                        if  self?.pagingUpcoming != 0 {
//                            self?.pagingUpcoming =  /self?.pagingUpcoming - 1
//                        }
                    }
                    
                    self?.arrComingOrder.append(contentsOf: arrBookings)
                    self?.collectionViewUpComingDataSource?.items = self?.arrComingOrder
                    self?.collectionViewUpcoming.reloadData()
                    
                    self?.lblNoBookingsUpcoming.isHidden =  self?.arrComingOrder.count == 0 ? false : true
                    
                }
            case .failure(let strError):
                
                self?.refreshControlUpcoming.endRefreshing()
                self?.isAllItemUpcomingFetched = false
               if  self?.pagingUpcoming != 0 {
                 self?.pagingUpcoming =  /self?.pagingUpcoming - 1
               }
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
            }
        }
    }
    
}



