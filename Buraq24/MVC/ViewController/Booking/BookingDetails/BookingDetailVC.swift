//
//  BookingDetailVC.swift
//  Buraq24
//
//  Created by MANINDER on 10/09/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit

enum TabType : String  {
    case Past = "Past"
    case Upcoming = "Upcoming"
}

class BookingDetailVC: BaseVC {

    //MARK:- Outlets
    @IBOutlet var lblDropOffText: UILabel!

    @IBOutlet var imgViewMap: UIImageView!
    @IBOutlet var lblOrderToken: UILabel!
    @IBOutlet var lblOrderStatus: UILabel!
    
    @IBOutlet var lblOrderPrice: UILabel!
    @IBOutlet var lblOrderDate: UILabel!
    @IBOutlet var lblDropLocationAddress: UILabel!
    
    @IBOutlet var lblComment: UILabel!
    
    @IBOutlet var viewDriver: UIView!
    @IBOutlet var constraintDriverHeight: NSLayoutConstraint!
    
    @IBOutlet var lblDriverName: UILabel!
    @IBOutlet var imgViewRating: UIImageView!
    @IBOutlet var imgViewDriver: UIImageView!
    
    @IBOutlet var lblServiceName: UILabel!
    @IBOutlet var lblOrderDetails: UILabel!
    @IBOutlet var lblBaseFair: UILabel!
    @IBOutlet var lblTotalprice: UILabel!
    @IBOutlet var lblTax: UILabel!
    
    @IBOutlet var lblOtherPrice: UILabel!
    
    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var constraintTopPricingView: NSLayoutConstraint!
    @IBOutlet var constraintDateViewHeight: NSLayoutConstraint!
    
    @IBOutlet var lblStartDate: UILabel!
    @IBOutlet var lblEndDate: UILabel!
    @IBOutlet var viewTime: UIView!
    @IBOutlet weak var btnCall: UIButton!
    
    //MARK:- Properties
    
    var order : Order?
    var type : TabType = .Past
    var delegateCancellation: RequestCancelDelegate?
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getOrderDetails()
        // Do any additional setup after loading the view.
        
        if order?.serviceId == 4 {
            lblDropOffText.text = R.string.localizable.delivery_location()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
    
     //MARK:- Action
    
    @IBAction func callDriver(_ sender: Any) {
    
        let val = "\(/self.order?.driverAssigned?.driverCountryCode)\(/self.order?.driverAssigned?.driverPhoneNumber)"
        self.callToNumber(number: val)
    }
    
    @IBAction func actionBtnCancelPressed(_ sender: UIButton) {
         showCancellationFormVC()
    }
    
    //MARK:- Functions
    
    func assignBookingData() {
        
        guard let orderDetail = order  else {return}
       
        btnCancel.isHidden = type == .Past
        if let orderToken = orderDetail.orderToken {
            
             lblOrderToken.text = "Id: " + orderToken
            
            guard let lati = orderDetail.dropOffLatitude else{return}
            guard let long = orderDetail.dropOffLongitude else{return}
            
            let strURL = Utility.shared.getStaticMapWithPolyLine(pickUpLat: /orderDetail.pickUpLatitude, pickUpLng: /orderDetail.pickUpLongitude, dropLat: lati, dropLng: long)
            //let strURL = Utility.shared.getGoogleMapImageURL(long:  String(long), lati: String(lati), width: Int(imgViewMap.size.width), height: Int(imgViewMap.size.height))
           
            if let url = NSURL(string: strURL) {
                imgViewMap.sd_setImage(with: url as URL , completed: nil)
            }
            
            lblDropLocationAddress.text = /orderDetail.dropOffAddress
            
            guard let orderDate = orderDetail.orderLocalDate else {return}
            lblOrderDate.text = orderDate.getBookingDateStr()
            
            guard let service = UDSingleton.shared.getService(categoryId: orderDetail.serviceId) else {return}
            guard let payment = orderDetail.payment else{return}
            
            if /service.serviceCategoryId == 2 || /service.serviceCategoryId == 4 {
                lblServiceName.text = /orderDetail.orderProductDetail?.productBrandName
            }else{
                lblServiceName.text = /service.serviceName
            }
            
            let orderDet  = /service.serviceCategoryId > 3 ? (/orderDetail.orderProductDetail?.productName)   : (/orderDetail.orderProductDetail?.productName + " × " +  String(/payment.productQuantity))
            
            lblOrderDetails.text = orderDet
            
            if orderDetail.organisationCouponUserId != 0 {
                lblOrderPrice.text =  "E-Tokens - " + "\(/orderDetail.payment?.productQuantity)"
            }
            else{
                
                lblOrderPrice.text =  "cash".localizedString + " - " + (/payment.finalCharge).getTwoDecimalFloat() + " " + "currency".localizedString
            }
            
            lblTotalprice.text = (/payment.finalCharge).getTwoDecimalFloat() + " " + "currency".localizedString
           
            var total : Double = 0.0
            
            if let initalCharge = Double(/payment.initalCharge) {
                total =  initalCharge
            }
            
            if let adminCharge = Double(/payment.adminCharge) {
                total =  total + adminCharge
            }
            
            lblBaseFair.text =  "\(total)".getTwoDecimalFloat() + " " + "currency".localizedString
            lblTax.text = "0.0".getTwoDecimalFloat() + " " + "currency".localizedString
            lblOtherPrice.text = "0.0".getTwoDecimalFloat() + " " + "currency".localizedString

            if let rating = orderDetail.rating?.ratingGiven {
                
             viewDriver.isHidden = false
                guard let driver = orderDetail.driverAssigned else{return}

                lblComment.text = orderDetail.rating?.comment
                lblDriverName.text = /driver.driverName
                guard let driverimage = driver.driverProfilePic else{return}
                if let url = URL(string: driverimage) {
                    imgViewDriver.sd_setImage(with: url , completed: nil)
                }
                imgViewRating.setRating(rating: rating)
                
            }else {
                viewDriver.isHidden = true
                constraintTopPricingView.constant = -60
            }
            lblOrderStatus.textColor = UIColor.statusGreen
            if orderDetail.orderStatus == .Scheduled || orderDetail.orderStatus == .DriverApprovalPending || orderDetail.orderStatus == .DriverApproval || orderDetail.orderStatus == .DriverSchCancelled || orderDetail.orderStatus == .DriverSchTimeOut  || orderDetail.orderStatus == .SystyemSchCancelled {
                
                lblOrderStatus.text = "Scheduled".localizedString
                
                 constraintTopPricingView.constant = 60
                
                lblDriverName.text = /orderDetail.driverAssigned?.driverName
                guard let driverimage = orderDetail.driverAssigned?.driverProfilePic else{return}
                if let url = URL(string: driverimage) {
                    imgViewDriver.sd_setImage(with: url , completed: nil)
                }
                imgViewRating.isHidden = true
                viewDriver.isHidden = false
                
            } else if  orderDetail.orderStatus == .CustomerCancel || orderDetail.orderStatus == .DriverCancel {
                lblOrderStatus.text = "cancelled".localizedString
                lblOrderStatus.textColor = UIColor.statusRed
                
            }else if orderDetail.orderStatus == .ServiceComplete {
                
                lblOrderStatus.text = "completed".localizedString
            }
                
            else if orderDetail.orderStatus == .etokenCustomerConfirm{
                lblOrderStatus.text = "OrderStatus.Confirmed".localizedString
            }
            else if orderDetail.orderStatus == .Confirmed {
                
                lblOrderStatus.text = "OrderStatus.Confirmed".localizedString
            }
            else if order?.orderStatus == .reached {
                
                lblOrderStatus.text = "OrderStatus.Reached".localizedString
            }
            else if orderDetail.orderStatus == .ServiceTimeout || orderDetail.orderStatus == .etokenTimeOut{
                
                lblOrderStatus.text = "etoken.Timeout".localizedString
            }
                
            else if orderDetail.orderStatus == .etokenCustomerPending{
                lblOrderStatus.text = "etoken.ApprovalPending".localizedString
            }
                
            else if orderDetail.orderStatus == .etokenSerCustCancel{
                lblOrderStatus.text = "etoken.Rejected".localizedString
            }
                
            else if orderDetail.orderStatus == .Ongoing {
                
                lblOrderStatus.text  = orderDetail.orderStatus.rawValue
                
                constraintTopPricingView.constant = 60
                
                lblDriverName.text = /orderDetail.driverAssigned?.driverName
                guard let driverimage = orderDetail.driverAssigned?.driverProfilePic else{return}
                if let url = URL(string: driverimage) {
                    imgViewDriver.sd_setImage(with: url , completed: nil)
                }
                imgViewRating.isHidden = true
                viewDriver.isHidden = false
            }
            else{
                lblOrderStatus.text  = orderDetail.orderStatus.rawValue
            }
            
        }
        updateSemantic()
        
        if let orderDates = orderDetail.orderDates , let orderStartDate =  orderDates.startedDate  {
            
            viewTime.isHidden = false
           constraintDateViewHeight.constant = 60
            
            guard let orderCompleteDate = orderDates.completedDate else {return}
            
            lblStartDate.text = orderStartDate.getBookingDateStr()
            lblEndDate.text = orderCompleteDate.getBookingDateStr()

            self.lblEndDate.isHidden =  false
            
            if   orderDetail.orderStatus.rawValue == "Ongoing" || orderDetail.orderStatus.rawValue == "DPending" || orderDetail.orderStatus.rawValue == "Scheduled"  {
                self.lblEndDate.isHidden = true
            }
            
        }else{
            constraintDateViewHeight.constant = 0
            viewTime.isHidden = true
        }
        
        if  orderDetail.orderStatus == .Confirmed || orderDetail.orderStatus == .reached || orderDetail.orderStatus == .CustomerCancel || orderDetail.orderStatus == .DriverCancel {
             viewTime.isHidden = true
            constraintDateViewHeight.constant = 0
        }
     }
    
    private  func updateSemantic() {
        
        if  LanguageFile.shared.isLanguageRightSemantic() {
            lblOrderPrice.textAlignment = .left
            lblOrderStatus.textAlignment = .left
            lblOrderToken.textAlignment  = .right
            lblOrderDate.textAlignment = .right
        }
    }
}

extension BookingDetailVC : RequestCancelDelegate {
    
        func showCancellationFormVC() {
            
            guard let orderId = order?.orderId , let formCancelation = R.storyboard.bookService.cancellationVC() else { return }
            formCancelation.view.backgroundColor = UIColor.colorDarkGrayPopUp
            formCancelation.modalPresentationStyle = .overCurrentContext
            formCancelation.modalTransitionStyle = .crossDissolve
            formCancelation.orderId = orderId
            formCancelation.delegateCancellation = self
            presentVC(formCancelation, true)
            

        }
    
        func didSuccessOnCancelRequest() {
            if let orderId = order?.orderId {
                let nc = NotificationCenter.default
                nc.post(name: Notification.Name(rawValue: LocalNotifications.nCancelFromBookinScreen.rawValue), object: "\(orderId)")
            }


            self.delegateCancellation?.didSuccessOnCancelRequest()
            self.popVC()
            

        }
    }

//MARK:- API

extension BookingDetailVC {
    
    func getOrderDetails() {
        
        guard let orderID = order?.orderId else {  return }
         let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        
        let objDetail = BookServiceEndPoint.orderDetails(orderID: orderID)
        
        objDetail.request(header: ["access_token" :  token]) { [weak self] (response) in
            switch response {
                
            case .success(let data):
                
                if let order = data as? Order {
                    self?.order = order
                    self?.assignBookingData()
                }
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )

                //Toast.show(text: strError, type: .error)
            }
       }
    }
    
}

