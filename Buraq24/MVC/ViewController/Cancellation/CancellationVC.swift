//
//  CancellationVC.swift
//  Buraq24
//
//  Created by MANINDER on 03/08/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.


import UIKit

protocol RequestCancelDelegate {
    func didSuccessOnCancelRequest()
    
}


class CancellationVC: UIViewController {
    
    //MARK:- OUTLETS
    @IBOutlet var viewOuter: UIView!
    @IBOutlet var txtViewCancelReson: PlaceholderTextView!
    
    
    //MARK:- PROPERTIES
    
    var orderId : Int?
    var delegateCancellation: RequestCancelDelegate?
    
    
    //MARK:- VIEW CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
       setupUI()
        setUpNotification()
    }

//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
    
    //MARK:- ACTIONS
 
    @IBAction func actionBtnSubmitPressed(_ sender: Any) {
        if (Validations.sharedInstance.validateCancellingReason(strReason: txtViewCancelReson.text)) {
            let strTrimmed = txtViewCancelReson.text.trimmed()
            cancelRequest(cancelReson: strTrimmed)
        }
    }
     //MARK:- FUNCTIONS
    
    func setupUI() {
        txtViewCancelReson.placeholder = "write_your_msg_here".localizedString as NSString
        txtViewCancelReson.setAlignment()
    }
    
    func setUpNotification() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(CancellationVC.dismissPopUp), name: Notification.Name(rawValue: LocalNotifications.DismissCancelPopUp.rawValue), object: nil)
    }
    
   
    func cancelPopUp() {
        dismissVC(completion: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: LocalNotifications.DismissCancelPopUp.rawValue), object: nil)
    }
    
    
    @objc func dismissPopUp(notifcation : Notification) {
        
        if let objOrder = notifcation.object as? Order {
            if /objOrder.orderId == /orderId {
                cancelPopUp()
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let firstTouch = touches.first {
            let hitView = self.view.hitTest(firstTouch.location(in: self.view), with: event)
            if hitView === viewOuter {
                 cancelPopUp()
            }
        }
    }
}
extension CancellationVC {
    func cancelRequest(cancelReson : String) {
         let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        let objCancel = BookServiceEndPoint.cancelRequest(orderId: /orderId, cancelReason: txtViewCancelReson.text)
        objCancel.request(header: ["access_token" :  token]) {  [weak self] (response) in
            switch response {
            case .success(_):
                self?.delegateCancellation?.didSuccessOnCancelRequest()
                 self?.cancelPopUp()
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
            }
        }
    }
}

