//
//  RightMenuViewController.swift
//  Buraq24
//
//  Created by MANINDER on 21/09/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit

class RightMenuViewController: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet var viewTopSupport: UIView!
    @IBOutlet var collectionViewSupport: UICollectionView!
    
    //MARK:- Properties
    var collectionViewDataSource : CollectionViewDataSource?
    lazy var supportList : [Support] = [Support]()
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.perform(#selector(createGradient), with: nil, afterDelay: 0.0)
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let supports = UDSingleton.shared.userData?.support {
            supportList.removeAll()
            supportList.append(contentsOf: supports)
        }
        configureCollectionView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        collectionViewSupport.reloadData()
    }
    
    
    //MARK:- Functions
    
    @objc func createGradient(){
        viewTopSupport.createCustomGradient(colors: [UIColor.colorSkyBlueLightGradient.cgColor , UIColor.colorSkyBlueDarkGradient.cgColor ], startPoint:  CGPoint(x: 0, y: 0), endPoint: CGPoint(x: 0.5, y: 1))
    }
    
    private  func configureCollectionView() {
        
        let configureCellBlock : ListCellConfigureBlock = {(cell, item, indexPath) in
            if let cell = cell as? SupportOption, let model = item as? Support {
                cell.configureCell(model: model)
            }
        }
        
        let didSelectCellBlock : DidSelectedRow = {  (indexPath , cell, item) in
            
            if let _ = cell as? SupportOption {
                Alerts.shared.show(alert: "AppName".localizedString, message: "coming_soon".localizedString , type: .error )

                //Toast.show(text: "coming_soon".localizedString, type: .error)
            }
        }
        
        collectionViewDataSource =  CollectionViewDataSource(items: supportList, collectionView: collectionViewSupport, cellIdentifier: R.reuseIdentifier.supportOption.identifier, cellHeight: CGFloat(BookingPopUpFrames.WidthSideMenu/2) + 20, cellWidth: CGFloat(BookingPopUpFrames.WidthSideMenu/2) , configureCellBlock: configureCellBlock )
        
        collectionViewDataSource?.aRowSelectedListener = didSelectCellBlock
        
        collectionViewSupport.delegate = collectionViewDataSource
        collectionViewSupport.dataSource = collectionViewDataSource
        
        collectionViewSupport.reloadData()
    }
    
}
