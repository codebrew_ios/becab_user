//
//  MenuViewController.swift
//  SideMenuExample
//
//  Created by kukushi on 11/02/2018.
//  Copyright © 2018 kukushi. All rights reserved.
//

import UIKit


class MenuViewController: UIViewController {
    
    //MARK:- Outlets
    
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var imgViewUser: UIImageView!
    @IBOutlet var lblRatingCount: UILabel!
    @IBOutlet var viewUserInfo: UIView!
    @IBOutlet var lblPhoneNumber: UILabel!
    @IBOutlet var imgViewUserRating: UIImageView!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.separatorStyle = .none
        }
    }
    @IBOutlet weak var selectionTableViewHeader: UILabel!
    @IBOutlet weak var selectionMenuTrailingConstraint: NSLayoutConstraint!
    
    
    //MARK:- Properties
    //let arrOptions : [SideMenuOptions] =   [ .Bookings ,.ETokens, .Promotions , .Payments , .Referral , .EmergencyContacts , .Settings , .Contactus , .SignOut]
    //change Virat
//     let arrOptions : [SideMenuOptions] =   [ .Bookings , .Payments , .Settings , .Contactus , .SignOut]
    let arrOptions : [SideMenuOptions] =   [ .Bookings, .Settings , .Contactus , .SignOut]
    
    var tableDataSource : TableViewDataSource?

    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
         setUpUI()
        assignUserInfo()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setUpUI()
    }
    
    //MARK:- Actions
    
    @IBAction func actionBtnEditProfile(_ sender: UIButton) {
//        guard let editVC = R.storyboard.sideMenu.editProfileVC() else{return}
//        self.navigationController?.pushViewController(editVC, animated: true)
    }
    
   //MARK:- Functions
    
    private func configureView() {
//        self.perform(#selector(createGradient), with: nil, afterDelay: 0.0)
        configureTableView()
    }
    
    @objc func createGradient(){
        viewUserInfo.createCustomGradient(colors: [UIColor.colorSkyBlueLightGradient.cgColor , UIColor.colorSkyBlueDarkGradient.cgColor ], startPoint:  CGPoint(x: 0, y: 0), endPoint: CGPoint(x: 2, y: 1))
    }
    
    func setUpUI() {
        imgViewUser.cornerRadius(radius: imgViewUser.frame.size.width/2)
    }
    
    func assignUserInfo() {
        
        guard let userName = UDSingleton.shared.userData?.userDetails?.user?.name else{return}
        lblUserName.text = userName
        if let urlImage = UDSingleton.shared.userData?.userDetails?.profilePic {
            imgViewUser.sd_setImage(with: URL(string : urlImage), placeholderImage: #imageLiteral(resourceName: "ic_user"), options: .refreshCached, progress: nil, completed: nil)
        }
        let strCountryCode = String(/UDSingleton.shared.userData?.userDetails?.user?.countryCode)
        let strPhNo = String(/UDSingleton.shared.userData?.userDetails?.user?.phoneNumber)
        //lblPhoneNumber.text = strCountryCode + "-" + strPhNo
        guard let myRating = UDSingleton.shared.userData?.userDetails?.myRatingCount else{return}
        guard let myAverageRating = UDSingleton.shared.userData?.userDetails?.myRatingAverage else{return}
        
            imgViewUserRating.isHidden = myRating == 0
            lblRatingCount.isHidden = myRating == 0
            lblRatingCount.text = "\(myRating)"
            imgViewUserRating.setRatingSmall(rating: myAverageRating)
        
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        let showPlaceTableOnLeft = (SideMenuController.preferences.basic.position == .under) != (SideMenuController.preferences.basic.direction == .right)
        selectionMenuTrailingConstraint.constant = showPlaceTableOnLeft ? SideMenuController.preferences.basic.menuWidth - size.width : 0
        view.layoutIfNeeded()
    }
    
    func didSelectRowPressed(index: Int) {
        
        let option = arrOptions[index]
        tableView.reloadData()
        switch option {
        case .Bookings :
            
             guard let bookingVC = R.storyboard.sideMenu.bookingsVC() else{return}
             bookingVC.isRightToLeft = LanguageFile.shared.isLanguageRightSemantic()
            self.navigationController?.pushViewController(bookingVC, animated: true)

             
             
             
            break
        case .ETokens :
            break
//            guard let bookingVC = R.storyboard.drinkingWater.tokenListingVC() else{return}
//            if let userData = UDSingleton.shared.userData {
//                // only passed drinking water service category id
//                let brands =  userData.services?.filter({$0.serviceCategoryId == 2}).first?.brands
//                CouponSelectedLocation.categoryId = 3
//                CouponSelectedLocation.categoryBrandId = 0
//                CouponSelectedLocation.Brands = brands
//                CouponSelectedLocation.brandSelected = brands?.first
//            }
//
//            bookingVC.isRightToLeft = LanguageFile.shared.isLanguageRightSemantic()
//            self.navigationController?.pushViewController(bookingVC, animated: true)
//
        case .Promotions :
            Alerts.shared.show(alert: "AppName".localizedString, message: "coming_soon".localizedString , type: .error )
            break
        case .Payments :
//            guard let paymentVC = R.storyboard.bookService.paymentVC() else{return}
//            paymentVC.mode = .SideMenu
            //self.navigationController?.pushViewController(paymentVC, animated: true)
            break
        case .Referral :
//            guard let referralVC = R.storyboard.sideMenu.referralVC() else{return}
//            self.navigationController?.pushViewController(referralVC, animated: true)
            break;
        case .EmergencyContacts :
//            guard let emerContactVC = R.storyboard.sideMenu.emergencyContactVC() else{return}
//            self.navigationController?.pushViewController(emerContactVC, animated: true)
            break
        case .Settings :
            
//            guard let setting = R.storyboard.sideMenu.settingsVC() else{return}
//            self.navigationController?.pushViewController(setting, animated: true)

            break
        case .Contactus :
//            guard let contactusVC = R.storyboard.sideMenu.contactUsVC() else{return}
//            self.navigationController?.pushViewController(contactusVC, animated: true)
            break
        case .SignOut :
      
            self.alertBoxOption(message: "logout_confirmation".localizedString  , title: "AppName".localizedString , leftAction: "no".localizedString , rightAction: "yes".localizedString , ok: { [weak self] in
                
                self?.logOut()
                
            })
        }
    }
    
    func logOut() {
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        let obj = LoginEndpoint.logOut()
        obj.request(header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token]) { [weak self](response) in
             switch response {
             case .success(_):
           self?.dismissVC(completion: nil)
           UDSingleton.shared.removeAppData()
             break;
             case .failure(let strError):
                
                Alerts.shared.show(alert: "AppName".localizedString, message:/strError , type: .error )

           // Toast.show(text: strError, type: .error)
           }
        }
    }
 
}


//MARK:- Side Menu Delegates


extension MenuViewController {
    
    
    func configureTableView() {
        let  configureCellBlock : ListCellConfigureBlock = { [weak self] ( cell , item , indexpath) in
            if let cell = cell as? SelectionCell {
                 cell.titleLabel.text = (self?.arrOptions[indexpath.row].rawValue)?.localizedString
                cell.selectionStyle = .gray
            }
        }
        
        let didSelectCellBlock : DidSelectedRow = { [weak self] (indexPath , cell, item) in
            if let cell = cell as? SelectionCell {
                self?.didSelectRowPressed(index: indexPath.row)
                cell.setSelected(true, animated: true)
            }
        }
        
        tableDataSource = TableViewDataSource(items: arrOptions, tableView: tableView, cellIdentifier: R.reuseIdentifier.selectionCell.identifier, cellHeight: 47)
        tableDataSource?.configureCellBlock = configureCellBlock
        tableDataSource?.aRowSelectedListener = didSelectCellBlock
        tableView.delegate = tableDataSource
        tableView.dataSource = tableDataSource
        tableView.reloadData()
    }
  

}

class SelectionCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
}

