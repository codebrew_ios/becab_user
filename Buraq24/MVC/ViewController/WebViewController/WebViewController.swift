//
//  WebViewController.swift
//  Buraq24Driver
//
//  Created by Apple on 28/09/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: BaseVC , UIWebViewDelegate, WKUIDelegate, WKNavigationDelegate{
    


    @IBOutlet weak var viewForWeb: UIView!
    @IBOutlet weak var webView: WKWebView!
    
    //MARK: -- PROPERTIES
    var stringUrl:String?
    var htmlString: String?
    var completeOnRedirect: (() -> ())?
    var paid = false
    var goBack = false
    
      //MARK: -- VIEW CONTROLLER LIFE CYCLE

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        webView.navigationDelegate = self
        self.openUrlinWebview()
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
         self.navigationController?.popViewController(animated: true)
    }
    
    @objc func handleBlock() {
        completeOnRedirect?()
        self.popVC()
    }
    
    //MARK:-- CUSTOM METHODS

    func openUrlinWebview(){
        
        startAnimating(nil, message: nil, messageFont: nil, type: .lineScalePulseOutRapid, color: UIColor.white, padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil)
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 0.9751529098, green: 0.635238111, blue: 0, alpha: 1)

        if let string = htmlString {
            webView.loadHTMLString(string, baseURL: nil)
        }
        else {
            if stringUrl == APIBasePath.privacyUrl{
                lblTitle.text = "Privacy Policy"
            }
            else if stringUrl == APIBasePath.aboutUs {
                lblTitle.text = "About Us"
            }
            else{
                lblTitle.text = "Terms and Conditions"
            }
            
            let url = URL(string: /stringUrl)
            
            if let val = url{
                let requestObj = URLRequest(url: val as URL)
                webView.load(requestObj)
                
            }
        }

    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.stopAnimating()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
         self.stopAnimating()
    }

    func webViewDidFinishLoad(_ webView: UIWebView) {
        stopAnimating()
        if goBack {
            self.perform(#selector(handleBlock), with: nil, afterDelay: 2)
        }
    }

    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        if completeOnRedirect != nil {
            if (request.url?.absoluteString ?? "").localizedCaseInsensitiveContains("paymentSuccess") || (request.url?.absoluteString ?? "").localizedCaseInsensitiveContains("paymentFailure") || (request.url?.absoluteString ?? "").localizedCaseInsensitiveContains("paymentError"){
                paid = true
            }
            if (request.url?.absoluteString ?? "").localizedCaseInsensitiveContains("payment-status")  {
                print("done")
                if paid {
                    goBack = true
                }
            }
        }
        return true
    }
}
