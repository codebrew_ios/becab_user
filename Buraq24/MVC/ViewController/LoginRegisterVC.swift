//
//  LoginRegisterVC.swift
//  Buraq24
//
//  Created by Sandeep Kumar on 24/10/19.
//  Copyright © 2019 CodeBrewLabs. All rights reserved.
//

import UIKit

class LoginRegisterVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnAction(_ sender: Any) {
        guard let vc = R.storyboard.main.landingVC() else { return }
        pushVC(vc)
    }
    
}
