//
//  HomeVC.swift
//  Buraq24
//
//  Created by MANINDER on 31/07/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import DropDown
import SideMenu
import Crashlytics
import  Fabric
import NVActivityIndicatorView
import CoreLocation
import GooglePlacePicker

protocol BookRequestDelegate {
    
    func didSelectServiceType(_ serviceSelected : Service)
    
    func didSelectBrandProduct(_ brand : Brand ,_ product : Product)
    
    func didSelectQuantity( count : Int)
    
    func didSelectNext( type : ActionType)
    
    
    
    func didSelectSchedulingDate(date : Date, minDate: Date)
    
    func didGetRequestDetails(request : ServiceRequest )
    
    func didGetRequestDetailWithScheduling(request : ServiceRequest)
    
    func didRequestTimeout()
    
    
    func didPaymentModeChanged(paymentMode: PaymentType)
    
    func didRatingSubmit(ratingValue : Int, comment : String)
    
    func didBuyEToken(brandId :Int, brandProductId:Int)
    
    //Selecting Freight Service Order
    
    func didSelectedFreightBrand(brand : Brand)
    func didSelectedFreightProduct(product : Product)
    
}

class HomeVC: UIViewController {
    //MARK:- OUTLETS
    
    //view Select Services
    
//    @IBOutlet var constraintNavigationLocationBar: NSLayoutConstraint!
    @IBOutlet weak var constraintYAddLocationView: NSLayoutConstraint!
    @IBOutlet weak var constraintNavigationBasicBar: NSLayoutConstraint!
//    @IBOutlet weak var constraintHeightPickUpView: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightNavigationBar: NSLayoutConstraint!
//    @IBOutlet var constraintOnlyBackButtonTop: NSLayoutConstraint!
    @IBOutlet var viewTopBarBasicHome: UIView!
    
    //@IBOutlet var segmentedMapType: UISegmentedControl!
    @IBOutlet weak var imgViewPickup: UIImageView!
    @IBOutlet weak var viewDropDown: UIView!
    
    //Enter location
    
    @IBOutlet var addSavedPlaces: UIButton?
    
    @IBOutlet var viewTopBarAddLocation: UIView!
    @IBOutlet var viewLocationTableContainer: UIView!
    @IBOutlet var viewPickUpLocation: UIView!
    
    @IBOutlet var collectionSavedPlaces: UICollectionView?
    
    @IBOutlet var btnLocationNext: UIButton!
    @IBOutlet var btnLocationBack: UIButton!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnOnlyBack: UIButton!
    
    @IBOutlet var txtPickUpLocation: UITextField!
    @IBOutlet var txtDropOffLocation: UITextField!
    @IBOutlet var tblLocationSearch: UITableView!
    @IBOutlet var btnRightNavigationBar: UIButton!
    @IBOutlet var btnLeftNavigationBar: UIButton!
//    @IBOutlet var lblTitleNavigationBar: UILabel!
    @IBOutlet var btnCurrentLocation: UIButton!
    
   // @IBOutlet var constraintTopSegmentedView: NSLayoutConstraint!
    
    //view Enter Order Details
    
    @IBOutlet var viewSelectService: SelectServiceView!
    @IBOutlet var viewGasService: GasServiceView!
    
    
    @IBOutlet var viewScheduler: SchedulerView!
    @IBOutlet var viewDrinkingWaterService: DrinkingWaterView!
    @IBOutlet var viewOrderPricing: OrderPricing!
    @IBOutlet var viewUnderProcess: UnderProcessingView!
    @IBOutlet var viewDriverAccepted: RequestAcceptedView!
    @IBOutlet var viewInvoice: InvoiceView!
    @IBOutlet var viewDriverRating: DriverRatingView!
    
    
    ///Freight Modeule
    @IBOutlet var viewFreightBrand: SelectBrandView!
    @IBOutlet var viewFreightProduct: SelectProductView!
    @IBOutlet var viewFreightOrder: FreightView!
    
    
    //MapView
    @IBOutlet var viewMapContainer: UIView!
    
    @IBOutlet var mapView: GMSMapView!
    
    @IBOutlet var imgViewPickingLocation: UIImageView!
    
    @IBOutlet var viewBottom: UIView!
    @IBOutlet weak var btnLocationPick: UIButton!
    @IBOutlet weak var btnLocationDropOff: UIButton!
    @IBOutlet weak var lblDriverSatus: UILabel!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var btnNormal: UIButton!
    @IBOutlet weak var btnSatellite: UIButton!
    
    //MARK:- PROPERTIES
    var cardId: String?
    var mapDataSource:GoogleMapsDataSource?
    
    var collectionViewWaterDataSource : CollectionViewDataSource?
    var collectionViewPlacesDataSource : CollectionViewDataSource? {
        didSet {
            collectionSavedPlaces?.delegate = collectionViewPlacesDataSource
            collectionSavedPlaces?.dataSource = collectionViewPlacesDataSource
            collectionSavedPlaces?.reloadData()
            
        }
    }
    
    var isCurrentLocationUpdated : Bool = false
    
    var change : Float = 0.05
    
    ///Searching Location TableView
    lazy var movingDrivers : [MovingVehicle] = [MovingVehicle]()
    var currentService : Int = 7
    var tableDataSource : TableViewDataSource?
    var googlePickupLocation : GooglePlaceDataSource?
    var googleDropOffLocation : GooglePlaceDataSource?
     var isPickUpLocation: Bool = false
    var locationEdit : LocationEditing = .DropOff {
        didSet {
            imgViewPickingLocation.image = locationEdit == .DropOff ? #imageLiteral(resourceName: "DropMarker") : #imageLiteral(resourceName: "PickUpMarker")
        }
    }
    
    var results = [GMSAutocompletePrediction]() {
        didSet {
            viewLocationTableContainer.alpha = results.count == 0 ? 0 : 1
            tblLocationSearch.reloadData()
        }
    }
    
    let user = UDSingleton.shared.userData
    
    var placePickerCustomDatasource : GooglePlacePickerManager!
    
    var arraySaveName = ["Home", "Work", "Others"]
    var dataItems : [String] = []
    
    var currentLocation : CLLocation?
    
    var isMapLoaded : Bool = false
    var timerMovingVehicle : Timer?
    var currentSeconds: Float  = 0
    
    var polyPath : GMSMutablePath?
    var polyline : GMSPolyline?
    
    var timer: Timer!
    var i: UInt = 0
    var animationPath = GMSMutablePath()
    var animationPolyline = GMSPolyline()
    
    
    lazy var driverMarker = GMSMarker()
    lazy  var userMarker  = GMSMarker()
    
    var durationLeft : String = ""
    
    
    var destination : CLLocationCoordinate2D?
    var source : CLLocationCoordinate2D?
    
    var isDoneCurrentPolyline : Bool = true
    
    var isCurrent : Bool = false
    var isFocus  : Bool = false
    var lastPoints : String = ""
    var driverBearing : Double?
    var isSearching : Bool = false
    
    
    var locationLatest : String = ""
    var latitudeLatest : Double?
    var longitudeLatest : Double?
    
    var isCheck = false
    
    //MENU Navigation Controller
    var  menuLeftNavigationController : UISideMenuNavigationController?
    var  menuRightNavigationController : UISideMenuNavigationController?
    
    
    var markerPickUpLocation : GMSMarker?
    var markerDropOffLocation : GMSMarker?
    var tempPickUp : String = ""
    var tempDropOff : String = ""
    var isLocationSelected : Bool = false
    var screenType : ScreenType = ScreenType(mapMode: .ZeroMode, entryType: .Forward){
        
        didSet {
            
            let entryType = screenType.entryType
            let mode = screenType.mapMode
            
            switch mode {
            case .ZeroMode:
                showBasicNavigation(updateType: .NormalMode)
            case .NormalMode:
                showBasicNavigation(updateType: .NormalMode)
                showSelectServiceView()
                startMovingVehicleTimer()
            case .SelectingLocationMode:
                clearMovingVehicles()
                showBasicNavigation(updateType: .SelectingLocationMode)
                
            case .OrderDetail: // FrightImages  pop uo treated as
                showBasicNavigation(updateType: .OrderDetail)
                showParticularAddOrderView(type: entryType)
                
            case .ScheduleMode :
                
                showBasicNavigation(updateType: .OrderDetail)
                showSchedulerView(moveType: entryType)
                
            case .OrderPricingMode:
                showPricingView(moveType: entryType)
                
            case .UnderProcessingMode:
                showProcessingView(moveType: .Forward)
                
            case .RequestAcceptedMode , .OnTrackMode:
                clearMovingVehicles()
                showBasicNavigation(updateType: .RequestAcceptedMode)
                showDriverView()
                
            case .ServiceDoneMode:
                
                showInvoiceView()
                
            case .ServiceFeedBackMode:
                break;
                
            case .SelectingFreightBrand:
                
                showBasicNavigation(updateType: .SelectingFreightBrand)
                
                dropPickUpAndDropOffPins(pickLat: /serviceRequest.latitudeDest, pickLong: /serviceRequest.longitudeDest, dropLat: /serviceRequest.latitude, dropLong: /serviceRequest.longitude)
                
                showFreightBrandView(moveType: entryType)
                
            case .SelectingFreightProduct:
                
                showFreightProductView(moveType: entryType)
            }
        }
    }
    
    var locationType : LocationType = .OnlyDropOff {
        didSet {
            viewPickUpLocation.alpha  = locationType == .PickUpDropOff  ? 1 : 0
            //            constraintHeightPickUpView.constant = locationType == .PickUpDropOff  ? 55 : 0
            }
}
    
    var serviceRequest : ServiceRequest = ServiceRequest()
    var homeAPI : Home?
    var currentOrder : Order?
    
    var dic = [String:TrackingModel]()
    
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let filter = GMSAutocompleteFilter()
        let locale = Locale.current
        filter.country = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as? String
        
        setupPlacePicker()
        setUpSideMenuPanels()
        SocketIOManager.shared.initialiseSocketManager()
        
        NotificationCenter.default.addObserver(self, selector: #selector(HomeVC.nCancelFromBookinScreen(notifcation:)), name: Notification.Name(rawValue: LocalNotifications.nCancelFromBookinScreen.rawValue), object: nil)
//        if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
//            statusBar.backgroundColor = UIColor.black
//        }
        
        self.listeningEvent()
        
        
        configureSavePlacesCollectionView()
        showNormalView(btnNormal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        checkForLocationPermissoion()
        collectionViewPlacesDataSource?.items = dataItems
        collectionSavedPlaces?.reloadData()
        showCollectionView()
        txtDropOffLocation.delegate = self
       txtPickUpLocation.delegate = self
//        if !isLocationSelected {
//            btnLocationNext.isHidden = true
//        }
        
        if !isMapLoaded {
            configureMapView()
            
            setUpBasicSettings()
            isMapLoaded = !isMapLoaded
            getUpdatedData()
        }
        addForgroundObserver()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        updateData()
        self.listeningEvent()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if screenType.mapMode == .NormalMode {
            clearMovingVehicles()
        }
    }
    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
    
    
    
    //MARK:-  ACTIONS
    /// Setting action for left and right navigation bar button
    
//    @IBAction func btnOpenPlacePickerPressed(_ sender: UIButton) {
//
//        if sender.tag == 0 {
//            isPickUpLocation = true
//        }else {
//            isPickUpLocation = false
//        }
//        let filter = GMSAutocompleteFilter()
//               //        filter.type = .city
//               // filter.country = /UserData.share.currentLocale
//               let locale = Locale.current
//               filter.country = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as? String
//               let acController = GMSAutocompleteViewController()
//               acController.delegate = placePickerCustomDatasource
//               acController.autocompleteFilter = filter
//
//               DispatchQueue.main.async {[unowned self] in
//
//                   self.present(acController, animated: true, completion: nil)
//
//               }
//
//    }
    
    @IBAction func btnOpenPlacePickerPressed(_ sender: UIButton) {
        if sender.tag == 0 {
            isPickUpLocation = true
        }else {
            isPickUpLocation = false
        }
        //isLocationSelected = true
        btnLocationNext.isHidden = false
        
        //btnLocationNext.setImage(#imageLiteral(resourceName: "NextMaterial").setLocalizedImage(), for: .normal)
        let filter = GMSAutocompleteFilter()
        //        filter.type = .city
       // filter.country = /UserDetail.share.currentLocale
        let locale = Locale.current
        filter.country = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as? String
        let acController = GMSAutocompleteViewController()
        acController.delegate = placePickerCustomDatasource
        acController.autocompleteFilter = filter
        //acController.tableCellBackgroundColor = .clear
        
        DispatchQueue.main.async {[unowned self] in
            
            self.present(acController, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func actionBtnMenuPressed(_ sender: UIButton) {
        presentMenu(type: .Left)
        
    }
    
    @IBAction func actionBtnChangeMaptype(_ sender: UISegmentedControl) {
        
        if let mapType = UserDefaultsManager.shared.mapType {
            guard let mapEnum = BuraqMapType(rawValue: mapType) else { return }
            mapView.mapType = mapEnum == .Hybrid ? .hybrid : .normal
            UserDefaultsManager.shared.mapType = mapEnum == .Hybrid ? BuraqMapType.Satellite.rawValue : BuraqMapType.Hybrid.rawValue
            guard let polylin = self.polyline else{return}
            polylin.strokeColor  = mapEnum == .Hybrid ? .white : .darkGray
            
        }
    }
    
    
    @IBAction func showNormalView(_ sender: Any) {
        actionBtnChangeMaptype(.Hybrid)
    }
    
    @IBAction func showSatelliteView(_ sender: Any) {
        actionBtnChangeMaptype(.Satellite)
    }
    
    func actionBtnChangeMaptype(_ type: BuraqMapType) {
        
        mapView.mapType = type == .Satellite ? .satellite : .normal
        self.polyline?.strokeColor  = type == .Satellite ? .white : .darkGray
        btnNormal.isSelected = type == .Hybrid
        btnSatellite.isSelected = type == .Satellite
        btnNormal.backgroundColor = btnNormal.isSelected ? UIColor.textColor : UIColor.white
        btnSatellite.backgroundColor = btnSatellite.isSelected ? UIColor.textColor : UIColor.white
    }
    
    @IBAction func actionBtnCurrentLocation(_ sender: UIButton) {
        
        if sender.isSelected{
            btnCurrentLocation.isSelected = false
            
            let currentLocation = GMSCameraPosition.camera(withLatitude: /mapView.myLocation?.coordinate.latitude,
                                                           longitude: /mapView.myLocation?.coordinate.longitude,
                                                           zoom: 14)
            mapView.animate(to: currentLocation)
            return
        }
        
        if  screenType.mapMode == .RequestAcceptedMode{
            
            guard let path = GMSMutablePath(fromEncodedPath: Polyline.points) else { return }
            let bounds = GMSCoordinateBounds(path: path)
            self.mapView?.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50.0))
            btnCurrentLocation.isSelected = true
            
        }
        else{
            
            let currentLocation = GMSCameraPosition.camera(withLatitude: /mapView.myLocation?.coordinate.latitude,
                                                           longitude: /mapView.myLocation?.coordinate.longitude,
                                                           zoom: 14)
            mapView.animate(to: currentLocation)
            
        }
        
        
        //        if !(self.btnCurrentLocation.isSelected){
        //
        //            guard let path = GMSMutablePath(fromEncodedPath: Polyline.points) else { return }
        //            let bounds = GMSCoordinateBounds(path: path)
        //            self.mapView?.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50.0))
        //            self.btnCurrentLocation.isSelected = true
        //
        //        }
        //        else{
        //                    let currentLocation = GMSCameraPosition.camera(withLatitude: /mapView.myLocation?.coordinate.latitude,
        //                                                                   longitude: /mapView.myLocation?.coordinate.longitude,
        //                                                                   zoom: 14)
        //                    mapView.animate(to: currentLocation)
        //              self.btnCurrentLocation.isSelected = false
        //        }
        
    }
    
    @IBAction func actionBtnSettingsPressed(_ sender: UIButton) {
        presentMenu(type: .Right)
    }
    
    ///Function to move backward
    
    @IBAction func actionBtnMapBackPressed(_ sender: UIButton) {
        
        let mode = screenType.mapMode
        
        switch mode {
            
        case .SelectingLocationMode:
            results.removeAll()
            changeLocationType(move: .Backward)
            screenType = ScreenType(mapMode: .NormalMode, entryType: .Backward)
        case .SelectingFreightBrand:
            
            viewFreightBrand.minimizeSelectBrandView()
            screenType = ScreenType(mapMode: .SelectingLocationMode, entryType: .Backward)
            
            
        case .SelectingFreightProduct:
            
            //Hide Frieght Product View and show
            viewFreightProduct.minimizeProductView()
            screenType = ScreenType(mapMode: .SelectingFreightBrand, entryType: .Backward)
            
        case .OrderDetail:
            
            hideParticularOrderView()
            //change here grater than 3 is for pick up or cab and less than three for gas booking
            guard let serviceID = serviceRequest.serviceSelected?.serviceCategoryId else{ return }
            var modeType : MapMode  = serviceID > 3 ? .SelectingFreightProduct : .SelectingLocationMode
            if serviceID == 7 {
                modeType = .SelectingFreightBrand
            }
            screenType = ScreenType(mapMode: modeType, entryType: .Backward)
            
        case .ScheduleMode:
            
            viewScheduler.minimizeSchedulerView()
            screenType = ScreenType(mapMode: .OrderDetail, entryType: .Backward)
            
        case .OrderPricingMode:
            
            viewOrderPricing.minimizeOrderPricingView()
            
            guard let serviceID = serviceRequest.serviceSelected?.serviceCategoryId else{ return }
            
            switch serviceID {
            case 7, 12: //Car and bike booking
                viewFreightProduct.minimizeProductView()
                screenType = ScreenType(mapMode: .SelectingFreightBrand, entryType: .Backward)
            case 4: //PickUp and delivery
                let mapmode : MapMode  = serviceRequest.requestType == .Present  ? .OrderDetail : .ScheduleMode
                screenType = ScreenType(mapMode: mapmode, entryType: .Backward)
            default: break
            
            }
            
        default :
            break;
        }
    }
    
    /// Button action for selecting location for Service
    @IBAction func actionBtnLocationSelected(_ sender: UIButton) {
        
        self.view.endEditing(true)
        results.removeAll()
        
        guard let serviceID = serviceRequest.serviceSelected?.serviceCategoryId else{return}
        guard let addressDropOff = txtDropOffLocation.text else{return}
        
        if serviceID > 3 {
            
            guard let addressPicUp = txtPickUpLocation.text else{return}
            
            if addressPicUp != tempPickUp  ||  addressPicUp.isEmpty  {
                Alerts.shared.show(alert: "AppName".localizedString, message: "pickup_address_validation_message".localizedString , type: .error )
                return
            }
            

            
        } else {

            guard let addressPicUp = txtPickUpLocation.text else{return}
            
            if addressPicUp != tempPickUp  ||  addressPicUp.isEmpty  {
                Alerts.shared.show(alert: "AppName".localizedString, message: "pickup_address_validation_message".localizedString , type: .error )
                return
            }
        }
        
        let modeType : MapMode  = serviceID > 3 ? .SelectingFreightBrand : .OrderDetail
        screenType = ScreenType(mapMode: modeType, entryType: .Forward)
        locationEdit  = serviceID > 3 ? .PickUp : .DropOff
        
//        print(/self.serviceRequest.latitudeDest)
//        print(/self.serviceRequest.longitudeDest)
//        print(/self.serviceRequest.latitude)
//        print(/self.serviceRequest.longitude)
//
//         self.setCameraGoogleMap(latitude: /self.serviceRequest.latitude, longitude: /self.serviceRequest.longitude)
//
//        source = CLLocationCoordinate2D(latitude: CLLocationDegrees(/serviceRequest.latitudeDest) , longitude: CLLocationDegrees(/serviceRequest.longitudeDest))
//
//        destination = CLLocationCoordinate2D(latitude: CLLocationDegrees(/serviceRequest.latitude) , longitude: CLLocationDegrees(/serviceRequest.longitude))
//
//        isDoneCurrentPolyline = true
//
//            self.getPolylineRoute(source: source, destination: destination, model: nil)
        
        //self.getPolylineRoute(source: serviceRequest., destination: <#T##CLLocationCoordinate2D?#>, model: <#T##TrackingModel?#>)
        
    }
    
    @IBAction func addSavedPlaces(_sender : UIButton){
//        guard let vc = R.storyboard.bookService.saveAddressVC() else {return}
//        self.pushVC(vc)
        addSavedPlaces?.isSelected = true
        dataItems = arraySaveName
        collectionViewPlacesDataSource?.items = dataItems
        collectionSavedPlaces?.reloadData()
        showCollectionView()
        
    }
    
    
    func changeLocationType(move : MoveType) {
        
        guard let serviceID = serviceRequest.serviceSelected?.serviceCategoryId else{return}
        
        // Change here for different service id
        
        if let lat = latitudeLatest, let lng = longitudeLatest {
            checkPickup(lat: lat, lng: lng) { [weak self] in
                guard let `self` = self else { return }
                if move == .Forward {
                    self.serviceRequest.locationNameDest =  serviceID > 3 ?   self.locationLatest : nil
                    self.serviceRequest.latitudeDest =  serviceID > 3 ?   self.latitudeLatest : nil
                    self.serviceRequest.longitudeDest = serviceID > 3 ?   self.longitudeLatest : nil
                    
                    self.txtPickUpLocation.text = self.locationLatest
                    self.txtDropOffLocation.text = ""
                    self.tempDropOff =  /self.txtDropOffLocation.text
                    self.tempPickUp =  /self.txtPickUpLocation.text
                    
                    
                    self.serviceRequest.locationName =     serviceID > 3 ?   nil : self.locationLatest
                    self.serviceRequest.latitude =   serviceID > 3 ?   nil : self.latitudeLatest
                    self.serviceRequest.longitude = serviceID > 3 ?   nil : self.longitudeLatest
                    
                } else {
                    self.locationEdit = serviceID > 3 ? .PickUp : .DropOff
                    self.viewSelectService.lblLocationName.text = self.locationLatest
                }
            }
        }
        else {
            if move == .Forward {
                serviceRequest.locationNameDest = nil
                serviceRequest.latitudeDest =  nil
                serviceRequest.longitudeDest = nil
                
                txtPickUpLocation.text = ""
                txtDropOffLocation.text = ""
                tempDropOff =  /txtDropOffLocation.text
                tempPickUp =  /txtPickUpLocation.text
                
                serviceRequest.locationName = nil
                serviceRequest.latitude = nil
                serviceRequest.longitude = nil
                
            } else {
                locationEdit = serviceID > 3 ? .PickUp : .DropOff
                viewSelectService.lblLocationName.text = locationLatest
            }
        }
      
        

    }
    
    
    //MARK:- SETUP PLACE PICKER
    func setupPlacePicker() {
        
        placePickerCustomDatasource = GooglePlacePickerManager.init(didPickPlace: { [weak self] (_ place) in
            
         
            if self!.isPickUpLocation {
                
                self?.txtPickUpLocation.text =  place.name
                self!.locationEdit = .PickUp
                self!.setCameraGoogleMap(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
                
            } else {
                self?.txtDropOffLocation.text =  place.name
                self!.locationEdit = .DropOff
                self!.setCameraGoogleMap(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
            }
            
            self?.setUpdatedAddress(location: /place.name, coordinate: place.coordinate)
            
            }, didCancel: { (canceled) in
                
                print("Cancel")
                
        }, didTapMarker: {  (didTap) in
            
        })
    }
    
    //MARK:- FUNCTIONS
    //MARK:-
    
    func setGradientBackground() {
        
        let colorTop =  UIColor(red: 255.0, green: 255.0, blue: 255.0, alpha: 0.05).cgColor
        let colorBottom = UIColor.white.cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = viewBottom.bounds
//        viewBottom.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    
    func setUpBasicSettings() {
        
        constraintNavigationBasicBar.constant = BookingPopUpFrames.navigationBarHeight
//        constraintNavigationLocationBar.constant = BookingPopUpFrames.navigationBarHeight
        
        //constraintTopSegmentedView.constant = BookingPopUpFrames.paddingX
        
        viewPickUpLocation.alpha  = 0  //To SetUP for Only Drop Location
//        constraintHeightPickUpView.constant = 0
        guard let services = UDSingleton.shared.userData?.services else {return}
        
        print(services)
        if services.count > 0{
            serviceRequest.serviceSelected = services[0]
        }
//        serviceRequest.serviceSelected = services[0]
//        constraintOnlyBackButtonTop.constant = BookingPopUpFrames.statusBarHeight
        screenType = ScreenType(mapMode: .ZeroMode, entryType: .Forward)
        
       
        
        assignDelegates()
        setGradientBackground()
        
       
        btnLocationBack.setImage(#imageLiteral(resourceName: "Back").setLocalizedImage(), for: .normal)
        btnOnlyBack.setImage(#imageLiteral(resourceName: "ic_back_map").setLocalizedImage(), for: .normal)
        txtDropOffLocation.setAlignment()
        txtPickUpLocation.setAlignment()
    }
    
    
    func getUpdatedData() {
        
        guard let services = UDSingleton.shared.userData?.services else { return }
        
        if services.count > 0 {
            getLocalDriverForParticularService(service: services[0])
        }
//        getLocalDriverForParticularService(service: services[0])
        
    }
    
    func assignDelegates() {
        
        viewSelectService.delegate = self
        viewGasService.delegate = self
        viewDrinkingWaterService.delegate = self
        viewOrderPricing.delegate = self
        viewUnderProcess.delegate = self
        viewDriverAccepted.delegate = self
        viewInvoice.delegate = self
        viewScheduler.delegate = self
        viewDriverRating.delegate = self
        viewFreightBrand.delegate = self
        viewFreightProduct.delegate = self
        viewFreightOrder.delegate = self
    }
    
    
    func showBasicNavigation(updateType: MapMode) {
        self.view.endEditing(true)
        if updateType == .SelectingLocationMode  {
            
            configureLocationSearchTableView()
        }
        
        UIView.animate(withDuration: 0.4, animations: { [weak self] in
            
            if updateType == .NormalMode {
                
                self?.constraintNavigationBasicBar.constant = -(BookingPopUpFrames.navigationBarHeight)
                self?.constraintYAddLocationView.constant = (BookingPopUpFrames.navigationBarHeight )
                self?.imgViewPickingLocation.isHidden = false
                self?.mapView.isUserInteractionEnabled = true
                self?.btnOnlyBack.alpha = 0
                
                
              //  self?.segmentedMapType.isHidden = false
                //self?.constraintTopSegmentedView.constant = BookingPopUpFrames.paddingX
                self?.btnCurrentLocation.isHidden = false
                
            }else if updateType == .SelectingLocationMode {
                
               // self?.segmentedMapType.isHidden = false
                self?.btnCurrentLocation.isHidden = false
                self?.constraintNavigationBasicBar.constant = BookingPopUpFrames.navigationBarHeight + BookingPopUpFrames.navigationTopPadding
                
                self?.imgViewPickingLocation.isHidden = false
                
                self?.mapView.isUserInteractionEnabled = true
                self?.btnOnlyBack.alpha = 0
                
                guard let selectedService  = self?.serviceRequest.serviceSelected else{return}
                
                // Change here for different service id

                
                     if  /selectedService.serviceCategoryId > 3 {
                    self?.locationType = .PickUpDropOff
                    self?.imgViewPickingLocation.image = #imageLiteral(resourceName: "PickUpMarker")
                    self?.constraintYAddLocationView.constant = -(BookingPopUpFrames.navigationBarHeight + CGFloat(115) )
                    //self?.constraintTopSegmentedView.constant = BookingPopUpFrames.paddingXPickUpLocation
                    
                }else{
                    
                    self?.constraintYAddLocationView.constant = -(BookingPopUpFrames.navigationBarHeight + CGFloat(55))
                    self?.locationType = .OnlyDropOff
                    self?.imgViewPickingLocation.image = #imageLiteral(resourceName: "DropMarker")
                    //self?.constraintTopSegmentedView.constant = BookingPopUpFrames.paddingXDropOffLocation
                }
                
            }else if updateType == .OrderDetail || updateType == .SelectingFreightBrand {
                
                self?.mapView.isUserInteractionEnabled = true
                self?.btnOnlyBack.alpha = 1
                //self?.segmentedMapType.isHidden = true
               // self?.constraintTopSegmentedView.constant = BookingPopUpFrames.paddingX
                self?.btnCurrentLocation.isHidden = true
                self?.constraintNavigationBasicBar.constant = BookingPopUpFrames.navigationBarHeight + BookingPopUpFrames.navigationTopPadding
                
                guard let service  = self?.serviceRequest.serviceSelected else{return}
                
                self?.imgViewPickingLocation.isHidden  = /service.serviceCategoryId > 3
                
                self?.constraintYAddLocationView.constant =  /service.serviceCategoryId > 3 ? (BookingPopUpFrames.navigationBarHeight) : BookingPopUpFrames.navigationBarHeight + CGFloat(55)
                
                
            }else if updateType == .RequestAcceptedMode || updateType == .OnTrackMode {
                
                self?.constraintNavigationBasicBar.constant = -(BookingPopUpFrames.navigationBarHeight)
                
                self?.imgViewPickingLocation.isHidden = true
                self?.mapView.isUserInteractionEnabled = true
                self?.btnOnlyBack.alpha = 0
                //self?.segmentedMapType.isHidden = false
                self?.btnCurrentLocation.isHidden = false
                
                guard let currOrder = self?.currentOrder else {return}
                
                if /currOrder.serviceId > 3 {
                    self?.constraintYAddLocationView.constant = (BookingPopUpFrames.navigationBarHeight )
                }else{
                    self?.constraintYAddLocationView.constant = (BookingPopUpFrames.navigationBarHeight )
                }
            }
            self?.view.layoutIfNeeded()
            }, completion: { (done) in
        })
    }
    
    func clearSelectedLocation() {
        view.endEditing(true)
        results.removeAll()
        tblLocationSearch.reloadData()
        viewLocationTableContainer.alpha = 0
    }
    
    
    func showSelectServiceView(moveType : MoveType = .Forward) {
        isSearching = false
        
        guard let services = UDSingleton.shared.userData?.services else {return}
        btnLocationNext.isHidden = true
        viewSelectService.showSelectServiceView(superView: viewMapContainer, moveType: moveType, requestPara: self.serviceRequest, service: services)
    }
    
    func showParticularAddOrderView(type : MoveType) {
        guard let serviceId = serviceRequest.serviceSelected?.serviceCategoryId else {return}
        switch serviceId {
            
        case 1,3:
            viewGasService.showGasServiceView(superView: viewMapContainer, moveType: type, requestPara: serviceRequest)
        case 2:
            viewDrinkingWaterService.showDrinkingWaterServiceView(superView: viewMapContainer, moveType: type, requestPara: serviceRequest)
        case 4, 7:
            
            viewFreightOrder.showFreightOrderView(supView: viewMapContainer, moveType: type, requestPara: serviceRequest)
            
        default:
            break
        }
    }
    
    
    func hideParticularOrderView() {
        
        guard let serviceId = serviceRequest.serviceSelected?.serviceCategoryId else {return}
        
        switch serviceId {
        case 1,3:
            viewGasService.minimizeGasServiceView()
        case 2:
            viewDrinkingWaterService.minimizeDrinkingWaterView()
        case 4, 7:
            viewFreightOrder.minimizeFreightOrderView()
        default:
            break
        }
    }
    
    
    func showFreightBrandView(moveType : MoveType) {
        let id = /serviceRequest.serviceSelected?.serviceCategoryId
        viewFreightBrand.showBrandView(superView: viewMapContainer, moveType: moveType, requestPara: serviceRequest, categoryId: id)
    }
    
    func showFreightProductView(moveType : MoveType) {
        viewFreightProduct.showProductView(superView: viewMapContainer, moveType: moveType, requestPara: serviceRequest)
    }
    
    func showSchedulerView(moveType : MoveType) {
        self.btnOnlyBack.alpha = 1
        viewScheduler.showSchedulerView(superView: viewMapContainer, moveType: moveType, requestPara: serviceRequest)
    }
    
    
    func showPricingView(moveType : MoveType ) {
        self.btnOnlyBack.alpha = 1
        
        viewOrderPricing.showOrderPricingView(superView: viewMapContainer, moveType: moveType, requestPara: serviceRequest)
    }
    
    func showProcessingView( moveType : MoveType ) {
        self.btnOnlyBack.alpha = 0
        guard let current = currentOrder else{return}
        viewUnderProcess.showWaitingView(superView: viewMapContainer , order: current)
    }
    
    func showDriverView() {
        guard let current = currentOrder else{return}
        viewDriverAccepted.showDriverAcceptedView(superView: viewMapContainer, order: current, label: lblDriverSatus)
        imgLogo.isHidden = true
        lblDriverSatus.isHidden = false
    }
    
    func showInvoiceView() {
        guard let current = currentOrder else{return}
        viewInvoice.showInvoiceView(superView: viewMapContainer, order: current)
    }
    
    func showDriverRatingView() {
        
        guard let current = currentOrder else{return}
        viewDriverRating.showDriverRatingView(superView: viewMapContainer, order: current)
    }
    
    func moveToNormalMode() {
        imgLogo.isHidden = false
        lblDriverSatus.isHidden = true
        
        self.cleanPath()
        imgViewPickingLocation.isHidden = false
        mapView.isUserInteractionEnabled = true
        self.mapView.clear()
        self.cleanPath()
        self.screenType = ScreenType(mapMode: .NormalMode, entryType: .Backward)
        
    }
    
    func driverAcceptedMode(order : Order) {
        self.cleanPath()
        mapView.clear()
        listeningEvent()
        currentOrder = order
        viewUnderProcess.minimizeProcessingView()
        viewOrderPricing.minimizeOrderPricingView()
        imgViewPickingLocation.isHidden = true
        mapView.isUserInteractionEnabled = true
        screenType = ScreenType(mapMode: .RequestAcceptedMode, entryType: .Forward)
        
        // Set
        if /order.serviceId > 3 &&  (order.orderStatus == .Confirmed || order.orderStatus == .reached) {
            destination =   CLLocationCoordinate2D(latitude: CLLocationDegrees(/order.pickUpLatitude) , longitude: CLLocationDegrees(/order.pickUpLongitude))
        }else{
            destination =   CLLocationCoordinate2D(latitude: CLLocationDegrees(/order.dropOffLatitude) , longitude: CLLocationDegrees(/order.dropOffLongitude))
        }
        
        source = CLLocationCoordinate2D(latitude: CLLocationDegrees(/order.driverAssigned?.driverLatitude) , longitude: CLLocationDegrees(/order.driverAssigned?.driverLongitude))
        
        getPolylineRoute(source: source, destination: destination, model: nil)
        
    }
    
    ///To set final price to pricing pop up
    
    
    func calculateImageSize() {
        
        if /mapView.camera.zoom >= minimumZoom {
            
            if previousZoom == /mapView.camera.zoom {return}
            
            previousZoom  = /mapView.camera.zoom
            let widthVal = (Float(vehicleSize.width)*(/mapView.camera.zoom).getZoomPercentage)/100
            let heightVal = (Float(vehicleSize.height)*(/mapView.camera.zoom).getZoomPercentage)/100
            vehicleCurrentSize =  CGSize(width: Double(widthVal), height: Double(heightVal))
            
            for item in movingDrivers {
                
                let marker = item.driverMarker
                let mgVehicle = UIImage().getDriverImage(type: /item.driver.driverServiceId)
                marker.icon = mgVehicle.imageWithImage(scaledToSize: vehicleCurrentSize)
            }
        }
    }
    
    func configureMapView() {
        
        let didUpdateCurrentLocation : DidUpdatecurrentLocation = {[weak self] (manager,locations) in  //current location closure
            let currentLocation = GMSCameraPosition.camera(withLatitude: /manager.location?.coordinate.latitude,
                                                           longitude: /manager.location?.coordinate.longitude,
                                                           zoom: 14.0)
            self?.currentLocation =   CLLocation(latitude: /manager.location?.coordinate.latitude, longitude: /manager.location?.coordinate.longitude )
            if !(/self?.isCurrentLocationUpdated) {
                
                self?.mapView.camera = currentLocation
                self?.isCurrentLocationUpdated = true
                
                let widthVal = (Float(vehicleSize.width)*(/self?.mapView.camera.zoom).getZoomPercentage)/100
                let heightVal = (Float(vehicleSize.height)*(/self?.mapView.camera.zoom).getZoomPercentage)/100
                vehicleCurrentSize =  CGSize(width: Double(widthVal), height: Double(heightVal))
                
                self?.mapDataSource?.getAddressFromlatLong(lat: /manager.location?.coordinate.latitude, long: /manager.location?.coordinate.longitude, completion: {  [weak self](strLocationName) in
                    
                    self?.setUpdatedAddress(location: strLocationName, coordinate: manager.location?.coordinate)
                })
            }
        }
        
        
        let didChangePosition : DidChangePosition = { [weak self] in
            if self?.screenType.mapMode == .NormalMode {
                self?.calculateImageSize()
            }
        }
        
        let didStopPosition : MapsDidStopMoving = { [weak self]  (position) in

            let newLocation = CLLocation(latitude: /position.latitude, longitude: /position.longitude)

            if  let currentLocation = self?.currentLocation {
                if let distance = self?.getDistance(newPosition: currentLocation , previous:  newLocation) {
                    //                    self?.btnCurrentLocation.isSelected = !(distance > Float(2))
                }
            }

            if self?.screenType.mapMode == .SelectingLocationMode {
                self?.viewLocationTableContainer.alpha = 0
                self?.results.removeAll()
                self?.tblLocationSearch.reloadData()
            }

            self?.mapDataSource?.getAddressFromlatLong(lat: /position.latitude , long:  /position.longitude , completion: { (strLocationName) in
                self?.setUpdatedAddress(location: strLocationName, coordinate: position)
            })
        }
        
        mapDataSource = GoogleMapsDataSource.init(mapStyleJSON: "MapStyle", mapView: mapView)
        mapView.delegate = mapDataSource
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = false
        mapView.settings.tiltGestures = false
        mapView.isBuildingsEnabled = false
        mapView.setMinZoom(2, maxZoom: 20)
        mapView.animate(toZoom: 6)
        mapDataSource?.didUpdateCurrentLocation = didUpdateCurrentLocation
        mapDataSource?.didChangePosition = didChangePosition
        mapDataSource?.mapStopScroll = didStopPosition
        
        if let mapType = UserDefaultsManager.shared.mapType {
            
            guard let mapEnum = BuraqMapType(rawValue: mapType) else { return }
            mapView.mapType = mapEnum == .Hybrid ? .normal : .hybrid
          //  segmentedMapType.selectedSegmentIndex = mapEnum == .Hybrid ? 0 : 1
            
        }
    }
    
    
    func setUpdatedAddress(location : String , coordinate : CLLocationCoordinate2D?) {
        self.locationLatest = location
        self.latitudeLatest = Double(/coordinate?.latitude)
        self.longitudeLatest =  Double(/coordinate?.longitude)
        
        self.viewSelectService.lblLocationName.text = location
        
        CouponSelectedLocation.latitude = /coordinate?.latitude
        CouponSelectedLocation.longitude = /coordinate?.longitude
        CouponSelectedLocation.selectedAddress = location
        
        if self.txtDropOffLocation.isEditing == false && self.txtPickUpLocation.isEditing == false {
            return
        }
        checkPickup(lat: Double(/coordinate?.latitude), lng: Double(/coordinate?.longitude)) { [weak self] in
            guard let `self` = self else { return }
            print(coordinate)
            if self.screenType.mapMode == .SelectingLocationMode {
                
                if self.txtDropOffLocation.isEditing == true  ||  self.locationEdit  == .DropOff {
                    
                    print(location)
                    self.txtDropOffLocation.text = location
                    self.serviceRequest.locationName = location
                    print(/coordinate?.longitude)
                    print(/coordinate?.latitude)
                    self.serviceRequest.longitude =  Double(/coordinate?.longitude)
                    self.serviceRequest.latitude =  Double(/coordinate?.latitude)
                    self.tempDropOff = location
                    
                }else if self.txtPickUpLocation.isEditing == true ||  self.locationEdit  == .PickUp {
                    
                    print(location)
                    self.serviceRequest.locationNameDest = location
                    print(/coordinate?.longitude)
                    print(/coordinate?.latitude)
                    self.serviceRequest.longitudeDest =  Double(/coordinate?.longitude)
                    self.serviceRequest.latitudeDest =  Double(/coordinate?.latitude)
                    self.txtPickUpLocation.text = location
                    self.tempPickUp = location
                }
            }
        }
    }
}

//MARK:- API
extension HomeVC {
    
    func orderEventFire() {
        
        guard let order = currentOrder else{return}
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        
        let dict : [String : Any] = [EmitterParams.EmitterType.rawValue : CommonEventType.ParticularOrder.rawValue ,EmitterParams.AccessToken.rawValue  :  token ,EmitterParams.OrderToken.rawValue :   /order.orderToken]
        SocketIOManager.shared.getParticularOrder(dict) { [weak self] (response) in
            
            if let order = response as? Order {
                
                if order.orderStatus == .Confirmed || order.orderStatus == .reached || order.orderStatus == .Ongoing {
                    self?.driverAcceptedMode(order: order)
                    
                } else if order.orderStatus == .ServiceTimeout {
                    self?.viewUnderProcess.minimizeProcessingView()
                    if self?.isSearching == true {
                        self?.screenType = ScreenType(mapMode: .NormalMode , entryType: .Forward)
                    }else{
                        self?.screenType = ScreenType(mapMode: .OrderPricingMode , entryType: .Forward)
                    }
                } else if order.orderStatus == .CustomerCancel {
                    
                    
                } else if order.orderStatus == .Searching {
                    
                    //  self?.screenType = ScreenType(mapMode: .NormalMode , entryType: .Forward)
                }
            }
        }
    }
    
    func checkFinalStatus() {
        
        guard let orderRequested = currentOrder else{return}
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        
        let dict : [String : Any] = [
            EmitterParams.EmitterType.rawValue : CommonEventType.ParticularOrder.rawValue ,
            EmitterParams.AccessToken.rawValue  :  token ,
            EmitterParams.OrderToken.rawValue :   /orderRequested.orderToken
        ]
        
        SocketIOManager.shared.getParticularOrder(dict) { [weak self] (response) in
            
            if let order = response as? Order {
                
                if order.orderStatus ==  .CustomerCancel {
                    //  self?.currentOrder?.orderStatus = order.orderStatus
                    if self?.screenType.mapMode == .RequestAcceptedMode {
                        
                        //                    ez.runThisInMainThread {
                        //
                        //                    self?.driverMarker.map = nil
                        //                    self?.polyline?.map = nil
                        //                    self?.userMarker.map = nil
                        //                    self?.viewDriverAccepted.minimizeDriverView()
                        //                   }
                    }
                }
            }
        }
    }
    
    func listeningEvent() {
        imgLogo.isHidden = false
        lblDriverSatus.isHidden = true
        SocketIOManager.shared.listenOrderEventConnected { [weak self]( data ,socketType) in
            self?.imgLogo.isHidden = false
            self?.lblDriverSatus.isHidden = true
            guard let socketType = socketType as? OrderEventType else {return}
            
            switch socketType {
                
            case .ServiceAccepted:
                guard let model = data as? Order else { return }
                if model.bookingType == .Present {
                    if self?.currentOrder?.orderStatus == .CustomerCancel {return}
                }
                
                if self?.serviceRequest.requestType == .Future {
                    self?.viewOrderPricing.minimizeOrderPricingView()
                    self?.viewUnderProcess.minimizeProcessingView()
                    self?.screenType = ScreenType(mapMode: .NormalMode , entryType: .Forward)
                    if  self?.isCheck == false {
                        self?.isCheck = true
                        self?.mapView.clear()
                        self?.cleanPath()
                        self?.alertBoxOk(message:"service_booked_successfully".localizedString , title: "congratulations".localizedString, ok: {
                            self?.isCheck = false
                        })
                    }
                }else{
                    self?.driverAcceptedMode(order: model)
                    //  self?.checkFinalStatus()
                }
                
            case .ServiceCurrentOrder:
                
                if self?.currentOrder?.bookingType == .Future {return}
                
                guard let model = data as? TrackingModel else { return }
                
                AllOrdersOngoing.ordersOngoing["\(/model.orderId)"] = model
                AllOrdersOngoing.order = model
                
                self?.dic["\(/model.orderId)"] = model
                AllOrdersOngoing.ordersOngoing = /self?.dic
                
                print("\(LocalNotifications.ETokenTrackingRefresh.rawValue)\(/model.orderId)")
                
                NotificationCenter.default.post(name: Notification.Name("\(LocalNotifications.ETokenTrackingRefresh.rawValue)\(/model.orderId)"), object: nil, userInfo: nil)
                
                //Driver Updated Lat and Long
                let updatedDriver = CLLocationCoordinate2D(latitude: CLLocationDegrees(/model.driverLatitude) , longitude: CLLocationDegrees(/model.driverLongitude))
                
                if (model.orderStatus != .Ongoing && model.orderStatus != .Confirmed) {return}
                self?.currentOrder?.orderStatus = model.orderStatus
                self?.driverBearing = model.bearing
                if let order = self?.currentOrder {
                    
                    if /order.serviceId > 3 && (model.orderStatus == .Ongoing || model.orderStatus == .Confirmed) {
                        self?.destination =   CLLocationCoordinate2D(latitude: CLLocationDegrees(/order.dropOffLatitude) , longitude: CLLocationDegrees(/order.dropOffLongitude))
                    }
                }
                self?.source = updatedDriver
                
                if /self?.btnCurrentLocation.isSelected{
                    guard let path = GMSMutablePath(fromEncodedPath: /model.polyline) else { return }
                    
                    let bounds = GMSCoordinateBounds(path: path)
                    self?.mapView?.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50.0))
                }
                
                if self?.currentOrder?.myTurn == OrderTurn.MyTurn{
                    
                    Polyline.points = /model.polyline
                    self?.getPolylineRoute(source: updatedDriver , destination: self?.destination, model: model)
                }
                else{
                    
                }
                guard let `self` = self else { return }
                self.viewDriverAccepted.setOrderStatus(tracking: model, label: self.lblDriverSatus)
                self.imgLogo.isHidden = true
                self.lblDriverSatus.isHidden = false
                
            case .ServiceCompletedByDriver:
                
                guard let model = data as? Order else { return }
                self?.checkCancellationPopUp()
                
                if model.bookingType == .Future {return }
                self?.currentOrder = model
                self?.viewDriverAccepted.minimizeDriverView()
                self?.screenType = ScreenType(mapMode: .ServiceDoneMode , entryType: .Forward)
                
            case .DriverCancelrequest:
                self?.checkCancellationPopUp()
                let mode = self?.screenType.mapMode
                if mode == .RequestAcceptedMode || mode == .OnTrackMode  {
                    
                    self?.mapView.clear()
                    self?.cleanPath()
                    self?.viewDriverAccepted.minimizeDriverView()
                    self?.screenType = ScreenType(mapMode: .NormalMode , entryType: .Forward)
                    
                }else if mode == .UnderProcessingMode   {
                    
                    self?.viewUnderProcess.minimizeProcessingView()
                    self?.screenType = ScreenType(mapMode: .OrderPricingMode , entryType: .Forward)
                }
                
            case .ServiceRejectedByAllDriver , .ServiceTimeOut  :
                
                self?.viewUnderProcess.minimizeProcessingView()
                
                let mapMode : MapMode = self?.isSearching == true ? .NormalMode : .OrderPricingMode
                self?.screenType = ScreenType(mapMode: mapMode , entryType: .Forward)
            case .serReached:
                guard let `self` = self else { return }
                self.lblDriverSatus.text = "driver_is_reached".localizedString
                self.imgLogo.isHidden = true
                self.lblDriverSatus.isHidden = false
                break
            case .ServiceOngoing , .DriverRatedCustomer :
                
                if self?.currentOrder?.bookingType == .Future {return}
                
                guard let model = data as? TrackingModel else { return }
                
                //Driver Updated Lat and Long
                
                let updatedDriver = CLLocationCoordinate2D(latitude: CLLocationDegrees(/model.driverLatitude) , longitude: CLLocationDegrees(/model.driverLongitude))
                
                if model.orderStatus != .Ongoing {return}
                self?.currentOrder?.orderStatus = model.orderStatus
                self?.driverBearing = model.bearing
                if let order = self?.currentOrder {
                    
                    if /order.serviceId > 3 && model.orderStatus == .Ongoing {
                        self?.destination =   CLLocationCoordinate2D(latitude: CLLocationDegrees(/order.dropOffLatitude) , longitude: CLLocationDegrees(/order.dropOffLongitude))
                    }
                }
                self?.source = updatedDriver
                
                if /self?.btnCurrentLocation.isSelected{
                    guard let path = GMSMutablePath(fromEncodedPath: /model.polyline) else { return }
                    
                    let bounds = GMSCoordinateBounds(path: path)
                    self?.mapView?.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50.0))
                }
                
                
                if self?.currentOrder?.myTurn == OrderTurn.MyTurn{
                    
                    Polyline.points = /model.polyline
                    
                    self?.getPolylineRoute(source: updatedDriver , destination: self?.destination, model: model)
                }
                else{
                    
                }
                guard let `self` = self else { return }
                self.viewDriverAccepted.setOrderStatus(tracking: model, label: self.lblDriverSatus)
                self.imgLogo.isHidden = true
                self.lblDriverSatus.isHidden = false
                
                //  guard let model = data as? Order else { return }
                break
                
            case .EtokenStart:
                
                print("start")
            case .EtokenConfirmed:
                
                print("etoken confirmed")
                
            case .EtokenSerCustPending:
                print("gerg")
                
            case .EtokenCTimeout:
                print("timedout")
                
            case .PaymentPending:
                print("Open web view")
                if ez.topMostVC is WebViewController {
                    return
                }
                guard let htmlData = data as? String else { return }
                guard let webView = R.storyboard.sideMenu.webViewController() else{return}
                webView.htmlString = htmlData
                webView.completeOnRedirect = {
                    
                }
                self?.navigationController?.pushViewController(webView, animated: true)
            }
        }
    }
    
    func getLocalDriverForParticularService(service: Service) {
        
        if APIManager.shared.isConnectedToNetwork() == false {
            alertBoxOk(message:"Validation.InternetNotWorking".localizedString , title: "AppName".localizedString, ok: { [weak self] in
                self?.getUpdatedData()
            })
            return
        }
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        
        let homeAPI =  BookServiceEndPoint.homeApi(categoryID: service.serviceCategoryId)
        homeAPI.request(isLoaderNeeded: false, header: ["language_id" : LanguageFile.shared.getLanguage(), "access_token" :  token]) {
            [weak self] (response) in
            
            switch response {
                
            case .success(let data):
                
                guard let model = data as? Home else { return }
                self?.homeAPI = model
                guard let orders = self?.homeAPI?.orders else {return}
                
                guard let lastCompletedOrder = self?.homeAPI?.orderLastCompleted else {return}
                
                if orders.count > 0 {
                    
                    let latestOrder = orders[0]
                    
                    if latestOrder.orderStatus == .reached || latestOrder.orderStatus == .Confirmed || latestOrder.orderStatus == .Ongoing {
                        
                        self?.currentOrder =  latestOrder
                        
                        if self?.currentOrder?.bookingType == .Present {
                            self?.driverAcceptedMode(order: latestOrder)
                        }else{
                            self?.screenType = ScreenType(mapMode: .NormalMode, entryType: .Forward)
                            
                        }
                        
                    }else if latestOrder.orderStatus == .Searching {
                        
                        if  /latestOrder.orderId != /self?.currentOrder?.orderId  { //coming after killing the app
                            self?.currentOrder =  latestOrder
                            self?.isSearching = true
                            self?.showBasicNavigation(updateType: .OrderDetail)
                            self?.screenType = ScreenType(mapMode: .UnderProcessingMode, entryType: .Forward)
                        }
                    }else {
                        self?.screenType = ScreenType(mapMode: .NormalMode, entryType: .Forward)
                    }
                    
                }else  if lastCompletedOrder.count > 0 {
                    
                    //User need to gice rating to Driver
                    
                    if self?.screenType.mapMode == .RequestAcceptedMode ||  self?.screenType.mapMode == .OnTrackMode {
                        self?.viewDriverAccepted.minimizeDriverView()
                    }
                    
                    self?.checkCancellationPopUp()
                    self?.currentOrder = lastCompletedOrder[0]
                    self?.imgViewPickingLocation.isHidden = true
                    self?.btnLocationNext.isHidden = true
                    self?.viewSelectService.minimizeSelectServiceView()
                    self?.screenType = ScreenType(mapMode: .ServiceDoneMode, entryType: .Forward)
                } else{
                    
                    if self?.screenType.mapMode == .UnderProcessingMode{
                        
                        self?.viewUnderProcess.minimizeProcessingView()
                        if self?.isSearching == true {
                            self?.screenType = ScreenType(mapMode:  .NormalMode , entryType: .Backward)
                            return
                        }
                        self?.screenType = ScreenType(mapMode:  .OrderPricingMode , entryType: .Backward)
                        
                    } else if  self?.screenType.mapMode == .NormalMode || self?.screenType.mapMode == .ZeroMode {
                        self?.btnLocationNext.isHidden = true
                        self?.viewSelectService.minimizeSelectServiceView()
                        self?.screenType = ScreenType(mapMode: .NormalMode, entryType: .Forward)
                    }else if self?.screenType.mapMode == .RequestAcceptedMode || self?.screenType.mapMode == .RequestAcceptedMode  {
                        
                        self?.viewDriverAccepted.minimizeDriverView()
                        self?.screenType = ScreenType(mapMode:  .NormalMode , entryType: .Backward)
                        
                        self?.moveToNormalMode()
                        self?.getNearByDrivers()
                        
                    }
                    
                }
                self?.serviceRequest.serviceSelected?.brands = self?.homeAPI?.brands
                
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
            }
        }
    }
    
    func checkPickup(lat: Double, lng: Double, _ completion: @escaping (() -> ())) {
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        let objRequest = BookServiceEndPoint.checkPickup(pickupLatitude: lat, pickupLongitude: lng)
        objRequest.request(isLoaderNeeded: true, header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token]) {[weak self] (response) in
            switch response {
                
            case .success(let data):
                let restrict = data as? Bool ?? false
                if !restrict {
                    completion()
                }
                else {
                    Alerts.shared.show(alert: "AppName".localizedString, message: "This area is restricted" , type: .error )
                }
                
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
            }
        }
    }
        
    
    
    func bookService() {
        self.isSearching = false
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        listeningEvent()
        let objRequest = BookServiceEndPoint.requestApi(objRequest: serviceRequest,
                                                        categoryId: /serviceRequest.serviceSelected?.serviceCategoryId,
                                                        categoryBrandId: /serviceRequest.selectedBrand?.categoryBrandId,
                                                        categoryBrandProductId: serviceRequest.selectedProduct?.productBrandId,
                                                        productQuantity: serviceRequest.quantity,
                                                        dropOffAddress:  /serviceRequest.locationName,
                                                        dropOffLatitude: /serviceRequest.latitude ,
                                                        dropOffLongitude:  /serviceRequest.longitude,
                                                        pickupAddress: /serviceRequest.locationNameDest,
                                                        pickupLatitude: /serviceRequest.latitudeDest,
                                                        pickupLongitude: /serviceRequest.longitudeDest,
                                                        orderTimings:  /serviceRequest.orderDateTime.toFormattedDateString(),
                                                        future: /serviceRequest.requestType.rawValue,
                                                        paymentType: /serviceRequest.paymentMode.rawValue ,
                                                        distance: 2000,
                                                        organisationCouponUserId: /serviceRequest.eToken?.organisationCouponUserId,
                                                        materialType: /serviceRequest.materialType,
                                                        productWeight: /serviceRequest.weight,
                                                        productDetail:  /serviceRequest.additionalInfo ,
                                                        orderDistance: serviceRequest.distance,
                                                        product_price: serviceRequest.quantity,
                                                        card_id: cardId)
        
        objRequest.request(isImage: true, images: serviceRequest.orderImages , isLoaderNeeded: true, header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token]) {[weak self] (response) in
            switch response {
                
            case .success(let data):
                
                guard let model = data as? Order else { return }
                if /model.orderId == /self?.currentOrder?.orderId { //Accepted socket recieved frist
                    return
                }else{
                    self?.viewOrderPricing.minimizeOrderPricingView()
                    self?.homeAPI?.orders.append(model)
                    self?.currentOrder = model
                    self?.screenType = ScreenType(mapMode: .UnderProcessingMode , entryType: .Forward)
                }
                
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
            }
        }
    }
    
    func cancelRequest(strReason : String , orderID : Int?) {
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        
        let objCancel = BookServiceEndPoint.cancelRequest(orderId: /orderID, cancelReason: strReason)
        objCancel.request(header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token]) {  [weak self] (response) in
            
            if self?.isSearching == true {
                self?.viewUnderProcess.minimizeProcessingView()
                self?.screenType = ScreenType(mapMode: .NormalMode , entryType: .Backward)
                return
            }
            
            switch response {
            case .success(_):
                
                self?.cancelDoneOrder()
                
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
            }
        }
    }
    
    @objc func nCancelFromBookinScreen(notifcation : Notification) {
        if let orderId = notifcation.object as? String, orderId.toInt() == self.currentOrder?.orderId {
            cancelDoneOrder()
        }
    }
    
    func cancelDoneOrder() {
        self.viewUnderProcess.minimizeProcessingView()
        
        if self.currentOrder?.orderStatus == .Confirmed || self.currentOrder?.orderStatus == .Ongoing {
            self.mapView.clear()
            self.cleanPath()
            self.viewDriverAccepted.minimizeDriverView()
            self.currentOrder?.orderStatus = .CustomerCancel
            self.screenType = ScreenType(mapMode: .NormalMode , entryType: .Backward)
            
        }else{
            
            self.currentOrder?.orderStatus = .CustomerCancel
            self.screenType = ScreenType(mapMode: .OrderPricingMode , entryType: .Backward)
        }
    }
    
    func submitRating(rateValue : Int , comment : String) {
        
        guard let orderId = currentOrder?.orderId else{return}
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        let rating = BookServiceEndPoint.rateDriver(orderId: orderId, rating: rateValue, comment: comment)
        
        rating.request(header: ["access_token" :  token]) {[weak self] (response) in
            switch response {
                
            case .success(_):
                
                self?.viewDriverRating.minimizeDriverRatingView()
                self?.mapView.clear()
                self?.cleanPath()
                self?.screenType = ScreenType(mapMode: .NormalMode , entryType: .Backward)
                
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
            }
        }
    }
    
    func updateData() {
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        print(LanguageFile.shared.getLanguage())
        let updateDataVC = LoginEndpoint.updateData(fcmID: UserDefaultsManager.fcmId)
        updateDataVC.request(isLoaderNeeded: false, header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" :  token]) { [weak self] (response) in
            switch response {
                
            case .success(let data):
                
                guard let model = data as? Home else { return }
                
                if let userData = UDSingleton.shared.userData {
                    userData.userDetails = model.userDetail
                    userData.services = model.services
                    userData.support = model.support
                    UDSingleton.shared.userData = userData
                }
                
                guard let version = model.version  else { return }
                guard  let forceVersion = version.iOSVersion?.user?.forceVersion  else { return }
                
                self?.checkUpdate(strForce: forceVersion)
                
            case .failure(let strError):
                
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
            }
        }
    }
}

extension HomeVC: GMSMapViewDelegate {
    
    func getDistance( newPosition : CLLocation , previous : CLLocation ) -> Float {
        return Float(newPosition.distance(from: previous))
    }
    
    func  getPolylineRoute(source: CLLocationCoordinate2D? , destination: CLLocationCoordinate2D?, model: TrackingModel? ){
        
        if isDoneCurrentPolyline {
            
            isDoneCurrentPolyline = false
            
            // driver giving direction data, and time
            
            if model != nil && model?.polyline != ""  {
                if self.currentOrder?.orderStatus == .CustomerCancel  {return}
                self.durationLeft = /model?.etaTime
                self.updateDriverCustomerMarker(driverLocation: self.source, customerLocation: self.destination , trackString: model?.polyline)
                self.isDoneCurrentPolyline = true
                return
            }
            
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            
            let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(/source?.latitude),\(/source?.longitude)&destination=\(/destination?.latitude),\(/destination?.longitude)&sensor=true&mode=driving&key=\(APIBasePath.googleApiKey)")!
            
            let task = session.dataTask(with: url, completionHandler: { (data, response, error) in
                if error != nil {
                    
                    self.isDoneCurrentPolyline = true
                    debugPrint(error!.localizedDescription)
                    
                }else {
                    do {
                        
                        if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any] {
                            DispatchQueue.global(qos: .background).async { [weak self] () -> Void in
                                guard let routes = json["routes"] as? NSArray else {return}
                                
                                if (routes.count > 0) {
                                    
                                    let overview_polyline = routes[0] as? NSDictionary
                                    let dictPolyline = overview_polyline?["overview_polyline"] as? NSDictionary
                                    
                                    guard let points = dictPolyline?.object(forKey: "points") as? String else { return }
                                    
                                    guard let legs  = routes[0] as? NSDictionary , let legsJ = legs["legs"] as? NSArray , let lg = legsJ[0] as? NSDictionary else { return }
                                    let duration = lg["duration"] as? NSDictionary
                                    let durationLeft1 = duration?.object(forKey: "text") as? String
                                    
                                    if self?.currentOrder?.orderStatus == .CustomerCancel  {return}
                                    
                                    self?.durationLeft = /durationLeft1
                                    self?.updateDriverCustomerMarker(driverLocation: self?.source, customerLocation: self?.destination , trackString: points)
                                    self?.isDoneCurrentPolyline = true
                                }
                            }
                        }
                    }catch {
                        debugPrint("error in JSONSerialization")
                    }
                }
            })
            task.resume()
        }
    }
    
    func showPath(polyStr :String){
        
        ez.runThisInMainThread {
            [weak self] in
            guard let self = self else { return }
            
            let path = GMSMutablePath(fromEncodedPath: polyStr) // Path
            self.polyPath = path
            self.polyline = GMSPolyline(path: path)
            self.polyline?.geodesic = true
            self.polyline?.strokeWidth = 4
            
            self.polyline?.strokeColor  = self.btnNormal.isSelected ? .darkGray : .white

            self.polyline?.map = self.mapView
            self.cleanPath()
            self.timer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(self.animatePolylinePath), userInfo: nil, repeats: true)
            
            
        }
    }
    
    func cleanPath() {
        if self.timer != nil {
            self.timer.invalidate()
            self.timer = nil
        }
    }
    
    @objc func animatePolylinePath() {
        guard let path = polyPath else { return }
        if (self.i < path.count()) {
            self.animationPath.add(path.coordinate(at: self.i))
            self.animationPolyline.path = self.animationPath
            self.animationPolyline.strokeColor = UIColor.appYellow
            self.animationPolyline.strokeWidth = 4
            self.animationPolyline.map = self.mapView
            self.i += 1
        }
        else {
            self.i = 0
            self.animationPath = GMSMutablePath()
            self.animationPolyline.map = nil
        }
    }
    
    
    
    func updateDriverCustomerMarker( driverLocation : CLLocationCoordinate2D? , customerLocation  : CLLocationCoordinate2D? , trackString : String?) {
        
        self.cleanPath()
        
        ez.runThisInMainThread { [weak self] in
            
            guard let source = driverLocation , let destination = customerLocation  else { return }
            guard let currOrder = self?.currentOrder else {return}
            
            self?.viewDriverAccepted?.lblTimeEstimation?.text = self?.durationLeft
            
            if /currOrder.serviceId > 3 {
                self?.userMarker.icon = currOrder.orderStatus == .Ongoing ? #imageLiteral(resourceName: "DropMarker") : #imageLiteral(resourceName: "PickUpMarker")
            }else{
                self?.userMarker.icon = #imageLiteral(resourceName: "DropMarker")
            }
            self?.mapView.clear()
            self?.cleanPath()
            self?.userMarker.isFlat = false
           self?.userMarker.title = "kirti"
            //self?.london.map = mapView
            self?.userMarker.snippet = "booopitiiboop"
            self?.userMarker.map = self?.mapView
            self?.userMarker.position = CLLocationCoordinate2D(latitude: destination.latitude, longitude: destination.longitude)
            
            self?.driverMarker.icon = UIImage().getDriverImage(type: /currOrder.serviceId)
            self?.driverMarker.isFlat = false
            self?.driverMarker.map = self?.mapView
            
            CATransaction.begin()
            CATransaction.setAnimationDuration(2.5)
            
            self?.driverMarker.position = CLLocationCoordinate2D(latitude: source.latitude, longitude: source.longitude)
            self?.driverMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
            if let degrees = self?.driverBearing  {
                self?.driverMarker.rotation = /CLLocationDegrees(degrees)
            }
            CATransaction.commit()
            guard let lineTrack = trackString  else { return }
            self?.showPath(polyStr: lineTrack)
        }
    }
}

//MARK:- Custom Delegates
//MARK:-

extension HomeVC : BookRequestDelegate {
    
    func didBuyEToken(brandId :Int, brandProductId:Int) {
        
        guard let etoken = R.storyboard.drinkingWater.tokenListingVC() else{return}
        
        print(brandId)
        
        CouponSelectedLocation.categoryId = brandId
        CouponSelectedLocation.categoryBrandId = brandProductId
        CouponSelectedLocation.selectedAddress = /serviceRequest.locationName
        CouponSelectedLocation.Brands = serviceRequest.serviceSelected?.brands
        CouponSelectedLocation.latitude = /serviceRequest.latitude
        CouponSelectedLocation.longitude = /serviceRequest.longitude
        
        self.pushVC(etoken)
        
    }
    
    func didPaymentModeChanged(paymentMode: PaymentType) {
        serviceRequest.paymentMode = paymentMode
        viewOrderPricing.updatePaymentMode(service: serviceRequest)
    }
    
    func didGetRequestDetails(request: ServiceRequest) {
        
        self.serviceRequest = request
        let mapmode : MapMode  = serviceRequest.requestType == .Present  ? .OrderPricingMode : .ScheduleMode
        screenType = ScreenType(mapMode: mapmode , entryType: .Forward)
    }
    
    func didGetRequestDetailWithScheduling(request: ServiceRequest) {
        self.serviceRequest = request
        screenType = ScreenType(mapMode: .OrderPricingMode , entryType: .Forward)
    }
    
    func didRequestTimeout() {
        orderEventFire()
        
    }
    
    func didSelectServiceType(_ serviceSelected: Service) {
        
        serviceRequest.serviceSelected = serviceSelected
        currentService = /serviceSelected.serviceCategoryId
        getLocalDriverForParticularService(service: serviceSelected)
        locationEdit = currentService > 3 ? .PickUp : .DropOff
        
    }
    
    func didSelectBrandProduct(_ brand: Brand, _ product: Product) {
        serviceRequest.selectedBrand = brand
        serviceRequest.selectedProduct = product
        
    }
    
    func didSelectQuantity(count: Int) {
        serviceRequest.quantity = count
        
    }
    
    func didSelectNext(type: ActionType) {
        
        if type == .SelectLocation {
            
            guard let serviceID = serviceRequest.serviceSelected?.serviceCategoryId else{return}
            
            // Change here for different service id

            self.locationEdit =  serviceID > 3  ?  .PickUp : .DropOff
//            self.locationEdit =  serviceID < 3  ?  .PickUp : .DropOff

            screenType = ScreenType(mapMode: .SelectingLocationMode , entryType: .Forward)
            changeLocationType(move: .Forward)
            
        }else if type == .NextGasType {
            
            screenType = ScreenType(mapMode: .OrderPricingMode , entryType: .Forward)
        }else if type == .SubmitOrder {
            
            guard let serviceID = serviceRequest.serviceSelected?.serviceCategoryId else{return}
            
            if serviceID == 12 || serviceID == 4 || serviceID == 3 || serviceID == 2 || serviceID == 7{
                
                //self.viewOrderPricing.minimizeOrderPricingView()
                 //self?.homeAPI?.orders.append(model)
            
//self.screenType = ScreenType(mapMode: .UnderProcessingMode , entryType: .Forward)
                bookService()
                
            }else {
                showWorkingProgressVC()
            }
            
        }else if type == .CancelOrderOnSearch {
            
            cancelRequest(strReason: reasonString, orderID: /currentOrder?.orderId)
            
        }else if type == .Payment {
            
            //let notification = NotificationCenter.default
            NotificationCenter.default.addObserver(self, selector: #selector(HomeVC.etokenNotification(notifcation:)), name: Notification.Name(rawValue: LocalNotifications.eTokenSelected.rawValue), object: nil)
            
            //            guard let paymentVC = R.storyboard.bookService.paymentVC() else{return}
            //            paymentVC.categoryId = self.serviceRequest.serviceSelected?.serviceCategoryId
            //            paymentVC.paymentType = serviceRequest.paymentMode
            //            paymentVC.delegate = self
            //
            //            pushVC(paymentVC)
            guard let vc = R.storyboard.bookService.cardViewController() else { return }
            vc.orderSummary = serviceRequest
            vc.selectedCardBlock = {[weak self] (id, request) in
                self?.serviceRequest = request
                self?.serviceRequest.paymentMode = .Card
                self?.cardId = id
                self?.didPaymentModeChanged(paymentMode: .Card)
            }
            pushVC(vc)

            
        }else if type == .DoneInvoice {
            showDriverRatingView()
            
        }else if type == .CancelTrackingOrder {
            
            showCancellationFormVC()
        } else if type == .BackFromFreightBrand {
            //back pressed on freight Brand View
            screenType = ScreenType(mapMode: .SelectingLocationMode, entryType: .Backward)
            
        } else if type == .BackFromFreightProduct {
            //back pressed on freight product View
            screenType = ScreenType(mapMode: .SelectingFreightBrand, entryType: .Backward)
        }
        
      //  showWorkingProgressVC()
    }
    
    func didRatingSubmit(ratingValue: Int, comment: String) {
        submitRating(rateValue: ratingValue, comment: comment)
    }
    
    func didSelectSchedulingDate(date: Date, minDate:Date) {
        showSchedular(date: date, minDate: minDate)
    }
    
    func didSelectedFreightBrand(brand : Brand)  {
        
        serviceRequest.selectedBrand = brand
        
        switch /serviceRequest.serviceSelected?.serviceCategoryId {
        case 7, 12: // Car and bike booking
            serviceRequest.selectedProduct = brand.products?.first
            serviceRequest.requestType = .Present
            serviceRequest.orderDateTime = Date()
            didGetRequestDetails(request: serviceRequest)
        case 4: //Pick Up And Delivery
            screenType = ScreenType(mapMode: .SelectingFreightProduct , entryType: .Forward)
        default: break
        }
        

//        if brand.categoryId == 7, let product = brand.products?.first {
//
//            serviceRequest.selectedProduct = product
//            serviceRequest.requestType = .Present
//            serviceRequest.orderDateTime = Date()
//            didGetRequestDetails(request: serviceRequest)
//            //            didSelectedFreightProduct(product: product)
//        } else {
//            screenType = ScreenType(mapMode: .SelectingFreightProduct , entryType: .Forward)
//        }
        
    }
    
    func didSelectedFreightProduct(product : Product) {
        
        serviceRequest.selectedProduct = product
        screenType = ScreenType(mapMode: (product.productBrandId == 32) ? .OrderPricingMode  : .OrderDetail , entryType: .Forward) //  . OrderPricingMode for booking cab
        
    }
}

//MARK:- TextField Delegates
//MARK:-

extension HomeVC :UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
            if textField == self.txtDropOffLocation {
                self.isPickUpLocation = false
                self.locationEdit = .DropOff
                
                if let latitude =  self.serviceRequest.latitude , let longitude = self.serviceRequest.longitude  , let previousSelected = self.serviceRequest.locationName {
                    
                    textField.text = previousSelected
                    self.setCameraGoogleMap(latitude: latitude, longitude: longitude)
                }
                
            }else if textField == self.txtPickUpLocation {
                self.isPickUpLocation = true
                self.locationEdit = .PickUp
                if let latitude =   self.serviceRequest.latitudeDest , let longitude = self.serviceRequest.longitudeDest  , let previousSelected = self.serviceRequest.locationNameDest {
                    
                    textField.text = previousSelected
                    self.setCameraGoogleMap(latitude: latitude, longitude: longitude)
                    
                    
                }
            }
        
        
    }
}
