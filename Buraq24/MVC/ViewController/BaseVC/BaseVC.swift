//
//  BaseVC.swift
//  Buraq24
//
//  Created by MANINDER on 29/08/18.
//  Copyright © 201ic_back8 CodeBrewLabs. All rights reserved.
//

import UIKit

class BaseVC: UIViewController {
    //MARK:- Outlet
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var lblTitle: UILabel!
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let imgBack = #imageLiteral(resourceName: "Back")
        
    btnBack.setImage(imgBack.setLocalizedImage(), for: .normal)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
     //MARK:- Actions
    
    @IBAction func actionBtnBackPressed(_ sender: UIButton) {
        self.popVC()
    }
    
    
    //MARK:- Functions

    func updateTitle(strTitle : String) {
        lblTitle.text = strTitle
    }
   
    func getLanguageFromCode(code:String) -> String {
        
         switch code {
         case "1":
             return "LanguageName.English".localized
         case "2":
            return "LanguageName.Spanish".localized
         default:
             return "LanguageName.English".localized
         }
     }
}
