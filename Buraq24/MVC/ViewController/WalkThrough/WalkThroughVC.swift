//
//  TutorialVC.swift
//  Buraq24
//
//  Created by Apple on 24/09/19.
//  Copyright © 2019 CodeBrewLabs. All rights reserved.
//

import UIKit

class WalkThroughVC: UIViewController {

    @IBOutlet weak var pageController: UIPageControl!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        onViewDidLoad()
        
    }
    
    @IBAction func btnSkipAction(_ sender: Any) {
        guard let vc = R.storyboard.main.landingVC() else { return}
        pushVC(vc)
        
    }
}

//MARK: -----> Custom Method
extension WalkThroughVC {
    func onViewDidLoad() {
        UDSingleton.shared.walkThrough = true
        pageController.numberOfPages = 3
        let vc = childViewControllers.first as? PageVC
        vc?.pageViewBlock = { [unowned self] index in
            self.pageController.currentPage = index
            
        }
    }
}
