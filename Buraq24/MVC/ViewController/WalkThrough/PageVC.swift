//
//  PageVC.swift
//  Buraq24
//
//  Created by Apple on 24/09/19.
//  Copyright © 2019 CodeBrewLabs. All rights reserved.
//

import UIKit
import Rswift

class PageVC: UIPageViewController {

    //MARK: -----> Outlets
    fileprivate lazy var pages: [UIViewController] = {
        return [
                    self.getViewController(withIdentifier: "Illustration1VC"),
                    self.getViewController(withIdentifier: "Illustration2VC"),
                    self.getViewController(withIdentifier: "Illustration3VC")
        ]
    }()
    
    fileprivate func getViewController(withIdentifier identifier: String) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: identifier)
        
    }
    
    //MARK: -----> Properties
    var pageViewBlock: ((Int) -> ())?
    
    
    //Mark: -----> Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        onViewDidLoad()
        
    }
}

//MARK: -----> Custom Method
extension PageVC {
    func onViewDidLoad() {
        self.dataSource = self
        self.delegate   = self
        
        if let firstVC = pages.first {
            setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
            
        }
    }
}

//MARK: -----> Custom Action
extension PageVC {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = pages.index(of: viewController) else { return nil }

        if let block = pageViewBlock {
            block(viewControllerIndex)
            
        }
        let previousIndex = viewControllerIndex - 1
        
        if previousIndex < 0 {
            return nil
        }
        
        return pages[previousIndex]
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = pages.index(of: viewController) else { return nil }
        if let block = pageViewBlock {
            block(viewControllerIndex)
            
        }
        let nextIndex = viewControllerIndex + 1
        if nextIndex >= pages.count {
            return nil
            
        }
        
        return pages[nextIndex]
    }
    
}

//MARK: -----> Delegate and DataSource
extension PageVC: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
}
