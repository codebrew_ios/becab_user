//
//  CardViewControllerViewController.swift
//  SideDrawer
//
//  Created by Apple on 16/11/18.
//  Copyright © 2018 Codebrew Labs. All rights reserved.
//

import UIKit

class CardViewController: BaseVC {
    
    //MARK:- OUTLETS
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnAddCard: UIButton!

    //MARK:- PROPERTIES
    
    var arrayModalCardList = [CardDataModal]()
    var isFirstTime        = true
    var isFromGenerateReport : Bool?
    var isFromHomeController : Bool?
    var orderSummary : ServiceRequest?
    var selectedCardBlock:((_ selectedCardId: String, _ serviceRequest: ServiceRequest) -> ())?

    var dataSource: TableViewDataSource?
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        apiGetCards(isLoaderNeed : isFirstTime)
        isFirstTime = false
    }
    
    //MARK: - Functions
    
    func initialSetup() {
        setupTableView()
    }
    
    func setupTableView() {
        
        let  configureCellBlock : ListCellConfigureBlock = { [weak self] ( cell , item , indexpath) in
            
            guard let cell = cell as? CardTableViewCell else {return}
            cell.obj = item as? CardDataModal
            cell.row = indexpath.row
            cell.delegate = self
        }
        
        let didSelectCellBlock : DidSelectedRow = { [weak self] (indexPath , cell, item) in
            guard let `self` = self else { return }
            let cardID = self.arrayModalCardList[indexPath.row].cardToken
            guard let detail = self.orderSummary else { return }
            self.selectedCardBlock?(/cardID, detail)
            self.popVC()
        }
        
        dataSource = TableViewDataSource(items: nil, tableView: tableView, cellIdentifier: R.reuseIdentifier.cardTableViewCell.identifier, cellHeight: 135)
        dataSource?.configureCellBlock = configureCellBlock
        dataSource?.aRowSelectedListener = didSelectCellBlock
        tableView.delegate = dataSource
        tableView.dataSource = dataSource
        reloadTable()
        
        
//        dataSource?.aRowSelectedListener = {[weak self] (indexPath) in
//
//            if /self?.isFromGenerateReport {
//
//                UtilityFunctions.runThisInMainThread {
//
//                    UtilityFunctions.show(alert: "", message: UtilityMessages.paymentConfirmation.rawValue , buttonOk: { [ weak self ] in
//
//                        let cardID = self?.arrayModalCardList[indexPath.row].cardToken
//
//                        if /self?.isFromHomeController {
//                            self?.apiSendMasterReports(cardID: cardID, stripeToken:nil)
//                        } else {
//                            self?.apiSendReports(cardID: cardID, stripeToken:nil)
//                        }
//                        }, viewController: /self, buttonText: StaticStrings.ok.rawValue)
//                }
//            }
//        }
    }
    
    func moveToNextController() {
        
//        UtilityFunctions.runThisInMainThread {
//
//            guard let vc =  R.storyboard.tabBar.addCardViewController() else {return }
//            vc.hidesBottomBarWhenPushed = true
//            self.pushVC(vc)
//
//            guard let index = self.navigationController?.viewControllers.index(of: self) else{return}
//            self.navigationController?.viewControllers.remove(at: index)
//        }
    }
    
    func updateUserModal() {
        
//        let user = UserData.share.loggedInUser
//        user?.cardAdded = false
//        UserData.share.loggedInUser = user
    }
    
    func reloadTable() {
        self.tableView.reloadData()
        if arrayModalCardList.count == 0 {
            btnAddCard.setTitle("Add Card", for: .normal)
        }
        else {
            btnAddCard.setTitle("Add Another Card", for: .normal)
        }
    }
    
    //MARK:- Button Selector
    
    @IBAction func buttonAddAnotherClicked(_sender : Any) {
        
        guard let vc = R.storyboard.bookService.addCardViewController() else {return}
        vc.isFromCardController = true
        self.pushVC(vc)
    }
    
    //MARK:- API
    
    func apiGetCards(isLoaderNeed : Bool) {
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        let api = BookServiceEndPoint.getUserCardList
        api.request(isLoaderNeeded: isLoaderNeed, header: ["language_id" : LanguageFile.shared.getLanguage(), "access_token" :  token]) {
            [weak self] (response) in
            switch response {
            case .success(let value) :
                
                guard let resp = (value as? [CardDataModal]) else { return }
                self?.arrayModalCardList = resp
                
                self?.dataSource?.items = self?.arrayModalCardList
                self?.reloadTable()
                
            case .failure(let error):
                debugPrint(error ?? "")
            }
        }
    }
    
    func apiDeleteCard(cardID : String?, index : Int?) {
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        let api = BookServiceEndPoint.deleteUserCard(cardId: cardID)
        api.request(isLoaderNeeded: true, header: ["language_id" : LanguageFile.shared.getLanguage(), "access_token" :  token]) {
            [weak self] (response) in
            switch response {
            case .success(let value) :

                Alerts.shared.show(alert: "AppName".localizedString, message: "Card deleted" , type: .success )

                    self?.arrayModalCardList.remove(at: /index)
                    
                    self?.dataSource?.items = self?.arrayModalCardList
                    self?.reloadTable()
                    
                    if /self?.arrayModalCardList.isEmpty {
                        
                        self?.updateUserModal()
                        self?.moveToNextController()
                    }
                
            case .failure(let error):
                debugPrint(error ?? "")
            }
        }
    }
}

//MARK:- CardTableViewCellDelegate
extension CardViewController : CardTableViewCellDelegate {
    
    func buttonClicked(index: Int?) {
        
        let alertController = UIAlertController(title: "Delete Card", message: "Are you sure you want to delete the card?", preferredStyle: UIAlertControllerStyle.alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
        alertController.addAction(cancelAction)
        let deleteAction = UIAlertAction(title: "Delete", style: UIAlertActionStyle.default, handler: { [weak self] action in
            guard let id = self?.arrayModalCardList[/index].cardToken else {return}
            self?.apiDeleteCard(cardID: id, index: index)
        })
        alertController.addAction(deleteAction)
        self.view.window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
}
