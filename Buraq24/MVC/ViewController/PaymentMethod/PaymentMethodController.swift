//
//  PaymentMethodController.swift
//  Clikat
//
//  Created by cblmacmini on 5/23/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import Material
import EZSwiftExtensions
import SwiftyJSON
//import CryptoSwift
//import Adjust

enum PaymentMethod : String {
    
    case COD = "0"
    case Card = "1"
    case DoesntMatter = "2"
    
    
    func indexValue () -> Int {
        
        switch self {
        case .COD:
            return 0
        case .Card:
            return 1
        default:
            return 2
        }
    }
    
    static let allValues = ["Cash on Delivery" , "Card" , "Both"]
    
    func paymentMethodString() -> String{
        
        switch self {
        case .COD:
            return "CashOnDelivery"
            
        case .Card:
            return "Card"
            
        default:
            return "Both"
            
        }
        
    }
    
    
    func visibilityBasedOnDelivery (withImgCOD imgCOD : UIImageView? , imgCard : UIImageView?){
        
        
        switch self {
        case .DoesntMatter :
            imgCOD?.isHidden = false
            imgCard?.isHidden = false
            
        case .Card :
            imgCOD?.isHidden = true
            imgCard?.isHidden = false
            
        case .COD :
            imgCOD?.isHidden = false
            imgCard?.isHidden = true
        }
        
    }
}

class PaymentMethodController: BaseVC {
    @IBOutlet weak var btnPayment: UIButton!
    @IBOutlet weak var btnCOD: UIButton!{
        didSet{
            btnCOD.setAttributedTitle(attributedStringWithImage( string: "CashOnDelivery" , image: Asset.Ic_payment_cash), for: .normal)
        }
    }
    @IBOutlet weak var btnCreditCard: UIButton!{
        didSet{
            btnCreditCard.setAttributedTitle(attributedStringWithImage(string: "CreditDebitCard", image: Asset.Ic_payment_card), for: .normal)
        }
    }
    var addOrder : Bool = false
    var getDta : getCard?
    var fromPayment : Bool = true
    
    @IBOutlet weak var viewBgCardDetails: View!
    @IBOutlet weak var viewCardHolderName: View!
    @IBOutlet weak var viewCardNumber: View!
    @IBOutlet weak var viewExpiry: View!
    @IBOutlet weak var lblChoosePayment: ThemeLabel!
    @IBOutlet weak var btnDone: ThemeButton!
    @IBOutlet weak var btnAddNewCard: ThemeButton!
    @IBOutlet weak var tfCardHoldersName : UITextField!
    @IBOutlet weak var tfCardNumber: UITextField!
    @IBOutlet weak var tfCVV: UITextField!
    @IBOutlet weak var tfExpiryDate: UITextField!
    
    @IBOutlet weak var tblView: UITableView!
    
    var tableDataSource : TableViewDataSource?{
        didSet{
            
            tblView.reloadData()
        }
    }
    var tokenId : String?
    var selectedPaymentMethod : PaymentMethod = .COD
    var orderId : String?
    var orderSummary : ServiceRequest?
    
    var isConfirmOrder : Bool = false
    var list: [CardsModel]?
    var selectedCardBlock:((_ selectedCardId: String, _ serviceRequest: ServiceRequest) -> ())?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBgCardDetails.isHidden = true
        btnPayment.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        configureTableView()
        self.getCardAPI()
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @IBAction func btnAddNewCardAct(_ sender: UIButton) {
        btnPayment.setTitle("Done", for: .normal)
        viewBgCardDetails.isHidden = false
        btnPayment.isHidden = false
        tblView.isHidden = true
        
    }
    
    @IBAction func btnDoneAct(_ sender: ThemeButton) {
        if sender.currentTitle == "Done" {
            getToken()
            
        }else {
            guard let arr = list else { return }
            let card = arr.filter{$0.isSelected}.first
            guard let block = selectedCardBlock, let cardData = card, let serviceRequest = orderSummary else { return }
            block(/cardData.card_id, serviceRequest)
            popVC()
        }
    }
}

extension PaymentMethodController {
    
    func attributedStringWithImage(string : String ,image : Asset) -> NSMutableAttributedString {
        let attachment = NSTextAttachment()
        attachment.image = UIImage(asset : image)
        let attachmentString = NSAttributedString(attachment: attachment)
        let myString = NSMutableAttributedString(string: "")
        myString.append(attachmentString)
        myString.append(NSAttributedString(string: "   "))
        myString.append(NSAttributedString(string: string))
        return myString
    }
    
}

//MARK: - Button Actions
extension PaymentMethodController {
    
    @IBAction func actionCod(sender: UIButton) {
        if sender.isSelected { return }
        sender.isSelected = true
        btnCreditCard.isSelected = false
        selectedPaymentMethod = .COD
        
        //        sender.isSelected = !sender.isSelected
        //        btnCreditCard.isSelected = !sender.isSelected
        viewBgCardDetails.isHidden = true
        btnPayment.setTitle("Finish", for: .normal)
        toggleInfoView()
        
        addOrder = false
        
    }
    
    @IBAction func actionCard(sender: UIButton) {
        //        if sender.isSelected { return }
        //        sender.isSelected.toggle()
        // btnCOD.isSelected.toggle()
        // viewBgCardDetails.isHidden = false
        //        toggleInfoView()
        //        fromPayment ? (btnAddNewCard.isHidden = false) : (btnAddNewCard.isHidden = true)
        
        sender.isSelected = true
        btnCOD.isSelected = false
        addOrder = false
        selectedPaymentMethod = .Card
        guard let VC = R.storyboard.bookService.cardCountVC() else { return }
        VC.delegate = self
        VC.fromPayment = fromPayment
        presentVC(VC)
        // pushVC(VC)
        toggleInfoView()
        btnCOD.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
    }
    
    @IBAction func actionFinish(sender: UIButton) {
        
        if selectedPaymentMethod == .Card{
            getToken()
            
        }else{
            confirmOrder()
        }
        
    }
    func confirmOrder(){
        if isConfirmOrder {
            //            APIManager.sharedInstance.opertationWithRequest(withApi: API.ConfirmScheduleOrder(FormatAPIParameters.ConfirmScheduleOrder(orderId: orderId, paymentType: selectedPaymentMethod.rawValue).formatParameters()), completion: {
            //                (response) in
            //                switch response {
            //                case .Success(_):
            //
            //                    UtilityFunctions.showSweetAlert(title : L10n.Success.string, message: L10n.OrderConfirmedSuccessfully.string, style: .Success, success: {
            //
            //                        if self.orderSummary?.selectedDeliverySpeed != .scheduled  {
            //
            //                            let VC = StoryboardScene.Order.instantiateOrderDetailController()
            //                            VC.isBuyOnly = /self.orderSummary?.isBuyOnly
            //                            VC.orderDetails = OrderDetails(orderId: self.orderId)
            //                            VC.type = .OrderUpcoming
            //                            VC.isConfirmOrder = true
            //                            self.pushVC(VC)
            //                        }
            //
            //                    })
            //
            //                default: break
            //                }
            //            })
            //            return
        }else if selectedPaymentMethod != .Card {
            generateOrder()
            return
        }else{
        }
        //        APIManager.sharedInstance.showLoader()
        //        HTTPClient().postPayFortRequest { [weak self] (response) in
        //            APIManager.sharedInstance.hideLoader()
        //            switch response {
        //            case .Success(let object):
        //                self?.startPayfortRequest(sdkToken: (object as? PayFort)?.sdkToken)
        //            case .Failure(let validation):
        //                print(validation.message)
        //            }
        //        }
    }
    
    
    func configureTableView(){
        let  configureCellBlock : ListCellConfigureBlock = { [weak self] ( cell , item , indexpath) in
            guard let self = self else { return }
            self.configureCell(withCell: cell, item: item)
            
        }
        
        let didSelectCellBlock : DidSelectedRow = { [weak self] (indexPath , cell, item) in
            self?.didSelect(index: indexPath.row)
            
        }
        
        tableDataSource = TableViewDataSource(items: list, tableView: tblView, cellIdentifier: "CardTblCell", cellHeight: UITableViewAutomaticDimension)
        tableDataSource?.configureCellBlock = configureCellBlock
        tableDataSource?.aRowSelectedListener = didSelectCellBlock
        tblView.delegate = tableDataSource
        tblView.dataSource = tableDataSource
        tblView.reloadData()
        
    }
    
    func didSelect(index: Int) {
        guard let arr = list else { return }
        for (i, item) in arr.enumerated() {
            item.isSelected = false
            list?[i] = item
        }
        
        list?[index].isSelected = true
        
        tableDataSource?.items = list
        tblView.reloadData()
        
    }
    
    func configureCell(withCell cell : Any , item : Any? ){
        
        guard let tempCell = cell as? CardTblCell else{
            return
            
        }
        
        tempCell.blockSelect = {
            [weak self] dtaCard in
            guard let self = self else { return }
            
            
            self.alertBoxOption(message: "Are you sure you want to delete", title: "Delete" , leftAction: "no".localizedString , rightAction: "yes".localizedString , ok: { [weak self] in
                
                self?.hitDelete(/dtaCard?.card_id)
                
            })
        }
        
        
        tempCell.getCrd = item as? CardsModel
        //        tempCell.subCategory = item as? SubCategory
    }
    
    
}
//MARK:- DELEGATE FUNCTION
extension PaymentMethodController : CardAdd{
    func clickAddNew() {
        viewBgCardDetails.isHidden = false
        selectedPaymentMethod = .Card
        btnCreditCard.isSelected = true
        btnCOD.isSelected = false
        btnPayment.isHidden = false
        toggleInfoView()
    }
    
    func card(_ data: getCard?, isCard: Bool) {
        if isCard{
            addOrder = false
            
            btnAddNewCard.isHidden = false
            tfCardNumber.text = /data?.card_number
            tfCardHoldersName.text = /data?.holder_name
            tfExpiryDate.text = /data?.expiration_month + "/\(/data?.expiration_year)"
            tfCVV.text = /data?.bank_code
            btnCreditCard.isSelected = true
            btnCOD.isSelected = false
            self.getDta = data
            toggleInfoView()
            
            generateOrder()
        }else{
            //            fromPayment ? (btnAddNewCard.isHidden = false) : (btnAddNewCard.isHidden = true)
            
            viewBgCardDetails.isHidden = true
            
        }
    }
}

//MARK: - Credit Card Info Animation
extension PaymentMethodController {
    
    func toggleInfoView(){
        
        if btnCOD.isSelected {
            selectedPaymentMethod = .COD
        }else {
            selectedPaymentMethod = .Card
            
            //            btnCOD.setTitleColor(SKAppType.type.color, for: .normal)
        }
    }
}
//MARK: - TextField Actions

extension PaymentMethodController : UITextFieldDelegate {
    
    @IBAction func textFieldEditingChanged(sender: BKCardNumberField) {
        
        let formatedtext = formatCreditCardNumber(inputText: sender.text ?? "")
        
        if formatedtext != tfCardNumber.text {
            tfCardNumber.text = formatedtext
        }
        if tfCardNumber.text?.characters.count == 19 {
            tfCardNumber.resignFirstResponder()
        }
    }
    
    func formatCreditCardNumber(inputText : String) -> String{
        
        var output = ""
        switch inputText.characters.count {
        case 1...4:
            output = inputText
        case 5...8:
            let firstStr = String(inputText[..<inputText.index(inputText.startIndex, offsetBy: 4)])
            let lastStr = String(inputText[inputText.index(inputText.startIndex, offsetBy: 4)...])
            output = String(format: "%@-%@", arguments: [firstStr,lastStr])
        case 9...12:
            let firstStr = String(inputText[..<inputText.index(inputText.startIndex, offsetBy: 4)])
            
            let middleStr = String(inputText[inputText.index(inputText.startIndex, offsetBy: 4)..<inputText.index(inputText.startIndex, offsetBy: 8)])
            
            let lastStr = String(inputText[inputText.index(inputText.startIndex, offsetBy: 8)...])
            
            output = String(format: "%@-%@-%@", arguments: [firstStr,middleStr,lastStr])
        case 13...16:
            let firstStr = String(inputText[..<inputText.index(inputText.startIndex, offsetBy: 4)])
            
            let middleStr1 = String(inputText[inputText.index(inputText.startIndex, offsetBy: 4)..<inputText.index(inputText.startIndex, offsetBy: 8)])
            
            let middleStr2 = String(inputText[inputText.index(inputText.startIndex, offsetBy: 8)..<inputText.index(inputText.startIndex, offsetBy: 12)])
            
            let lastStr = String(inputText[inputText.index(inputText.startIndex, offsetBy: 12)...])
            
            output = String(format: "%@-%@-%@-%@", arguments: [firstStr,middleStr1,middleStr2,lastStr])
        default:
            output = ""
        }
        return output
    }
}

//MARK: - GenerateOrder Web service

extension PaymentMethodController {
    func addCard(conektaToken: ConektaToken){
        let card = conektaToken.card
        if APIManager.shared.isConnectedToNetwork() == false {
            alertBoxOk(message:"Validation.InternetNotWorking".localizedString , title: "AppName".localizedString, ok: {
            })
            return
        }
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        let addUserCardAPI =  BookServiceEndPoint.addUserCard(token_id: /tokenId, cardHolder: /card?.name, month: /card?.expMonth, year: /card?.expYear)
        addUserCardAPI.request(isLoaderNeeded: true, header: ["language_id" : LanguageFile.shared.getLanguage(), "access_token" :  token]) { [weak self] (response) in
            
            switch response {
                
            case .success(_):
                self?.viewBgCardDetails.isHidden = true
                self?.btnPayment.setTitle("Finish", for: .normal)
                self?.getCardAPI()
                
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
            }
        }
    }
    
    func hitDelete(_ id: String ) {
        if APIManager.shared.isConnectedToNetwork() == false {
            alertBoxOk(message:"Validation.InternetNotWorking".localizedString , title: "AppName".localizedString, ok: {
            })
            return
        }
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        let deleteAPI =  BookServiceEndPoint.deleteUserCard(cardId: id)
        
        deleteAPI.request(isLoaderNeeded: true, header: ["language_id" : LanguageFile.shared.getLanguage(), "access_token" :  token]) { [weak self] (response) in
            
            switch response {
                
            case .success(_):
                self?.getCardAPI()
                
                
            case .failure(let strError):
                self?.tblView.isHidden = true
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
                
            }
        }
        
    }
    
    func getCardAPI() {
        if APIManager.shared.isConnectedToNetwork() == false {
            alertBoxOk(message:"Validation.InternetNotWorking".localizedString , title: "AppName".localizedString, ok: {
            })
            return
        }
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        let addUserCardAPI =  BookServiceEndPoint.getUserCardList
        addUserCardAPI.request(isLoaderNeeded: true, header: ["language_id" : LanguageFile.shared.getLanguage(), "access_token" :  token]) { [weak self] (response) in
            
            switch response {
                
            case .success(let response):
                self?.viewBgCardDetails.isHidden = true
                guard let cards = response as? [CardsModel] else {
                    return
                }
                if cards.count > 0 {
                    cards[0].isSelected = true
                    self?.tblView.isHidden = false
                    self?.list = cards
                    self?.tableDataSource?.items = cards
                    self?.tblView.reloadData()
                    self?.btnPayment.isHidden = false
                    
                }else {
                    self?.btnPayment.isHidden = true
                }
                
                
            case .failure(let strError):
                self?.tblView.isHidden = true
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
                
            }
        }
        
    }
    
    func generateOrder(){
        
        //        var agentId = [Int]()
        //        if let value = orderSummary?.agentId?.toInt() {
        //            agentId.append(value)
        //        }
        //
        //        let timeInter = Date().timeIntervalSince(orderSummary?.currentDate ?? Date())
        //        let objR = API.GenerateOrder(FormatAPIParameters.GenerateOrder(
        //            promoCode: orderSummary?.promoCode,
        //            cartId: orderSummary?.cartId,
        //            isPackage: orderSummary?.isPackage,
        //            paymentType:selectedPaymentMethod.rawValue,
        //            agentIds: agentId,
        //            deliveryDate: orderSummary?.deliveryDate?.add(seconds: Int(timeInter)),
        //            duration: /orderSummary?.duration, card_id: "\(/getDta?.id!)").formatParameters())
        //
        //        APIManager.sharedInstance.opertationWithRequest(withApi: objR) {
        //            [weak self](response) in
        //            weak var weakSelf = self
        //            switch response {
        //            case .Success(let object):
        //                weakSelf?.handleGenerateOrder(orderId: object)
        //            case .Failure(_):
        //                break
        //            }
        //        }
    }
    func handleGenerateOrder(orderId : Any?){
        //        self.orderSummary?.paymentMethod = self.selectedPaymentMethod
        //        //print(orderId)
        //        AdjustEvent.Order.sendEvent(revenue: orderSummary?.totalAmount)
        //        if !(/self.orderSummary?.isBuyOnly) {
        //            DBManager.sharedManager.cleanCart()
        //        }
        //        UtilityFunctions.showSweetAlert(title: L10n.OrderPlacedSuccessfully.string, message: L10n.YourOrderHaveBeenPlacedSuccessfully.string, style: .Success, success: {
        //            [weak self] in
        //            guard let self = self else { return }
        //
        //            if self.orderSummary?.selectedDeliverySpeed == .scheduled {
        //
        //                let VC = StoryboardScene.Order.instantiateOrderSchedularViewController()
        //                let orderIdArr = orderId as? [Int]
        //                let ordrs = orderIdArr?.map({ (orderId) -> String in
        //                    return String(orderId)
        //                })
        //                VC.orderId = ordrs?.joined(separator: ",")
        //                VC.orderSummary = self.orderSummary
        //                VC.orderSummary?.paymentMethod = self.selectedPaymentMethod
        //                self.pushVC(VC)
        //            }
        //            else
        //            {
        //                let orderIdArr = orderId as? [Int]
        //                let ordrs = orderIdArr?.map({ (orderId) -> String in
        //                    return String(orderId)
        //                })
        //               let orders = ordrs?.joined(separator: ",")
        //                let orderDetailVc = StoryboardScene.Order.instantiateOrderDetailController()
        //                orderDetailVc.isBuyOnly = /self.orderSummary?.isBuyOnly
        //
        //                orderDetailVc.orderDetails = OrderDetails(orderSummary: self.orderSummary,orderId: orders,scheduleOrder: "")
        //
        ////                orderDetailVc.orderDetails?.paymentType = self.selectedPaymentMethod
        //                orderDetailVc.type = .OrderUpcoming
        //                orderDetailVc.isOrderCompletion = true
        //                self.pushVC(orderDetailVc)
        //            }
        //
        //            })
    }
}

extension PaymentMethodController : PayFortDelegate {
    
    func sdkResult(_ response: Any!) {
        
        let response = JSON(response)
        if response["response_message"].stringValue == "Success"{
            generateOrder()
            //  AdjustEvent.Purchase.sendEvent()
        }
        print(response)
    }
}

extension PaymentMethodController {
    
    
    func randomStringWithLength (len : Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        
        let randomString : NSMutableString = NSMutableString(capacity: len)
        
        for _ in 0...len {
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
        }
        
        return randomString as String
    }
}

//MARK: - Payfort
extension PaymentMethodController {
    func startPayfortRequest(sdkToken : String?){
        let payfortController = PayFortController(enviroment: .production)
        payfortController?.delegate = self
        var request    = [String : String]()
        request["customer_email"] = /UDSingleton.shared.userData?.userDetails?.user?.email
        request["command"] = "PURCHASE"
        request["currency"] = "TTD"
        request["merchant_reference"] = randomStringWithLength(len: 8) + "-" + /orderId
        request["sdk_token"] = sdkToken ?? ""
        request["amount"] = Int(/orderSummary?.quantity * 100).toString
        request["customer_name"] = /UDSingleton.shared.userData?.userDetails?.user?.name
        payfortController?.isShowResponsePage = true
        payfortController?.setPayFortRequest(NSMutableDictionary(dictionary: request))
        payfortController?.callPayFort(self)
        
    }
}
//MARK:- PaymentMethod
extension PaymentMethodController{
    
    func getToken(){
        //        if tfExpiryDate.text == ""{
        //
        //        }else
        if tfCardHoldersName.text == ""{
            var alert = UIAlertController(title: "Error", message: "Enter Holder name", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }else if tfExpiryDate.text == ""{
            var alert = UIAlertController(title: "Error", message: "Enter expiry date", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }else if tfCVV.text == ""{
            let alert = UIAlertController(title: "Error", message: "Enter cvv", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }else{
            let conekta = Conekta()
            
            conekta.delegate = self
            conekta.publicKey = "key_KH7QNPz32iZVDNyqSNNqybA"
            
            conekta.collectDevice()
            
            let card = ConektaCard()
            let dteBreak = tfExpiryDate.text!.components(separatedBy: "/")
            let name = tfCardHoldersName.text!
            let number = tfCardNumber.text!
            let monthExp = dteBreak[0].replacingOccurrences(of: " ", with: "")
            let yearExp = dteBreak[1].replacingOccurrences(of: " ", with: "")
            let cvc = tfCVV.text!
            
            card.setNumber(number: number, name: name, cvc: cvc, expMonth: monthExp, expYear: yearExp)
            
            let token = ConektaToken(publicKey: conekta.publicKey!)
            
            token.card = card
            
            token.create(success: { (data) -> Void in
                
                //print(data)
                print(data)
                if data["message"] != nil{
                    if data["message"] as! String == "The card number is invalid."{
                        //                    UtilityFunctions.showSweetAlert(title: "", message: "The card number is invalid.", style: .Warning, success: {
                        //
                        //                    })
                        let alert = UIAlertController(title: "", message: "The card number is invalid.", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }else{
                    self.tokenId = data["id"] as! String
                    //                if self.fromPayment{
                    //                self.confirmOrder()
                    //                }else{
                    self.addCard(conektaToken: token)
                    //                }
                    // data["id"] as! String
                    
                }
                
            }, error: { (error) -> Void in
                print(error)
            })
        }
    }
}
