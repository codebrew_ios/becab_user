//
//  CardCountVC.swift
//  Sneni
//
//  Created by Apple on 06/09/19.
//  Copyright © 2019 Taran. All rights reserved.
//

import UIKit
protocol CardAdd {
    func card(_ data : getCard?,isCard : Bool)
    func clickAddNew()
}
class CardCountVC: UIViewController {
     var delegate : CardAdd!
    var cardAddDta : getCard?
    var fromPayment : Bool = true
     @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var btnAddNewCard: ThemeButton!
    @IBOutlet weak var lblTitle: ThemeLabel!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var btnBackl: ThemeButton!
    @IBOutlet weak var btnMenu: ThemeButton!
    var tableDataSource : TableViewDataSource?{
        didSet{
            
            tblView.reloadData()
        }
    }
    var arrList : [getCard] = []
    var arrName : [String] = ["djfnjkf","kdfkdfkf"]
    override func viewDidLoad() {
        super.viewDidLoad()
        if !fromPayment{
            btnDone.isHidden = true
            lblTitle.text = "Card List"
            
        }
        btnAddNewCard.addTarget(self, action: #selector(btnAddNewCard(_:)), for: .touchUpInside)
        hitGetCard()
        configureTableView()
        btnDone.addTarget(self, action: #selector(btnDoneAct(_:)), for: .touchUpInside)
        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var lblNoCard: UILabel!
    
    func configureTableView(){
        let  configureCellBlock : ListCellConfigureBlock = { [weak self] ( cell , item , indexpath) in
            guard let self = self else { return }
            self.configureCell(withCell: cell, item: item)
            
        }
        
        let didSelectCellBlock : DidSelectedRow = {  (indexPath , cell, item) in
            self.cardAddDta = self.arrList[indexPath.row]
            
        }
        
        tableDataSource = TableViewDataSource(items: arrList, tableView: tblView, cellIdentifier: "CardTblCell", cellHeight: UITableViewAutomaticDimension)
        tableDataSource?.configureCellBlock = configureCellBlock
        tableDataSource?.aRowSelectedListener = didSelectCellBlock
        tblView.delegate = tableDataSource
        tblView.dataSource = tableDataSource
        tblView.reloadData()
        
    }
    
    func configureCell(withCell cell : Any , item : Any? ){
        
        guard let tempCell = cell as? CardTblCell else{
            return
            
        }
        
        tempCell.blockSelect = {
            [weak self] dtaCard in
            guard let self = self else { return }
        
            
            self.alertBoxOption(message: "Are you sure you want to delete", title: "Delete" , leftAction: "no".localizedString , rightAction: "yes".localizedString , ok: { [weak self] in
                
//               self?.hitDelete("\(/dtaCard?.id)")
                
            })
            
            
        }
        
       
//        tempCell.getCrd = item as? getCard
//        tempCell.subCategory = item as? SubCategory
    }
    @objc func btnDoneAct(_ sender : UIButton){
        delegate.card(cardAddDta, isCard: true)
        self.dismissVC(completion: nil)
//        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnBackAct(_ sender: ThemeButton) {
        delegate.card(cardAddDta, isCard: false)
        self.dismissVC(completion: nil)
//        self.navigationController?.popViewController(animated: true)
    }
    @objc func btnAddNewCard(_ sender : UIButton){
        delegate.clickAddNew()
        self.dismissVC(completion: nil)
    }
}
//MARK:- API HIT
extension CardCountVC{
    func hitGetCard(){
//        let objR = API.GetCardList([:])
//
//        APIManager.sharedInstance.opertationWithRequest(withApi: objR) {
//            [weak self](response) in
//            weak var weakSelf = self
//            switch response {
//            case .Success(let object):
//                print(object)
//                guard let object = object as? getCardSetting else { return }
//                self?.arrList = object as? [getCard] ?? []
//                if (self?.fromPayment)!{
//                if self?.arrList.count == 0{
//
//                    self?.btnDone.isHidden = true
//                }else{
//
//                    self?.btnDone.isHidden = false
//                }
//                }else{
//                    self?.btnDone.isHidden = true
//                }
//                if self?.arrList.count == 0{
//                    self?.lblNoCard.isHidden = false
//
//                }else{
//                    self?.lblNoCard.isHidden = true
//
//                }
//                self?.configureTableView()
////                self?.arrList = object
//            case .Failure(_):
//                break
//            }
//        }
    }
    func hitDelete(_ id : String?){
//        let objR = API.DeleteCard(["card_id":/id])
//
//        APIManager.sharedInstance.opertationWithRequest(withApi: objR) {
//            [weak self](response) in
//            weak var weakSelf = self
//            switch response {
//            case .Success(let object):
//                for (num,val) in (self?.arrList.enumerated())!{
//                    if "\(/val.id)" == /id{
//                        self?.arrList.remove(at: num)
//                    }
//                }
//                self?.configureTableView()
//            //                self?.arrList = object
//            case .Failure(_):
//                break
//            }
//        }
    }
}
