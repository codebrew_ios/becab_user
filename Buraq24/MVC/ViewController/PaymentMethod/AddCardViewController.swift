//
//  AddCardViewController.swift
//  SideDrawer
//
//  Created by Apple on 15/11/18.
//  Copyright © 2018 Codebrew Labs. All rights reserved.
//

import UIKit
import Material
import Stripe

class AddCardViewController: BaseVC {

    //MARK:- Outlets
    @IBOutlet weak var textFieldCardNumber: TextField!
    @IBOutlet weak var textFieldCardExpiry: TextField!
    @IBOutlet weak var textFieldFirstName : TextField!
    @IBOutlet weak var textFieldLastName  : TextField!
    @IBOutlet weak var textFieldCVV       : TextField!
    
    @IBOutlet weak var imageViewCardType: UIImageView!
    
    @IBOutlet weak var buttonDefault: Button!
    @IBOutlet weak var buttonSave: Button!
    
    //MARK:- Properties
    var isFromCardController : Bool?
    var isFromGenerateReport : Bool?
    var isFromHomeController : Bool?
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        initialSetUp()
    }

    //MARK:- Check Card Type
    
    func checkCardNumber(input: String) -> CardType {
        
        var type: CardType = .Unknown
        
        // detect card type
        for card in CardType.allCards {
            if (matchesRegex(regex: card.regex, text: input)) {
                type = card
                break
            }
        }
        return (type)
    }
    
    // match card
    func matchesRegex(regex: String!, text: String!) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: regex, options: [.caseInsensitive])
            let nsString = text as NSString
            let match = regex.firstMatch(in: text, options: [], range: NSMakeRange(0, nsString.length))
            return (match != nil)
        } catch {
            return false
        }
    }
    
    //MARK:- Functions
    
    func initialSetUp() {
        
        let buttonTitle = /isFromGenerateReport ? "Make Payment" : "Save"
        buttonSave.setTitle(buttonTitle, for: .normal)
    }
    
    func updateUserModal() {
//
//        let user = UserData.share.loggedInUser
//        user?.cardAdded = true
//        UserData.share.loggedInUser = user
    }
    
    func moveToNextController() {
        
//        UtilityFunctions.runThisInMainThread {
//
//            guard let vc =  R.storyboard.tabBar.cardViewController()  else {return }
//            vc.hidesBottomBarWhenPushed = true
//            self.pushVC(vc)
//
//            //remove current controller from stack
//            guard let index = self.navigationController?.viewControllers.index(of: self) else{return}
//            self.navigationController?.viewControllers.remove(at: index)
//        }
    }
    
    //MARK:- Stripe Token
    private func getToken() {
        
        startAnimateLoader()

        let month = textFieldCardExpiry.text?.components(separatedBy: "/").first
        let year = textFieldCardExpiry.text?.components(separatedBy: "/").last
        
        let cardParams      = STPCardParams()
        cardParams.number   = textFieldCardNumber.text
        cardParams.name     = /textFieldLastName.text?.isEmpty ? textFieldFirstName.text?.trimmed() : /textFieldFirstName.text?.trimmed() + " " + /textFieldLastName.text?.trimmed()
        cardParams.expMonth = UInt(/month) ?? 0
        cardParams.expYear  = UInt(/year) ?? 0
        cardParams.cvc      = textFieldCVV.text
        
        STPAPIClient.shared().createToken(withCard: cardParams) {[weak self] (token: STPToken?, error: Error?) in
            self?.stopAnimating()
            guard let token = token, error == nil else {
                
                // Present error to user...
                Alerts.shared.show(alert: "", message: /error?.localizedDescription, type: .error)
                return
            }
            debugPrint(token.tokenId)
            
            if /self?.isFromGenerateReport {
                
                ///self?.isFromHomeController ?  self?.apiSendMasterReports(cardID: nil, stripeToken: token.tokenId): self?.apiSendReports(cardID: nil, stripeToken: token.tokenId)
                
            } else {
                self?.apiAddCard(token : token.tokenId)
            }
        }
    }
    
    //MARK:- Button Selectors
    
    @IBAction func buttonDefaultClicked(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func buttonSaveClicked(_ sender: Any) {
        
        self.view.endEditing(true)
        
        let cardNumber = textFieldCardNumber.text?.replacingOccurrences(of: " " , with: "")
        let expiry     = textFieldCardExpiry.text?.replacingOccurrences(of: "/" , with: "")
        
        let validation = Validations.sharedInstance.isValidAddCardInfo(cardNumber: cardNumber, expiry : expiry, cvv: textFieldCVV.text, firstName: textFieldFirstName.text?.trimmed())
        
        switch validation {
            
        case .success:
            getToken()
            
        case .failure(let msg):
            Alerts.shared.show(alert: "", message: msg, type: .error)
        }
    }
    
    //MARK:- TextField Did Change
    
    @IBAction func textFieldDidChange(_ sender: UITextField) {
        
        var pattern : String
        
        if sender.tag == 101 {
            pattern = "**** **** **** **** ****"
            
            let type    = checkCardNumber(input: /sender.text?.replacingOccurrences(of: " " , with: ""))
            
            // Set card image
            if let image = type.cardImage {
                imageViewCardType.image = image
            } else {
                imageViewCardType.image = nil
            }
            
        } else {
            pattern = "**/**"
        }
        
        sender.text =  sender.text?.applyPatternOnNumbers(replacmentCharacter: "*",pattern: pattern)
    }
    
    //MARK:- API
    
    func apiAddCard(token : String?) {
        
        let accessToken = /UDSingleton.shared.userData?.userDetails?.accessToken
        let api = BookServiceEndPoint.addCard(stripeToken: /token, defaultStatus: buttonDefault.isSelected)
        api.request(isLoaderNeeded: true, header: ["language_id" : LanguageFile.shared.getLanguage(), "access_token" :  accessToken]) {
            [weak self] (response) in

            switch response {
                
            case .success(let value) :
                
                guard let _ = (value as? CardDataModal) else {return}
                
                Alerts.shared.show(alert: "", message: "Card added successfully", type: .success)
                
                if /self?.isFromCardController {
                    self?.popVC()
                } else {
                    self?.updateUserModal()
                    self?.moveToNextController()
                }
                
            case .failure(let error):
                debugPrint(error ?? "")
            }
        }
    }
}
