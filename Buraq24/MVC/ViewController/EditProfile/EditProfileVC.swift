//
//  EditProfileVC.swift
//  Buraq24
//
//  Created by MANINDER on 11/09/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit

class EditProfileVC: BaseVC {
    
    //MARK:- Outlets
    @IBOutlet var imgViewUser: UIImageView!
    @IBOutlet var txtFieldFullName: UITextField!
    @IBOutlet var txtPhoneNumber: UITextField!

    //MARK:- Properties
    var imageToChange : UIImage?
    
    
    
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        assignPreviousData()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
    
    //MARK:- Actions
    
    @IBAction func actionBtnEditProfile(_ sender: UIButton) {
        self.view.endEditing(true)
        let strName = /txtFieldFullName.text?.trimmed()
        if Validations.sharedInstance.validateUserName(userName: strName) {
            editProfile(strName: strName)
        }
    }
    
    @IBAction func actionBtnChangeProfilePressed(_ sender: UIButton) {
        
        self.view.endEditing(true)
        CameraImage.shared.captureImage(from: self, At: self.imgViewUser, mediaType: nil, captureOptions: [.camera, .photoLibrary], allowEditting: true) { [unowned self] (image) in
            
            guard let img = image else { return }
            self.imgViewUser.image = img
            self.imageToChange = img
            
        }
    }
    
    //MARK:- Functions
    
    private func setUpUI() {
        
        self.txtFieldFullName.setAlignment()
    }
    
    func assignPreviousData() {
        
        guard let userName = UDSingleton.shared.userData?.userDetails?.user?.name else{return}
        txtFieldFullName.text = userName
        
        if let urlImage = UDSingleton.shared.userData?.userDetails?.profilePic {
            imgViewUser.sd_setImage(with: URL(string : urlImage), placeholderImage: #imageLiteral(resourceName: "ic_user"), options: .refreshCached, progress: nil, completed: nil)
        }
        let strCountryCode = String(/UDSingleton.shared.userData?.userDetails?.user?.countryCode)
        let strPhNo = String(/UDSingleton.shared.userData?.userDetails?.user?.phoneNumber)
        txtPhoneNumber.text = strCountryCode + "-" + strPhNo
        txtPhoneNumber.isUserInteractionEnabled = false
    }
}

//MARK:- API
extension EditProfileVC {
    
    func editProfile(strName : String) {
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        
        let objEdit = LoginEndpoint.editProfile(name: strName)
        objEdit.request(isImage: true, images: [imageToChange], isLoaderNeeded: true, header: ["access_token" :  token]) {[weak self] (response) in
            switch response {
            case .success(let data):
                
                guard let updatedUser = data as? UserDetail else{return}
                
                if let userData = UDSingleton.shared.userData {
                    userData.userDetails = updatedUser
                    UDSingleton.shared.userData = userData
                    
                }
                self?.alertBoxOk(message:"profile_updated_successfully".localizedString , title: "AppName".localizedString, ok: {
                     self?.popVC()
                })
               
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
            }
        }
    }
    
}
