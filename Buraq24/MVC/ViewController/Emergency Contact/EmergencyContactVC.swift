//
//  EmergencyContactVC.swift
//  Buraq24
//
//  Created by MANINDER on 01/09/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit

class EmergencyContactVC: BaseVC {
    
    //MARK:- Outlets
    @IBOutlet var tblContacts: UITableView!
    
    //MARK:- Properties
    var tableDataSource : TableViewDataSource?
    var arrContacts : [EmergencyContact]?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getEmergencyContacts()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
    
    //MARK:- Functions
    
    
    func configureTableView() {
        let  configureCellBlock : ListCellConfigureBlock = { ( cell , item , indexpath) in
            if let cell = cell as? ContactCell , let model = item as? EmergencyContact{
                cell.assignCellData(model: model)
                cell.callBackBtn = { model in
                    if let contactNumber = model.phoneNumber {
                        contactNumber.showCallOption()
                    }
                }
            }
        }
        
        tableDataSource = TableViewDataSource(items: arrContacts, tableView: tblContacts, cellIdentifier: R.reuseIdentifier.contactCell.identifier, cellHeight: 120)
        tableDataSource?.configureCellBlock = configureCellBlock
        
        tblContacts.delegate = tableDataSource
        tblContacts.dataSource = tableDataSource
        
        tblContacts.reloadData()
    }
    
    
    //MARK:- API
    
    func getEmergencyContacts(){
        
        let token = /UDSingleton.shared.userData?.userDetails?.accessToken
        let contactsVC = LoginEndpoint.eContacts()
        contactsVC.request(header: ["access_token" :  token]) { [weak self] (response) in
            switch response {
            case .success(let data):
                
                if let contacts = data as? [EmergencyContact] {
                    self?.arrContacts = contacts
                    self?.configureTableView()
                }
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message: /strError , type: .error )
            }
        }
        
    }
    
    
}
