//
//  ReferralVC.swift
//  Buraq24
//
//  Created by MANINDER on 07/09/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit

class ReferralVC: BaseVC {

    //MARK:- Outlets
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
    
    //MARK:- Actions
    
    @IBAction func actionBtnSharePressed(_ sender: UIButton) {
        
        let textToShare = "Install Buraq24 Customer Application"
        if let appUrl = NSURL(string: APIBasePath.AppStoreURL) {
            let objectsToShare = [textToShare, appUrl] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.popoverPresentationController?.sourceView = self.view
            self.present(activityVC, animated: true, completion: nil)
        }
    }

}
