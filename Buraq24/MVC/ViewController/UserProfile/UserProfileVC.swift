//
//  UserProfileVC.swift
//  Buraq24
//
//  Created by MANINDER on 13/08/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit

class UserProfileVC: UIViewController {
    
    //MARK:- OUTLETS
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.delegate = tableDataSource
            tableView.dataSource = tableDataSource
        }
    }
    @IBOutlet weak var headerView: UIView!
    @IBOutlet var txtFieldFullName: UITextField!
    @IBOutlet var constraintBottomButton: NSLayoutConstraint!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnNext: UIButton!
    
    //MARK:- PROPERTIES
    var loginDetail : LoginDetail?
    var arrDocuments: [UIImage]?
    var tableDataSource : TableViewDataSource?{
        didSet{
            
            tableView.reloadData()
        }
    }
    
    
    //MARK:- VIEW CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        addKeyBoardObserver()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        removeKeyBoardObserver()
    }
    
    
    func configureTableView(){
        
        tableDataSource = TableViewDataSource(items: nil, tableView: tableView, cellIdentifier: "SignUpUploadDocCell", cellHeight: 100.0)
        let  configureCellBlock : ListCellConfigureBlock = { [weak self] ( cell , item , indexpath) in
            guard let signupCell = cell as? SignUpUploadDocCell else { return }
            
            signupCell.imgDoc.image = self?.arrDocuments?[indexpath.row]
            signupCell.lblDocName.text = "Identification Proof"
            
        }
        tableDataSource?.configureCellBlock = configureCellBlock
        tableView.delegate = tableDataSource
        tableView.dataSource = tableDataSource
        tableView.reloadData()
        
    }
    
    //MARK:- ACTIONS
    
    @IBAction func actionBtnBackPressed(_ sender: UIButton) {
        self.popVC(to: LandingVC.self)
    }
    
    
    @IBAction func actionBtnNextPressed(_ sender: UIButton) {
        let strName = /txtFieldFullName.text?.trimmed()
        if Validations.sharedInstance.validateSignUpData(userName: strName, imgCount: arrDocuments?.count ?? 0) {
            saveName(strName: strName)
        }
    }
    
    @IBAction func actionBtnUploaDocument(_ sender: UIButton) {
        if /self.arrDocuments?.count < 1 {
            self.view.endEditing(true)
            
            CameraImage.shared.captureImage(from: self, At: sender, mediaType: nil, captureOptions: [.camera, .photoLibrary], allowEditting: true) { [unowned self] (image) in
                
                var imgArray = [UIImage]()
                guard let img = image else { return }
                imgArray.append(img)
                
                 self.arrDocuments = imgArray
//                self.arrDocuments?.insert(img, at: 0)
                self.tableDataSource?.items = self.arrDocuments
                self.tableView.reloadData()
                
            }
        }
        
    }
    
    
    //MARK:- FUNCTIONS
    
    func setUpUI() {
        btnNext.setImage(#imageLiteral(resourceName: "ic_next_r").setLocalizedImage(), for: .normal)
        btnBack.setImage(#imageLiteral(resourceName: "Back").setLocalizedImage(), for: .normal)
        
        txtFieldFullName.setAlignment()
    }
    
    func addKeyBoardObserver() {
        
        NotificationCenter.default.addObserver(self,selector: #selector(keyboardWillShow),name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(keyboardWillhide),name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func removeKeyBoardObserver() {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            animateBottomView(true, height: keyboardHeight)
        }
    }
    
    @objc func keyboardWillhide(_ notification: Notification) {
        
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            animateBottomView(false, height: keyboardHeight)
        }
    }
    
    
    func animateBottomView(_ isToShown : Bool , height : CGFloat) {
        
        UIView.animate(withDuration: 0.2) { [weak self] in
            if isToShown {
                self?.constraintBottomButton.constant = (height + CGFloat(20))
            }else {
                self?.constraintBottomButton.constant =  CGFloat(40)
            }
            self?.view.layoutIfNeeded()
        }
    }
    
}

//MARK:- API
extension UserProfileVC {
    
    func saveName(strName : String) {
        
        self.view.endEditing(true)
        
        
        print(loginDetail?.services)
        
        let objR = LoginEndpoint.addName(name: strName)
        objR.request(isImage: true, images: arrDocuments, isLoaderNeeded: true, header: ["language_id" : LanguageFile.shared.getLanguage() , "access_token" : /loginDetail?.userDetails?.accessToken ]) { [weak self] (response) in
            switch response {
            case .success(let data):
                guard let model = data as? UserDetail else { return }
                self?.loginDetail?.userDetails = model
                self?.saveUserInfo( )
                
            case .failure(let strError):
                Alerts.shared.show(alert: "AppName".localizedString, message:/strError , type: .error )
                
                //    Toast.show(text: strError, type: .error)
            }
        }
        
        
    }
    
    
    func saveUserInfo() {
        UDSingleton.shared.userData = self.loginDetail
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.setHomeAsRootVC()
        
    }
}
