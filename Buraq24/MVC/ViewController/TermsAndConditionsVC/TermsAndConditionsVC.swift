//
//  TermsAndConditionsVC.swift
//  Buraq24
//
//  Created by MANINDER on 25/09/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit
import NVActivityIndicatorView


class TermsAndConditionsVC: BaseVC , UIWebViewDelegate  {
    
    //MARK:- Outlets
    @IBOutlet var webView: UIWebView!
    
    //MARK:- Properties
    var strWebLink : String?
    var strNavTitle : String?
    
    //MARK:- View life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadRequest()
    }
    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
    
    //MARK:- Functions
    func loadRequest() {
        if let strURL =  strWebLink , let strTitle =  strNavTitle {
            super.updateTitle(strTitle: strTitle)
            guard let url = URL(string : strURL) else{ return }
            webView.delegate = self
            webView.loadRequest(URLRequest(url: url))
        }
    }
    

//MARK:- WebView Delegates

    
    func webViewDidStartLoad(_ webView: UIWebView) {
        startAnimating( message: nil, messageFont: nil, type: .lineScalePulseOutRapid , color: UIColor.white , padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        stopAnimating()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        stopAnimating()
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return true
    }
    
}
