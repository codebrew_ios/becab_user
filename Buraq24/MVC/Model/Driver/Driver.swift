//
//  Driver.swift
//  Buraq24
//
//  Created by MANINDER on 18/08/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//


import UIKit
import ObjectMapper



class DriverList: Mappable {
    
    var drivers : [HomeDriver]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        drivers <- map["drivers"]
    }
}



class HomeDriver : Mappable {
    
    var latitude : Double?
    var longitude : Double?
    var driverServiceId : Int?
    var driverUserId : Int?
    var bearingValue : Float?
    

    
    required init?(map: Map) {
        
        
    }
    
    func mapping(map: Map) {
      latitude <- map["latitude"]
         longitude <- map["longitude"]
          driverServiceId <- map["category_id"]
        driverUserId <- map["user_detail_id"]
    bearingValue <- map["bearing"]
    }
}



class Driver: Mappable {
    var driverName : String?
    var driverCountryCode : String?
    var driverPhoneNumber : Int?
    var driverLatitude : Double?
    var driverLongitude : Double?
    var driverTimeZone : String?
    var driverProfilePic : String?
    var driverEmail : String?
    var driverAddress : String?
    var driverOrganisationId : Int?
    var driverSocketId : String?
    var driverUserId : Int?
    var driverOnlineStatus : UserOnlineStatus?
    var driverLanguageId : Int?
    var driverRatingCount : Int?
    var driverRatingAverage : String?
 
    
required init?(map: Map){
    

    }
    
    func mapping(map: Map) {
        
       driverName <- map["name"]
       driverCountryCode <- map["phone_code"]
       driverPhoneNumber <- map["phone_number"]
       driverLatitude <- map["latitude"]
       driverLongitude <- map["longitude"]
       driverTimeZone <- map["timezone"]
       driverProfilePic <- map["profile_pic_url"]
       driverEmail <- map["email"]
       driverAddress <- map["address"]
       driverOrganisationId <- map["organisation_id"]
       driverSocketId <- map["socket_id"]
       driverUserId <- map["user_id"]
        driverRatingCount <- map["rating_count"]
        driverRatingAverage <- map["rating_avg"]
        var onlStatus:String?
        onlStatus <- map["online_status"]
        driverOnlineStatus = UserOnlineStatus(rawValue: /onlStatus)
        driverLanguageId <- map["language_id"]
    }
}
