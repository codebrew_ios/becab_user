
//
//  CardModal.swift
//  SideDrawer
//
//  Created by Apple on 16/11/18.
//  Copyright © 2018 Codebrew Labs. All rights reserved.
//

import Foundation
import Foundation
import ObjectMapper

class CardModal: Mappable {
    
    var data      : [CardDataModal]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        data <- map["data"]
    }
}

class CardDataModal: Mappable {
    
    var _id       : String?
    var cardToken : String?
    var lastFourDigit   : String?
    var nameOnCard      : String?
    var stripeUserId    : String?
    var typeOfCard      : String?
    var userId          : String?
    var createdOn       : Int?
    var defaultStatus   : Int?
    var expirationMonth : Int?
    var expirationYear  : Int?
    var cardAdded       : Bool?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        _id           <- map["_id"]
        cardToken     <- map["card_id"]
        lastFourDigit <- map["last4"] //lastFourDigit
        nameOnCard    <- map["card_holder_name"]
        stripeUserId  <- map["stripeUserId"]
        typeOfCard    <- map["brand"]//typeOfCard
        userId        <- map["customer_id"]//userId
        createdOn     <- map["createdOn"]
        defaultStatus <- map["defaultStatus"]
        var monthStr = ""
        var yearStr = ""
        monthStr <- map["exp_month"]//expirationMonth
        expirationMonth = Int(monthStr)
        yearStr  <- map["exp_year"]//expirationYear
        expirationYear = Int(yearStr)
        cardAdded       <- map["cardAdded"]
    }
}
