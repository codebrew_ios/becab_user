
//
//  CardsModel.swift
//  Buraq24
//
//  Created by Apple on 04/10/19.
//  Copyright © 2019 CodeBrewLabs. All rights reserved.
//

import Foundation
import ObjectMapper

class CardsModel: Mappable {
    var last4: String?
    var created_at: String?
    var user_id: Int?
    var updated_at: String?
    var fingerprint: String?
    var exp_year: String?
    var user_card_id: Int?
    var is_default: String?
    var card_holder_name: String?
    var brand: String?
    var exp_month: String?
    var customer_id: String?
    var card_id: String?
    var deleted: String?
    var isSelected: Bool = false
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        last4 <- map["last4"]
        created_at <- map["created_at"]
        user_id <- map["user_id"]
        updated_at <- map["updated_at"]
        fingerprint <- map["fingerprint"]
        exp_year <- map["exp_year"]
        user_card_id <- map["user_card_id"]
        is_default <- map["is_default"]
        card_holder_name <- map["card_holder_name"]
        brand <- map["brand"]
        exp_month <- map["exp_month"]
        customer_id <- map["customer_id"]
        card_id <- map["card_id"]
        deleted <- map["deleted"]
        
    }
}
