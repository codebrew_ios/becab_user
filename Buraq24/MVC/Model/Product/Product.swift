//
//  Product.swift
//  Buraq24
//
//  Created by MANINDER on 18/08/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit
import ObjectMapper

class Product: Mappable {
    
    var productDescription : Int?
    var productSortOrder : Int?
    var productName : String?
    var productBrandId : Int?
    var actualPrice : Float? // Change according to backend
    var pricePerQuantity : Float?
    var pricePerWeight : Float?
    var pricePerDistance : Float?
    var alphaPrice : Float?
    var productImage : String?
    var min_quantity :Int?
    var  max_quantity :Int?
    
    var imageURL : URL?

    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        productDescription <- map["description"]
        productSortOrder <- map["sort_order"]
        productName <- map["name"]
        productBrandId <- map["category_brand_product_id"]
        actualPrice <- map["actual_value"]
        pricePerQuantity <- map["price_per_quantity"]
        pricePerWeight <- map["price_per_weight"]
        pricePerDistance <- map["price_per_distance"]
         alphaPrice <- map["alpha_price"]
        min_quantity <- map["min_quantity"]
        max_quantity <- map["max_quantity"]
        productImage <- map["image_url"]
        
        guard let imgeURL = productImage else{return}
        imageURL = URL(string: imgeURL)
        
    }
    
}
