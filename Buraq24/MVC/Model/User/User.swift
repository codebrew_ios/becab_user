//
//  User.swift
//  Buraq24
//
//  Created by MANINDER on 17/08/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit
import ObjectMapper


enum NotificationStatus : String  {
    case On = "1"
    case Off = "0"
}



class UserDetail: Mappable {

    var user : User?
    var profilePic : String?
    var userType : UserType?
    var mulkiyaValidity : String?
    var mulkiyaNumber : String?
    var fcmId : String?
    var mulkiyaBack : String?
    var userDetailId : Int?
    var socketId : String?
    var latitude : Float?
    var longitude : Float?
    var organisationId : Int?
    var categoryBrandId : Int?
    var categoryId : Int?
    var timezoneDifference : String?
    var timezoneName : String?
    var accessToken : String?
    var onlineStatus : UserOnlineStatus?
    var lanuageId : Int?
    var userId : Int?
    var maximumRides : Int?
    var mulkiyaFront : String?
    var myRatingCount : Int?
     var myRatingAverage: Int?
   
    var notificationStatus : NotificationStatus  = .Off
    
   
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        user <- map["user"]
        profilePic <- map["profile_pic_url"]
        var userValue:Int?
        userValue <- map["user_type_id"]
        userType = UserType(rawValue: /userValue)
        mulkiyaNumber <- map["mulkiya_number"]
        fcmId <- map["fcm_id"]
        mulkiyaBack <- map["mulkiya_back"]
        userDetailId <- map["user_detail_id"]
        socketId <- map["socket_id"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        organisationId <- map["organisation_id"]
        categoryBrandId <- map["category_brand_id"]
        categoryId <- map["category_id"]
        timezoneDifference <- map["timezonez"]
        timezoneName <- map["timezone"]
        accessToken <- map["access_token"]
        lanuageId <- map["language_id"]
        userId <- map["user_id"]
        maximumRides <- map["maximum_rides"]
        mulkiyaFront <- map["mulkiya_front"]
        myRatingCount <- map["rating_count"]
        myRatingAverage <- map["ratings_avg"]
        
       notificationStatus <- (map["notifications"],EnumTransform<NotificationStatus>())
        onlineStatus <- (map["online_status"],EnumTransform<UserOnlineStatus>())


    }
}


class User: Mappable {
    
    var name : String?
    var email : String?
    var stripeConnectToken : String?
    var stripeCustomerId : String?
    var phoneNumber : Int?
    var countryCode : String?
    var userId : Int?
    var organisationId : Int?
    var stripeConnectId : String?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        email <- map["email"]
        stripeConnectToken <- map["stripe_connect_token"]
        stripeCustomerId <- map["stripe_customer_id"]
        countryCode <- map["phone_code"]
        phoneNumber <- map["phone_number"]
        userId <- map["user_id"]
        organisationId <- map["organisation_id"]
        stripeConnectId <- map["stripe_connect_id"]
    }
}
