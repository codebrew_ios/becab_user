//
//  Home.swift
//  Buraq24
//
//  Created by MANINDER on 18/08/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit
import ObjectMapper

class Home: Mappable {
    
    var userDetail : UserDetail?
    var drivers : [Driver]?
    var orders: [Order] = [Order]()
    var orderLastCompleted: [Order] = [Order]()
    var brands : [Brand]?
    var support : [Support]?
    var services : [Service]?
     var version : Version?
    
     required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        userDetail <- map["AppDetail"]
        drivers <- map["drivers"]
        orders <- map["currentOrders"]
         orderLastCompleted <- map["lastCompletedOrders"]
        brands <- map["categories"] // brands with products
     
        version <- map["Versioning"]
        support <- map["supports"]
         services <- map["services"]
        
    }
}

