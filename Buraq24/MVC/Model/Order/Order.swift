//
//  Order.swift
//  Buraq24
//
//  Created by MANINDER on 18/08/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit
import ObjectMapper


enum OrderTurn : String {
    case MyTurn = "1"
    case OtherCustomerTurn = "0"
    
}


class Rating : Mappable {
    
    
    var comment  : String?
    var ratingGiven : Int?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        comment <- map["comments"]
        ratingGiven <- map["ratings"]
    }
}

class OrderDates : Mappable{
    
    
    var startedDate : Date?
    var completedDate : Date?
    

    required init?(map: Map) {

    }

    func mapping(map: Map) {
        var strAccepted : String?
        strAccepted <- map["accepted_at"]
        
        var strCompleted : String?
        strCompleted <- map["updated_at"]
        guard let utcAccDateStr = strAccepted else{return}
        startedDate = utcAccDateStr.getLocalDate()
        guard let utcCompletedDateStr = strCompleted else{return}
        completedDate = utcCompletedDateStr.getLocalDate()
    }
}

class Order: Mappable {
    
    var orderId : Int?
    var customerOrganisationId : Int?
    var refundStatus : String?
    var initialCharge : String?
    var continuousOrderId : Int?
    
    
    var bottleCharge : String?
    var bottleReturned : String?
    var userType : UserType = .Customer
    var bottleReturnedValue : Int?
    // var paymentStatus : PaymentStatus = .Pending
    
    var serviceId: Int?
    var serviceBrandId : Int?
    var categoryBrandProductId : Int?
    
    var userCardId : Int?
    var bookingType : BookingType = .Present
    var paymentType : PaymentType = .Cash
    var orderStatus : OrderStatus = .Searching
    
    var pickUpLongitude : Double?
    var pickUpLatitude : Double?
    var pickUpAddress : String?
    
    var dropOffAddress  : String?
    var dropOffLatitude : Double?
    var dropOffLongitude : Double?
    
    
    var orderToken : String?
    var orderTimings : String?
    var orderLocalDate : Date?
    var orderDate : Date?
    var cancelReason : String?
    
    var finalCharge : String?
    var productQuantity :  Int?
    var orderProductDetail : OrderProductDetail?
    var driverAssigned : Driver?
    var payment : Payment?
    var rating : Rating?
    
    var orderDates : OrderDates?
    
    var myTurn : OrderTurn = .MyTurn
    var isDirectSearching = false
    
    var organisationCouponUserId :Int?
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        orderId <- map["order_id"]
        
        customerOrganisationId <- map["customer_organisation_id"]
        refundStatus <- map["refund_status"]
        initialCharge <- map["initial_charge"]
        continuousOrderId <- map["continuous_order_id"]
        serviceId <- map["category_id"]
        bottleCharge <- map["bottle_charge"]
        bottleReturned <- map["bottle_returned"]
        bottleReturnedValue <- map["bottle_returned_value"]
        productQuantity <- map["product_quantity"]
        cancelReason <- map["cancel_reason"]
        orderTimings <- map["order_timings"]
        
        guard let utcDateStr = orderTimings else{return}
        
        orderLocalDate = utcDateStr.getLocalDate()
        
        finalCharge <- map["final_charge"]
        orderToken <- map["order_token"]
        userCardId <- map["user_card_id"]
        
        organisationCouponUserId <- map["organisation_coupon_user_id"]

        
        pickUpLongitude <- map["pickup_longitude"]
        pickUpLatitude <- map["pickup_latitude"]
        pickUpAddress <- map["pickup_address"]
        dropOffAddress  <- map["dropoff_address"]
        dropOffLatitude <- map["dropoff_latitude"]
        dropOffLongitude <- map["dropoff_longitude"]
        orderProductDetail <- map["brand"]
        driverAssigned <- map["driver"]
        payment <- map["payment"]
         rating <- map["ratingByUser"]
        
        var orderstatus : String?
        orderstatus <- map["order_status"]
        
        guard let status = orderstatus else{return}
        orderStatus = OrderStatus(rawValue: /status)!
        
        var orderTurn : String?
        orderTurn  <- map["my_turn"]
        
        guard let turn = orderTurn else{return}
        myTurn = OrderTurn(rawValue: /turn)!
        
        orderDates  <- map["cRequest"]
        
        
        var strFuture : String?
        strFuture <- map["future"]
         guard let future = strFuture else{return}
        bookingType = BookingType(rawValue: /future)!

    }
    
    
}


class OrderProductDetail : Mappable {
    
    var productName : String?
    var productDescription : String?
    var pricePerWeight : Float?
    var productCategoryId : Int?
    var pricePerDistance : Float?
    var productBrandId : Int?
    var productPricePerQuantity : Float?
    var productActualPrice : Float?
    var productAlphaPrice : Float?
    var productId : Int?
    var productBrandName : String?
    var category_name :String?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        productName <- map["name"]
        productDescription <- map["description"]
        pricePerWeight <- map["price_per_weight"]
        productCategoryId <- map["category_id"]
        pricePerDistance <- map["price_per_distance"]
        productBrandId <- map["category_brand_id"]
        productPricePerQuantity <- map["price_per_quantity"]
        productActualPrice <- map["actual_value"]
        productAlphaPrice <- map["alpha_price"]
        productId <- map["category_brand_product_id"]
        productBrandName <- map["brand_name"]
        category_name <- map["category_name"]
        
    }
}

