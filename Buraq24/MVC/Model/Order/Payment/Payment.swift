//
//  Payment.swift
//  Buraq24
//
//  Created by MANINDER on 31/08/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit
import ObjectMapper

class Payment: Mappable {
    
    
    var paymentType : PaymentType = .Cash
    var productWeight : String?
    var productPerQuantityCharge : String?
    var refundStatus : String?
    var initalCharge : String?
    var adminCharge : String?
    var bottleCharge : String?
    var refundId : String?
    var transactionId: String?
    var paymentId : Int?
    var productActualValue : String?
    var orderDistance : String?
    var productPerWeightCharge :String?
    var bottleReturnedValue : Int?
    var finalCharge : String?
    var bankCharge : String?
    var productPerDistanceCharge :String?
    var productStatus : String?
    var productQuantity : Int?
    var productAplhaCharge : String?
    var buraqPercentage : Float?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        var strType : String?
        strType <- map["payment_type"]
        //guard let type = strType else {return}
        
        
       // paymentType = PaymentType(rawValue: type)!
        productWeight <- map["product_weight"]
        productPerQuantityCharge <- map["product_per_quantity_charge"]
        refundStatus <- map["refund_status"]
        initalCharge <- map["initial_charge"]
        adminCharge <- map["admin_charge"]
        bottleCharge <- map["bottle_charge"]
        refundId <- map["refund_id"]
        transactionId <- map["transaction_id"]
        paymentId <- map["payment_id"]
        productActualValue <- map["product_actual_value"]
        orderDistance <- map["order_distance"]
        productPerWeightCharge <- map["product_per_weight_charge"]
        bottleReturnedValue <- map["bottle_returned_value"]
        finalCharge <- map["final_charge"]
        bankCharge <- map["bank_charge"]
        productPerDistanceCharge <- map["product_per_distance_charge"]
        productStatus <- map["payment_status"]
        productQuantity <- map["product_quantity"]
        productAplhaCharge <- map["product_alpha_charge"]
        buraqPercentage <- map["buraq_percentage"]
        
    }
}
