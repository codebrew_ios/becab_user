//
//  EmergencyContact.swift
//  Buraq24
//
//  Created by MANINDER on 07/09/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit
import ObjectMapper

class EmergencyContact: Mappable {
    
    var phoneNumber : Int?
    var name : String?
    var contactId : Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        phoneNumber <- map["phone_number"]
        name <- map["name"]
        contactId <- map["emergency_contact_id"]
    }
}
