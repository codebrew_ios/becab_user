//
//  WaterCompaniesListing.swift
//  Buraq24
//
//  Created by Apple on 26/11/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit
import ObjectMapper


class companies: Mappable{

    var name : String?
    var phone_code  : String?
    var phone_number : String?
    var organisation_id : Int?
    var image_url:String?
    var image :String?
    var monday_service:String?
    var tuesday_service:String?
    var wednesday_service:String?
    var thursday_service:String?
    var friday_service:String?
    var saturday_service:String?
    var sunday_service:String?

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        name <- map["name"]
        phone_code <- map["phone_code"]
        phone_number <- map["phone_number"]
        organisation_id <- map["organisation_id"]
        image_url <- map["image_url"]
        image <- map["image"]
        
        monday_service <- map["monday_service"]
        tuesday_service <- map["tuesday_service"]
        wednesday_service <- map["wednesday_service"]
        thursday_service <- map["thursday_service"]
        friday_service <- map["friday_service"]
        saturday_service <- map["saturday_service"]
        sunday_service <- map["sunday_service"]
        
    }
}


class getCard : NSObject, Mappable {
    
    
    
    var id: Int?
    var user_id: Int?
    var customer_id : String?
    var card_type : String?
    var brand_name : String?
    var holder_name : String?
    var card_id: String?
    var card_number: String?
    var expiration_year: String?
    var expiration_month: String?
    var bank_name: String?
    var bank_code: String?
    var creation_date: String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        user_id <- map["user_id"]
        customer_id <- map["customer_id"]
        card_type <- map["card_type"]
        holder_name <- map["holder_name"]
        card_id <- map["card_id"]
        card_number <- map["card_number"]
        card_number <- map["expiration_year"]
        expiration_year <- map["expiration_year"]
        bank_name <- map["bank_name"]
        bank_code <- map["bank_code"]
        expiration_month <- map["expiration_month"]
        
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(id, forKey: "id")
        aCoder.encode(user_id , forKey: "user_id")
        aCoder.encode(customer_id, forKey: "customer_id")
        aCoder.encode(card_type, forKey: "card_type")
        aCoder.encode(holder_name, forKey: "holder_name")
        aCoder.encode(card_id, forKey: "card_id")
        aCoder.encode(card_number, forKey: "card_number")
        aCoder.encode(expiration_year, forKey: "expiration_year")
        aCoder.encode(bank_name, forKey: "bank_name")
        aCoder.encode(bank_code, forKey: "bank_code")
        aCoder.encode(creation_date, forKey: "creation_date")
        aCoder.encode(expiration_month, forKey: "expiration_month")
        
        
    }
    
    
    
    required init(coder aDecoder: NSCoder) {
        
        id = aDecoder.decodeObject(forKey: "id") as? Int
        user_id = aDecoder.decodeObject(forKey: "user_id") as? Int
        customer_id = aDecoder.decodeObject(forKey: "customer_id") as? String
        card_type = aDecoder.decodeObject(forKey: "card_type") as? String
        holder_name = aDecoder.decodeObject(forKey: "holder_name") as? String
        card_id = aDecoder.decodeObject(forKey: "card_id") as? String
        card_number = aDecoder.decodeObject(forKey: "card_number") as? String
        card_number = aDecoder.decodeObject(forKey: "card_number") as? String
        expiration_year = aDecoder.decodeObject(forKey: "expiration_year") as? String
        bank_name = aDecoder.decodeObject(forKey: "bank_name") as? String
        bank_code = aDecoder.decodeObject(forKey: "bank_code") as? String
        expiration_month = aDecoder.decodeObject(forKey: "expiration_month") as? String
        
    }
}
