//
//  FreightView.swift
//  Buraq24
//
//  Created by MANINDER on 17/09/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit

class FreightView: UIView,UITextFieldDelegate {
    
    //MARK:- Outlets
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var stackMoreDetails: UIStackView!
    @IBOutlet var btnAddMore: UIButton!

    @IBOutlet var txtFieldMaterialType: UITextField!

    @IBOutlet var txtDetailName: UITextField!
    @IBOutlet var txtDetailPhone: UITextField!
    @IBOutlet var txtDetailInvoice: UITextField!
    @IBOutlet var txtDropPersonName: UITextField!

    @IBOutlet var txtFieldWeight: UITextField!
    
    @IBOutlet var txtViewInformation: PlaceholderTextView!
    
    @IBOutlet var collectionViewImages: UICollectionView!
    

    //MARK:- Properties
    
    var collViewDataSource : CollectionViewDataSource?
    
   lazy  var images : [UIImage] = [UIImage]()
    var request : ServiceRequest?
    var delegate : BookRequestDelegate?
    
    var heightPopUp : CGFloat = 200
    var isAdded : Bool = false
    var viewSuper : UIView?
    
    var openMoreDetail : Bool = false {
        didSet {
            btnAddMore.isHidden = true
            btnAddMore.isSelected = openMoreDetail
            stackMoreDetails.isHidden = !openMoreDetail
        }
    }
    
    //MARK:- Action
    @IBAction func actionAddMorePressed(_ sender: UIButton) {
        openMoreDetail = !openMoreDetail
        self.layoutIfNeeded()
        
        maximizeFreightOrderView()
    }
    
    @IBAction func actionSchedulePressed(_ sender: UIButton) {
        
        request?.requestType = .Future
        moveToNextPopUp()
    }
    
    @IBAction func actionBtnBookNowPressed(_ sender: UIButton) {
        
        request?.requestType = .Present
        request?.orderDateTime = Date()
        moveToNextPopUp()
        
    }
    
    @IBAction func actionBtnAddMoreImage(_ sender: UIButton) {
        
        if images.count < 2 {
           
            guard let topController = ez.topMostVC else{return}
            CameraImage.shared.captureImage(from: topController, At: collectionViewImages , mediaType: nil, captureOptions: [.camera, .photoLibrary], allowEditting: true) { [unowned self] (image) in
                guard let img = image else { return }
                ez.runThisInMainThread {
                      self.images.append(img)
                    self.collViewDataSource?.items = self.images
                    self.collectionViewImages.reloadData()
                }
            }
            
        }else{
            Alerts.shared.show(alert: "AppName".localizedString, message: "max_images_validation_msg".localizedString , type: .error )

            
        }
    }
    
    
    private func moveToNextPopUp() {
        
        guard var request = request else{return}
        
        request.materialType = /txtFieldMaterialType.text?.trimmed()
        request.additionalInfo = /txtViewInformation.text?.trimmed()
        
        
        if openMoreDetail {
            request.pickupPersonName = /txtDetailName.text?.trimmed()
            request.pickupPersonPhone = /txtDetailPhone.text?.trimmed()
            request.invoiceNumber = /txtDetailInvoice.text?.trimmed()
            request.deliveryPersonName = /txtDropPersonName.text?.trimmed()
        } else {
            request.pickupPersonName = ""
            request.pickupPersonPhone = ""
            request.invoiceNumber = ""
            request.deliveryPersonName = ""
        }
        
        if let weight = txtFieldWeight.text?.trimmed() {
            request.weight = Double(weight)
        }
        
        if  images.count > 0  {
            request.orderImages = images
        }
        
        delegate?.didGetRequestDetails(request: request)
        minimizeFreightOrderView()
    }
    
    //MARK:- Functions
    
    func minimizeFreightOrderView() {
        
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            
            let height = /[/self?.heightPopUp, /self?.scrollView.contentSize.height].min()
            self?.frame = CGRect(x: BookingPopUpFrames.XPopUp , y: /self?.viewSuper?.frame.origin.y + /self?.viewSuper?.frame.size.height , width: BookingPopUpFrames.WidthPopUp, height: height)
            
            }, completion: { (done) in
        })
    }
    
    func maximizeFreightOrderView() {
        
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            
            let height = /[/self?.heightPopUp, /self?.scrollView.contentSize.height].min()
            self?.frame = CGRect(x: BookingPopUpFrames.XPopUp, y: (/self?.viewSuper?.frame.origin.y + /self?.viewSuper?.frame.size.height) - CGFloat(BookingPopUpFrames.PaddingX) - /height , width: BookingPopUpFrames.WidthPopUp, height: height)
            
            }, completion: { (done) in
        })
    }
    
    func showFreightOrderView(supView : UIView , moveType : MoveType , requestPara : ServiceRequest) {
        
        txtDetailName.text = ""
        txtDetailPhone.text = ""
        txtDetailInvoice.text = ""
        txtDropPersonName.text = ""
        
        request = requestPara
        heightPopUp = supView.bounds.size.height*0.75//supView.bounds.size.width*120/100
        viewSuper = supView
        
        openMoreDetail = requestPara.serviceSelected?.serviceCategoryId == 4
        self.layoutIfNeeded()

        if !isAdded {
            
            txtFieldWeight.setAlignment()
            txtViewInformation.setAlignment()
            txtFieldMaterialType.setAlignment()
            txtViewInformation.placeholderColor = UIColor.placeHolderGray
            self.frame = CGRect(x: BookingPopUpFrames.XPopUp, y: (/viewSuper?.frame.origin.y + /viewSuper?.frame.size.height) , width: BookingPopUpFrames.WidthPopUp, height: heightPopUp)
            viewSuper?.addSubview(self)

            viewSuper?.layoutIfNeeded()
            self.frame = CGRect(x: BookingPopUpFrames.XPopUp, y: (/viewSuper?.frame.origin.y + /viewSuper?.frame.size.height) , width: BookingPopUpFrames.WidthPopUp, height: /[heightPopUp, scrollView.contentSize.height].min())
            
             configureImagesCollectionView()
            isAdded = true
        }

        if moveType == .Forward {
            
             self.txtViewInformation.placeholder = "enter_additional_information".localizedString as NSString
            self.request?.orderImages.removeAll()
            
            
            request?.materialType = nil
             request?.additionalInfo = nil
            request?.weight = nil
            
            txtFieldWeight.text = ""
            txtViewInformation.text = ""
            txtFieldMaterialType.text = ""
            images.removeAll()
            collViewDataSource?.items = images
            collectionViewImages.reloadData()
        }
       
        maximizeFreightOrderView()
    }
    
    private func configureImagesCollectionView() {
        
        
         self.txtFieldWeight.delegate = self
        
        let configureCellBlock : ListCellConfigureBlock = { [weak self] (cell, item, indexPath) in
            
            if let cell = cell as? AddImageCell, let image = item as? UIImage {
                cell.assignCellData(image: image)
                cell.callBackDeletion = { (cell : AddImageCell )   in
                    
                    if let indexCell = self?.collectionViewImages.indexPath(for: cell) {
                        
                        ez.runThisInMainThread {
                             self?.images.remove(at: indexCell.row)
                            self?.collViewDataSource?.items = self?.images
                            self?.collectionViewImages.reloadData()
                        }
                    }
                }
            }
        }
        
        
        let widthColl = collectionViewImages.frame.size.height
        
        collViewDataSource = CollectionViewDataSource(items:  images , collectionView: collectionViewImages, cellIdentifier: R.reuseIdentifier.addImageCell.identifier, cellHeight:  widthColl, cellWidth: widthColl , configureCellBlock: configureCellBlock, aRowSelectedListener: nil)
        collectionViewImages.delegate = collViewDataSource
        collectionViewImages.dataSource = collViewDataSource
        collectionViewImages.reloadData()
    }
    
    
    
    //MARK:- TextView Delegates
    //MARK:-
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string.isEmpty { return true }
        let currentText = textField.text ?? ""
        let replacementText = (currentText as NSString).replacingCharacters(in: range, with: string)
        
        return replacementText.isValidDouble(maxDecimalPlaces: 2)
    }
}




