//
//  SelectBrandView.swift
//  Buraq24
//
//  Created by MANINDER on 04/10/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit

class SelectBrandView: UIView {

    
    //MARK:- Outlets
    
    @IBOutlet var collectionViewBrands: UICollectionView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet var constraintWidthCollection: NSLayoutConstraint!
    
    //MARK:- Properties
    
    lazy var brands : [Brand] = [Brand]()
    var collectionViewDataSource : CollectionViewDataSource?
   
    
    var request : ServiceRequest?
    var delegate : BookRequestDelegate?
    
    var heightPopUp : CGFloat = 250
    var isAdded : Bool = false
     var viewSuper : UIView?
    
    
    //MARK:- Actions
    
    @IBAction func actionBtnCancelPressed(_ sender: UIButton) {
     
        self.delegate?.didSelectNext(type: .BackFromFreightProduct)
         minimizeSelectBrandView()
    }
    
    @IBAction func actionBtnNextPressed(_ sender: UIButton) {
        
        guard let brand =  self.request?.selectedBrand else{return}
        self.delegate?.didSelectedFreightBrand(brand: brand)
         minimizeSelectBrandView()
    }
    
    //MARK:- Functions
    
    func minimizeSelectBrandView() {
        
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.frame = CGRect(x: BookingPopUpFrames.XPopUp , y: /self?.viewSuper?.frame.origin.y + /self?.viewSuper?.frame.size.height , width: BookingPopUpFrames.WidthPopUp, height: /self?.heightPopUp)
            }, completion: { (done) in
        })
    }
    
    func maximizeSelectBrandView() {
        
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.frame = CGRect(x: BookingPopUpFrames.XPopUp, y: (/self?.viewSuper?.frame.origin.y + /self?.viewSuper?.frame.size.height) - CGFloat(BookingPopUpFrames.PaddingX) - /self?.heightPopUp , width: BookingPopUpFrames.WidthPopUp, height: /self?.heightPopUp)
            }, completion: { (done) in
        })
    }

    
    func showBrandView(superView : UIView , moveType : MoveType , requestPara : ServiceRequest, categoryId: Int) {
        
        request = requestPara
        guard var brandList = requestPara.serviceSelected?.brands else{return}
        
        switch categoryId {
//        case 12:
//            lblTitle.text = "Select Moto type"
        case 7:
            lblTitle.text = "Select Vehicle type"
//        case 4:
//            lblTitle.text = "Select Parcel type"
        default: break
        }
        
//        brandList = brandList.filter {   if let intVal = ($0.products?.count)  {
//            return intVal > 0
//            }
//            return false
//        }

        brands.removeAll()
        brands.append(contentsOf: brandList)
        
        self.frame = CGRect(x: BookingPopUpFrames.XPopUp, y: (superView.frame.origin.y + superView.frame.size.height) , width: BookingPopUpFrames.WidthPopUp, height: heightPopUp)
     
        if !isAdded {
            viewSuper = superView
            superView.addSubview(self)
          configureCollectionView()
            isAdded = true
        }
        
        if moveType == .Forward && brands.count > 0 {
            
            request?.selectedBrand = brands[0]
            self.collectionViewDataSource?.items = brands
            ez.runThisAfterDelay(seconds: 0.1, after: { [weak self] in
                self?.collectionViewBrands.reloadData()
            })
        }
        let count = CGFloat(brands.count)
        constraintWidthCollection.constant = getCollectionCellWidth(count: count) * count
        
        maximizeSelectBrandView()
    }
    
    private func getCollectionCellWidth(count: CGFloat) -> CGFloat {
        let count = 4//brands.count
//        if count > 4 {
//            count = 4
//        } else if count < 3 {
//            count = 3
//        }
        let widthCollection = BookingPopUpFrames.WidthPopUp-32.0
        return widthCollection/CGFloat(count)
    }
    
    private func configureCollectionView() {
        
//        lblTitle.text = "Select Type"
        
//        if let _ = brands.first(where: {/$0.categoryBrandId == 8 }) {
//            lblTitle.text = "Select parcel type"
//        } else if let _ = brands.first(where: {/$0.categoryBrandId == 20}) {
//            lblTitle.text = "Select carro type"
//        } else {
//            lblTitle.text = "Select moto type"
//        }
        
        let configureCellBlock : ListCellConfigureBlock = {
            [weak self] (cell, item, indexPath) in
            
            if let cell = cell as? SelectBrandCell, let model = item as? Brand {
                
                guard let brand = self?.request?.selectedBrand else {return}
                
                cell.markAsSelected(selected: model.categoryBrandId == brand.categoryBrandId, model: model)
            }
        }
        
        let didSelectBlock : DidSelectedRow = {
            [weak self] (indexPath, cell, item) in
            
            if let _ = cell as? SelectBrandCell, let model = item as? Brand {
                self?.request?.selectedBrand = model
                
                self?.collectionViewBrands.reloadData()
            }
        }
        
        let widthColl = getCollectionCellWidth(count: CGFloat(brands.count))
//        let widthColl = collectionViewBrands.frame.size.width/3
        let heightColl = collectionViewBrands.frame.height///2.5
        
        collectionViewDataSource = CollectionViewDataSource(
            items: brands,
            collectionView: collectionViewBrands,
            cellIdentifier: R.reuseIdentifier.selectBrandCell.identifier,
            cellHeight: heightColl,
            cellWidth: widthColl,
            configureCellBlock: configureCellBlock,
            aRowSelectedListener: didSelectBlock)
        
        collectionViewBrands.delegate = collectionViewDataSource
        collectionViewBrands.dataSource = collectionViewDataSource
        collectionViewBrands.reloadData()
        
    }
  
}
