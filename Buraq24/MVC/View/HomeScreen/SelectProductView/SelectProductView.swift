//
//  SelectProductView.swift
//  Buraq24
//
//  Created by MANINDER on 05/10/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit

class SelectProductView: UIView {
    
    //MARK:- Outlets
    
    @IBOutlet var collectionViewProduct: UICollectionView!
    @IBOutlet var constraintWidthCollection: NSLayoutConstraint!
    
    //MARK:- Properties
    
    lazy var products : [Product] = [Product]()
    var collectionViewDataSource : CollectionViewDataSource?
    
    
    var request : ServiceRequest?
    var delegate : BookRequestDelegate?
    
    var heightPopUp : CGFloat = 250
    var isAdded : Bool = false
    var viewSuper : UIView?
    
    
    //MARK:- Actions
    
    @IBAction func actionBtnCancelPressed(_ sender: UIButton) {
        
        self.delegate?.didSelectNext(type: .BackFromFreightProduct)
        minimizeProductView()
    }
    
    
    
    @IBAction func actionBtnNextPressed(_ sender: UIButton) {
        
        guard let product =  self.request?.selectedProduct else{return}
        self.delegate?.didSelectedFreightProduct(product: product)
        minimizeProductView()
        
    }
    
    //MARK:- Functions
    
    func minimizeProductView() {
        
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.frame = CGRect(x: BookingPopUpFrames.XPopUp , y: /self?.viewSuper?.frame.origin.y + /self?.viewSuper?.frame.size.height , width: BookingPopUpFrames.WidthPopUp, height: /self?.heightPopUp)
    
            }, completion: { (done) in
        })
    }
    
    func maximizeProductView() {
        
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.frame = CGRect(x: BookingPopUpFrames.XPopUp, y: (/self?.viewSuper?.frame.origin.y + /self?.viewSuper?.frame.size.height) - CGFloat(BookingPopUpFrames.PaddingX) - /self?.heightPopUp , width: BookingPopUpFrames.WidthPopUp, height: /self?.heightPopUp)
            }, completion: { (done) in
        })
    }
    
    func showProductView(superView : UIView , moveType : MoveType , requestPara : ServiceRequest) {
          viewSuper = superView
        request = requestPara
        guard let productList = requestPara.selectedBrand?.products else{return}
        
        products.removeAll()
        products.append(contentsOf: productList)
        
        self.frame = CGRect(x: BookingPopUpFrames.XPopUp, y: (superView.frame.origin.y + superView.frame.size.height) , width: BookingPopUpFrames.WidthPopUp, height: heightPopUp)
        
        if !isAdded {
            superView.addSubview(self)
            if productList.count > 0 {
                configureCollectionView()
            }
            isAdded = true
        }
        
        if moveType == .Forward && productList.count > 0 {
            
            request?.selectedProduct = productList[0]
            self.collectionViewDataSource?.items = products
            ez.runThisAfterDelay(seconds: 0.1, after: { [weak self] in
                self?.collectionViewProduct.reloadData()
            })
        }
        let count = CGFloat(products.count)

        constraintWidthCollection.constant = getCollectionCellWidth() * count

        maximizeProductView()
    }
    
    private func getCollectionCellWidth() -> CGFloat {
        let count = 4
        let widthCollection = BookingPopUpFrames.WidthPopUp-32.0
        return widthCollection/CGFloat(count)
    }
    
    private func configureCollectionView() {
        
        let configureCellBlock : ListCellConfigureBlock = {
            [weak self] (cell, item, indexPath) in
            
            if let cell = cell as? SelectBrandCell, let model = item as? Product {
                
                guard let product = self?.request?.selectedProduct else {return}
                cell.markProductSelected(selected: model.productBrandId == product.productBrandId, model: model)
            }
            
        }
        
        let didSelectBlock : DidSelectedRow = { [weak self] (indexPath, cell, item) in
            
            if let _ = cell as? SelectBrandCell, let model = item as? Product {
                self?.request?.selectedProduct = model
                self?.collectionViewProduct.reloadData()
            }
        }
        
        
        
        let widthColl = getCollectionCellWidth()
        let heightColl = collectionViewProduct.frame.height//.width/2.5
        
        collectionViewDataSource = CollectionViewDataSource(items:  products , collectionView: collectionViewProduct, cellIdentifier: R.reuseIdentifier.selectBrandCell.identifier, cellHeight:  heightColl, cellWidth: widthColl , configureCellBlock: configureCellBlock, aRowSelectedListener: didSelectBlock)
        collectionViewProduct.delegate = collectionViewDataSource
        collectionViewProduct.dataSource = collectionViewDataSource
        collectionViewProduct.reloadData()
    }
    
}


