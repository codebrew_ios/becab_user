//
//  OrderPricing.swift
//  Buraq24
//
//  Created by MANINDER on 27/08/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit

class OrderPricing: UIView {
    
    //MARK:- Outlets
    //MARK:-
    @IBOutlet var lblBrandName: UILabel!
    @IBOutlet var lblOrderDetails: UILabel!
    @IBOutlet var lblFinalPrice: UILabel!
    
    @IBOutlet weak var txtInfoTotal: UILabel!
    @IBOutlet var btnBookService: UIButton!
    
    @IBOutlet var imgViewPaymentType: UIImageView!
    @IBOutlet var imgViewPromoCodeType: UIImageView!

    @IBOutlet var lblPaymentType: UILabel!
    @IBOutlet weak var btnPaymentType: UIButton!
    @IBOutlet var lblPromoCodeType: UILabel!
    @IBOutlet weak var btnPromoCodeType: UIButton!
    
    
    var viewSuper : UIView?
    
    //MARK:- Properties
    //MARK:-
    
    var isAdded : Bool = false
    var request : ServiceRequest = ServiceRequest()
    var frameHeight : CGFloat = 240
    var delegate : BookRequestDelegate?

    
    
    //MARK:- Actions
    //MARK:-
    
    @IBAction func actionBtnBookPressed(_ sender: UIButton) {
        
//        if /request.serviceSelected?.serviceCategoryId != 1 {
//           alertBoxOk(message:"work_in_progress".localizedString , title: "AppName".localizedString, ok: {
//            })
//            return
//        }else{
            self.delegate?.didSelectNext(type: .SubmitOrder)
       // }
        
    }
    
    @IBAction func actionBtnPaymentType(_ sender: UIButton) {
        self.delegate?.didPaymentModeChanged(paymentMode: .Cash)
    }
    
    @IBAction func actionBtnPromoCodePressed(_ sender: UIButton) {

       // self.delegate?.didSelectNext(type: .Payment)

       // Alerts.shared.show(alert: "AppName".localizedString, message: "coming_soon".localizedString , type: .error )
    }
    
    //MARK:- Functions
    //MARK:-
    
    func minimizeOrderPricingView() {
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.frame = CGRect(x: BookingPopUpFrames.XPopUp, y: /self?.viewSuper?.frame.origin.y + /self?.viewSuper?.frame.size.height , width: BookingPopUpFrames.WidthPopUp, height: /self?.frameHeight)
            
            }, completion: { (done) in
        })
    }
    
    func maximizeOrderPricingView() {
        
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
           
            self?.frame = CGRect(x: BookingPopUpFrames.XPopUp, y: (/self?.viewSuper?.frame.origin.y + /self?.viewSuper?.frame.size.height) - CGFloat(BookingPopUpFrames.PaddingX) - /self?.frameHeight , width: BookingPopUpFrames.WidthPopUp, height: /self?.frameHeight)
            
            }, completion: { (done) in
                
        })
    }
    
    func showOrderPricingView(superView : UIView , moveType : MoveType ,requestPara : ServiceRequest ) {
        request = requestPara
        
        if !isAdded {
            
            frameHeight =  superView.frame.size.width*70/100
            viewSuper = superView
            self.frame = CGRect(x: BookingPopUpFrames.XPopUp, y: (superView.frame.origin.y + superView.frame.size.height) , width: BookingPopUpFrames.WidthPopUp, height: frameHeight)
            superView.addSubview(self)
            isAdded = true
        }
         assignData()
        updatePaymentMode(service: requestPara)
         maximizeOrderPricingView()
    }
    
    
    func assignData() {
        
        
        if    BundleLocalization.sharedInstance().language == Languages.English{
            if request.serviceSelected?.serviceCategoryId == 3{
                txtInfoTotal.text = "Base price"
            }
            else{
                txtInfoTotal.text = "Total"
            }
        }
        
         lblBrandName.text =  /request.serviceSelected?.serviceCategoryId != 2 ?   /request.serviceSelected?.serviceName : /request.selectedBrand?.brandName
        
        var totalCost : Float = 0.0
        
        if  /request.serviceSelected?.serviceCategoryId == 4 {
            
             lblOrderDetails.text =   /request.selectedProduct?.productName
            lblBrandName.text =   /request.selectedBrand?.brandName
            
        } else if /request.serviceSelected?.serviceCategoryId == 7  {
            
            lblBrandName.text =   /request.serviceSelected?.serviceName
            lblOrderDetails.text =  /request.selectedBrand?.brandName + " : "  + /request.selectedProduct?.productName
            
        } else{
            
            lblBrandName.text =   /request.serviceSelected?.serviceName
            //change Virat
//            lblOrderDetails.text =   /request.productName + " × " +  String(request.quantity)
            lblOrderDetails.text =   request.quantity > 0 ?  "" : /request.productName
            
        }
        
        //Change Virat
        if request.quantity > 0 {
            totalCost = Float(request.quantity)
            
        }else {
            if let pricePPD = request.selectedProduct?.pricePerDistance {
                totalCost = (pricePPD * request.distance)
                
            }
            
            if let pricePPQ = request.selectedProduct?.pricePerQuantity{
                totalCost = totalCost + (pricePPQ * Float(request.quantity))
                
            }
            
            totalCost = totalCost + Float(/request.selectedProduct?.alphaPrice)
            totalCost = totalCost + totalCost.getBuraqShare(percent: /request.serviceSelected?.buraqPercentage)
        }
        
        
        lblFinalPrice.text =  String(totalCost).getTwoDecimalFloat() + " " + "currency".localizedString
        btnBookService.titleLabel?.textAlignment = .center
        
        if request.requestType == .Future {
           
            let strOrderType = "schedule".localizedString
            let strDate = strOrderType + "\n" + request.orderDateTime.toLocalDateAcTOLocale() + ", " + request.orderDateTime.toLocalTimeAcTOLocale()
            btnBookService.setTitle( strDate, for: .normal)
            
        }else{
            
            
            if BundleLocalization.sharedInstance().language == Languages.English{
                
                if request.serviceSelected?.serviceCategoryId == 2{
                    btnBookService.setTitle("Order Now", for: .normal)
                }
                else{
                    btnBookService.setTitle("Book Now", for: .normal)
                }
            }
            else{
                btnBookService.setTitle( "book_now".localizedString, for: .normal)
            }
            
        }
      
    }

    
    func updatePaymentMode(service : ServiceRequest) {
        
        btnPromoCodeType.layer.borderColor = UIColor.appTheme.cgColor
        btnPaymentType.layer.borderColor = UIColor.appTheme.cgColor
        btnPaymentType.layer.cornerRadius = 5

        switch service.paymentMode {
        case .Card:
            
            lblPaymentType.textColor = UIColor.lightGray
            lblPromoCodeType.textColor = R.color.appTheme()
            btnPromoCodeType.layer.borderWidth = 1
            btnPaymentType.layer.borderWidth = 0
        case .Cash:
            
            lblPaymentType.textColor = R.color.appTheme()
            lblPromoCodeType.textColor = UIColor.lightGray
            btnPromoCodeType.layer.borderWidth = 0
            btnPaymentType.layer.borderWidth = 1
        case .EToken:
            
            imgViewPaymentType.image = #imageLiteral(resourceName: "ic_token")
            lblPaymentType.text = "e_token".localizedString
            
        }
        
//        switch service.paymentMode {
//        case .Card:
//
//            lblPaymentType.text = "card".localizedString
//            imgViewPaymentType.image = #imageLiteral(resourceName: "CreditCard")
//         case .Cash:
//
//             imgViewPaymentType.image = #imageLiteral(resourceName: "CashOnDelivery")
//            lblPaymentType.text = "cash".localizedString
//        case .EToken:
//
//             imgViewPaymentType.image = #imageLiteral(resourceName: "ic_token")
//             lblPaymentType.text = "e_token".localizedString
//
//            }
    }
    
}
