//
//  InvoiceView.swift
//  Buraq24
//
//  Created by MANINDER on 28/08/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit

class InvoiceView: UIView {

    //MARK:- IBOutlets
    //MARK:-
    @IBOutlet var lblBrandProductName: UILabel!
    @IBOutlet var lblOrderDetails: UILabel!
    @IBOutlet var lblBaseFairValue: UILabel!
    @IBOutlet var lblFinalAmount: UILabel!
    
    @IBOutlet var lblOthersPrice: UILabel!
    @IBOutlet var lblTaxValue: UILabel!
    //MARK:- Properties
    //MARK:-
    
    var viewSuper : UIView?
    var delegate : BookRequestDelegate?
    var isAdded : Bool = false
    var orderDone : Order?
    
    var frameHeight : CGFloat = 300
    
    
    //MARK:- Actions
    //MARK:-
    @IBAction func actionBtnFinishOrder(_ sender: Any) {
        self.minimizeInvoiceView()
        self.delegate?.didSelectNext(type: .DoneInvoice)
    }
    
   
    //MARK:- Functions
    //MARK:-
    
    func minimizeInvoiceView() {
        
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.frame = CGRect(x: BookingPopUpFrames.XPopUp, y: /self?.viewSuper?.frame.origin.y + /self?.viewSuper?.frame.size.height , width: BookingPopUpFrames.WidthPopUp, height: /self?.frameHeight)
            }, completion: { (done) in
        })
    }
    
    func maximizeInvoiceView() {
        
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.frame = CGRect(x: BookingPopUpFrames.XPopUp, y: (/self?.viewSuper?.frame.origin.y + /self?.viewSuper?.frame.size.height) - CGFloat(BookingPopUpFrames.PaddingX) - /self?.frameHeight , width: BookingPopUpFrames.WidthPopUp, height: /self?.frameHeight)
            }, completion: { (done) in
        })
    }
    
    func showInvoiceView(superView : UIView ,order : Order ) {
        orderDone = order
        if !isAdded {
//            frameHeight =  superView.frame.size.width*76/100
            viewSuper = superView
            self.frame = CGRect(x: BookingPopUpFrames.XPopUp, y: (superView.frame.origin.y + superView.frame.size.height) , width: BookingPopUpFrames.WidthPopUp, height: frameHeight)
            superView.addSubview(self)
            isAdded = true
        }
        
         assignPopUpData()
        maximizeInvoiceView()
    }
    
    
    func assignPopUpData() {
        
        guard let currentOrd = orderDone else{return}
        
        guard let service = UDSingleton.shared.getService(categoryId: currentOrd.serviceId) else {return}
        lblBrandProductName.text = /service.serviceName
        guard let payment = currentOrd.payment else{return}
        
        
        if /service.serviceCategoryId == 2 || /service.serviceCategoryId == 4 {
            lblBrandProductName.text = /currentOrd.orderProductDetail?.productBrandName
        }else{
            lblBrandProductName.text = /service.serviceName
        }
        
        
        let orderDetail  = /service.serviceCategoryId > 3 ? (/currentOrd.orderProductDetail?.productName)   : (/currentOrd.orderProductDetail?.productName + " × " +  String(/payment.productQuantity))
       
        lblOrderDetails.text = orderDetail
//        lblBaseFairValue.text =  (/payment.initalCharge).getTwoDecimalFloat() + " " + "currency".localizedString
//        lblTaxValue.text = (/payment.adminCharge).getTwoDecimalFloat() + " " + "currency".localizedString
//        lblFinalAmount.text =  (/payment.finalCharge).getTwoDecimalFloat() + " " + "currency".localizedString

        var total : Double = 0.0
        
        if let initalCharge = Double(/payment.initalCharge) {
            total =  initalCharge
        }
        
        if let adminCharge = Double(/payment.adminCharge) {
            total =  total + adminCharge
        }
        
        lblBaseFairValue.text =  "\(total)".getTwoDecimalFloat() + " " + "currency".localizedString
        lblTaxValue.text = "0.0".getTwoDecimalFloat() + " " + "currency".localizedString
        lblFinalAmount.text =  (/payment.finalCharge).getTwoDecimalFloat() + " " + "currency".localizedString
        lblOthersPrice.text = "0.0".getTwoDecimalFloat() + " " + "currency".localizedString
        
    }
}
