//
//  SignUpUploadDocCell.swift
//  Buraq24
//
//  Created by Apple on 16/01/20.
//  Copyright © 2020 CodeBrewLabs. All rights reserved.
//

import UIKit

class SignUpUploadDocCell: UITableViewCell {
    
    //MARK: -IBOutlets
    @IBOutlet weak var imgDoc: UIImageView!
    @IBOutlet weak var lblDocName: UILabel!
    
    //MARK: -LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK: -UIActions
    @IBAction func btnCross(sender: UIButton){
        
    }
  
    
}
