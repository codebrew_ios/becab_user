//
//  RequestAcceptedView.swift
//  Buraq24
//
//  Created by MANINDER on 28/08/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit

class RequestAcceptedView: UIView {
   
    //MARK:- IBOutlets
    //MARK:-
    
    @IBOutlet var imgViewDriverRating: UIImageView!
    @IBOutlet var imgViewDriver: UIImageView!
    
    @IBOutlet var lblDriverRatingTotalCount: UILabel!
    @IBOutlet var lblDriverName: UILabel!
    @IBOutlet var lblDriverStatus: UILabel!
    @IBOutlet var lblTimeEstimation: UILabel!
    
//    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var lblDistance: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblP: UILabel!
//    @IBOutlet var lblPeople: UILabel!
    @IBOutlet var lblBrand: UILabel!
    @IBOutlet var lblVehicleNo: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var lblSeparatorCancel: UILabel!
    @IBOutlet weak var stackMain: UIStackView!
    
    
    //MARK:- Properties
    //MARK:-
    
    var viewSuper : UIView?
    var delegate : BookRequestDelegate?
    var isAdded : Bool = false
    var orderCurrent : Order?
    var frameHeight : CGFloat = 240
    
    
    //MARK:- Actions
    //MARK:-
    @IBAction func actionBtnCancelTrackingOrder(_ sender: Any) {
        self.delegate?.didSelectNext(type: .CancelTrackingOrder)
    }
    
    @IBAction func actionBtnCallDriverPressed(_ sender: UIButton) {
        
        guard let order = orderCurrent else {return}
        guard let driverDetail = order.driverAssigned else{return}
        guard let intNumber = driverDetail.driverPhoneNumber else {return}
        self.callToNumber(number: String(intNumber))

    }
    
    //MARK:- Functions
    //MARK:-
    
    func minimizeDriverView() {
        
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.frame = CGRect(x: BookingPopUpFrames.XPopUp, y: /self?.viewSuper?.frame.origin.y + /self?.viewSuper?.frame.size.height , width: BookingPopUpFrames.WidthPopUp, height: /self?.frameHeight)
            }, completion: { (done) in
        })
    }
    
    func maximizeDriverView() {

        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.frame = CGRect(x: BookingPopUpFrames.XPopUp, y: (/self?.viewSuper?.frame.origin.y + /self?.viewSuper?.frame.size.height) - CGFloat(BookingPopUpFrames.PaddingX) - /self?.frameHeight , width: BookingPopUpFrames.WidthPopUp, height: /self?.frameHeight)
            }, completion: { (done) in
        })
    }
    
    func showDriverAcceptedView(superView: UIView, order: Order, label: UILabel) {
        orderCurrent = order
        if !isAdded {
            frameHeight =  superView.frame.size.width*60/100
            viewSuper = superView
            self.frame = CGRect(x: BookingPopUpFrames.XPopUp, y: (superView.frame.origin.y + superView.frame.size.height) , width: BookingPopUpFrames.WidthPopUp, height: frameHeight)
            superView.addSubview(self)
            isAdded = true
            
            self.sizeToFit()
            self.layoutIfNeeded()
            frameHeight =  self.frame.height
            self.frame = CGRect(x: BookingPopUpFrames.XPopUp, y: (superView.frame.origin.y + superView.frame.size.height) , width: BookingPopUpFrames.WidthPopUp, height: frameHeight)

        }
        assignPopUpData(label: label)
        maximizeDriverView()
    }
    
    
    func assignPopUpData(label: UILabel) {
        
        guard let order = orderCurrent else {return}
        guard let driverDetail = order.driverAssigned else{return}
        lblDriverName.text = /driverDetail.driverName
        label.text = "driver_accepted_request".localizedString
        let valDouble = (/order.payment?.finalCharge).getTwoDecimalFloat() + " " + "currency".localizedString

        if (order.serviceId == 4 || order.serviceId == 7) {
            lblBrand.text = order.orderProductDetail?.productBrandName
        }
        lblP.text = valDouble
       // lblVehicleNo.text = order.orderProductDetail?.productName
        //lblPeople.text = /order.orderProductDetail?.productName
//        lblPrice.text = valDouble
        
    //  lblDriverStatus.text = order.myTurn == .MyTurn ? "driver_is_on_the_way".localizedString : "driver_completing_nearby_order".localizedString
        
        if let  driverimage = driverDetail.driverProfilePic {
        
          if let url = URL(string: driverimage) {
                imgViewDriver.sd_setImage(with: url , completed: nil)
          }else{
                imgViewDriver.image = #imageLiteral(resourceName: "ic_user")
            }
        }
        
     
        guard let driverRating = driverDetail.driverRatingCount else { return }
        guard let driverAverage = driverDetail.driverRatingAverage else { return }
        
        if let intAverage = Int(driverAverage) {
        
            imgViewDriverRating.isHidden =  driverRating == 0
            lblDriverRatingTotalCount.isHidden =  driverRating == 0
            lblDriverRatingTotalCount.text = "\(driverRating)"
            imgViewDriverRating.setRatingSmall(rating: intAverage)
        }
        
//        lblDriverStatus.text = order.myTurn == .MyTurn ? "driver_is_on_the_way".localizedString : "driver_accepted_request".localizedString
        if order.serviceId == 7 {  //Cab
            label.text = order.myTurn == .MyTurn ? "driver_is_on_the_way_DriveStarted".localizedString : "driver_accepted_request".localizedString
        } else {
            label.text = order.myTurn == .MyTurn ? "truck_driver_is_on_the_way".localizedString : "driver_accepted_request".localizedString
        }

        if order.orderStatus == .reached {
            label.text = "driver_is_reached".localizedString
        }
        btnCancel.isHidden = false
        if order.orderStatus == .Ongoing {
            btnCancel.isHidden = true
        }
        lblSeparatorCancel.isHidden = btnCancel.isHidden
        stackMain.spacing = btnCancel.isHidden ? 30 : 8
    }
    
    
    func setOrderStatus(tracking : TrackingModel, label: UILabel) {
//        lblDriverStatus.text = tracking.orderTurn == .MyTurn ? "driver_is_on_the_way".localizedString : "driver_completing_nearby_order".localizedString
//        lblDriverStatus.text = tracking.orderTurn == .MyTurn ? "driver_is_on_the_way_DriveStarted".localizedString : "driver_completing_nearby_order".localizedString
        btnCancel.isHidden = false
        if orderCurrent?.serviceId == 7 {  //Cab
            label.text = tracking.orderTurn == .MyTurn ? "driver_is_on_the_way_DriveStarted".localizedString : "driver_accepted_request".localizedString
        } else {
            label.text = tracking.orderTurn == .MyTurn ? "truck_driver_is_on_the_way".localizedString : "driver_accepted_request".localizedString
        }
        if tracking.orderStatus == .reached {
            label.text = "driver_is_reached".localizedString
        }
        if tracking.orderStatus == .Ongoing {
            btnCancel.isHidden = true
        }
        lblSeparatorCancel.isHidden = btnCancel.isHidden
        stackMain.spacing = btnCancel.isHidden ? 30 : 8
    }
   
}
