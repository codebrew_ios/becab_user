//
//  SelectServiceView.swift
//  Buraq24
//
//  Created by MANINDER on 28/08/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit

class SelectServiceView: UIView, iCarouselDataSource, iCarouselDelegate {
    
    //MARK:- Outlets
    @IBOutlet var collectionViewServices: UICollectionView!
    @IBOutlet var lblSelectedService: UILabel?
    @IBOutlet var lblLocationName: UILabel!
    @IBOutlet var viewCrousal: iCarousel!
    @IBOutlet var btnNextService: UIButton!
    @IBOutlet weak var crousalHeightCOnstant: NSLayoutConstraint!
    @IBOutlet var imgViewLocationtype: UIImageView!
    
    //MARK:- Properties
    var previousIndex : Int = -1
    
    var collectionViewDataSource : CollectionViewDataSource?
    var services : [Service] = [Service]()
    var heightPopUp : CGFloat = 200
    var viewSuper : UIView?
    var request : ServiceRequest?
    var delegate : BookRequestDelegate?

    lazy var serviceLocal = [ServiceType]()
    
    var firstTime = false
    var isAdded : Bool = false
    
    //MARK:- Actions
    @IBAction func actionBtnSelectLocation(_ sender: UIButton) {
        
        let idCateGory = /request?.serviceSelected?.serviceCategoryId
        
        self.delegate?.didSelectNext(type: .SelectLocation)
        minimizeSelectServiceView()
        
//        if idCateGory == 5  {
//            Alerts.shared.show(alert: "AppName".localizedString, message: "coming_soon".localizedString , type: .error )
//        }else{
//            self.delegate?.didSelectNext(type: .SelectLocation)
//            minimizeSelectServiceView()
//        }
    }
    
    //MARK:- Functions
    /// Configure Service CollectionView
    func configureServiceCollectionView() {
        
        viewCrousal.delegate = self
        viewCrousal.dataSource = self
        viewCrousal.type = .linear
        //        viewCrousal.isVertical = true
        viewCrousal.currentItemIndex = numberOfItems(in: viewCrousal)/2
        
        if   UIDevice.current.userInterfaceIdiom == .pad{
            self.crousalHeightCOnstant.constant = 120.0
        }
        
    }
    
    private func updateSelectedText(_ strText : String) {
        lblSelectedService?.text = strText
        print("lable3 : \(/lblSelectedService?.text)")
    }
    
    func minimizeSelectServiceView() {
        
        UIView.animate(withDuration: 0.3, animations: {
            [weak self] in
            self?.frame = CGRect(x: 0 , y: /self?.viewSuper?.frame.origin.y + /self?.viewSuper?.frame.size.height  + 20, width: BookingPopUpFrames.WidthFull, height: /self?.heightPopUp)
            }, completion: { (done) in
        })
    }
    
    func maximizeSelectServiceView() {
        
        UIView.animate(withDuration: 0.3, animations: {
            [weak self] in
            self?.frame = CGRect(x: 0, y: (/self?.viewSuper?.frame.origin.y + /self?.viewSuper?.frame.size.height) - CGFloat(BookingPopUpFrames.PaddingX) - /self?.heightPopUp , width: BookingPopUpFrames.WidthFull, height: /self?.heightPopUp)
            }, completion: {
                (done) in
        })
    }
    
    func showSelectServiceView(superView: UIView, moveType: MoveType, requestPara: ServiceRequest, service: [Service]) {

        btnNextService.setImage(#imageLiteral(resourceName: "NextMaterial").setLocalizedImage(), for: .normal)
        request = requestPara
        
        services = service
        
        if !isAdded {
            viewSuper = superView
            self.frame = CGRect(x: 0, y: (superView.frame.origin.y + superView.frame.size.height) , width: BookingPopUpFrames.WidthFull, height: heightPopUp)
            superView.addSubview(self)
            self.createServices()
            configureServiceCollectionView()
            isAdded = true
        }
        
        if firstTime == false {
            print(service.count)
            print(service)
            lblSelectedService?.text = previousIndex == -1 ? /service[0].serviceName : /service[2].serviceName
            firstTime = true
        }
        
        print("lable1 : \(String(describing: lblSelectedService?.text))")
        
        if  let languageCode = UserDefaultsManager.languageId{
            if languageCode == "3" ||  languageCode == "5" {
                lblLocationName.textAlignment = .right
                
            }else{
                lblLocationName.textAlignment = .left
                
            }
        }
        maximizeSelectServiceView()
        
    }
    
    
    func createServices() {
        
        serviceLocal.removeAll()
        
//        let bikeBooking = services.first(where: {$0.serviceCategoryId == 12}) ?? Service()
//
//        serviceLocal.append( ServiceType.init(serviceName: "Moto" , serviceImageSelected: UIImage.init(named: "ic_moto") ?? UIImage(), serviceImageUnSelected: UIImage.init(named: "ic_moto") ?? UIImage(), objService: bikeBooking))
        
        let carBooking = services.first(where: {$0.serviceCategoryId == 7}) ?? Service()
        serviceLocal.append(ServiceType(serviceName: "Taxi Booking" , serviceImageSelected: UIImage.init(named: "ic_Carro") ?? UIImage(), serviceImageUnSelected: UIImage.init(named: "ic_Carro") ?? UIImage(), objService: carBooking))
 
//        let pickUpDelivery = services.first(where: {$0.serviceCategoryId == 4}) ?? Service()
//
//        serviceLocal.append(ServiceType(serviceName: "Mensajeria" , serviceImageSelected: UIImage.init(named: "ic_Mensajería") ?? UIImage(), serviceImageUnSelected: UIImage.init(named: "ic_Mensajería") ?? UIImage(), objService: pickUpDelivery))
    }
    
    //MARK:- iCrousal  Delegate & Data Source
    func numberOfItems(in carousel: iCarousel) -> Int {
        return self.serviceLocal.count//*10000
    }
    
    func carouselWillBeginScrollingAnimation(_ carousel: iCarousel) {
        if  let imageview = carousel.currentItemView as? UIImageView {
            let lcoalIndex = carousel.currentItemIndex % self.serviceLocal.count
            let localObj = self.serviceLocal[lcoalIndex]//(lblSelectedService.text == "Trucks") ? self.serviceLocal[0] : self.serviceLocal[1]
            imageview.image = localObj.serviceImageUnSelected
        }
    }
    
    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        
    }
    
    func carouselDidScroll(_ carousel: iCarousel) {
    }
    
    func carouselDidEndScrollingAnimation(_ carousel: iCarousel) {
        
        if let imageview = carousel.currentItemView as? UIImageView {
            let lcoalIndex = carousel.currentItemIndex % self.serviceLocal.count
            let localObj = self.serviceLocal[lcoalIndex]
            
            UIView.transition(with: imageview,
                              duration:0.6,
                              options: .transitionCrossDissolve,
                              animations: {
                                imageview.image = localObj.serviceImageSelected
                                imageview.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)},
                              completion: nil)
            
            if   previousIndex !=  carousel.currentItemIndex {
                carousel.reloadItem(at: previousIndex, animated: false)
            }
            
            previousIndex = carousel.currentItemIndex
            
            lblSelectedService?.text =  localObj.serviceName
            request?.serviceSelected = localObj.objService
            
            delegate?.didSelectServiceType(localObj.objService)
            
            let idCateGory = /localObj.objService.serviceCategoryId
            imgViewLocationtype.image = idCateGory > 3 ? #imageLiteral(resourceName: "PickUpIcon") : #imageLiteral(resourceName: "DropIcon")
        }
    }

    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        
        var itemView: UIImageView
        //reuse view if available, otherwise create a new view
        if let view = view as? UIImageView {
            itemView = view
        } else {
            if   UIDevice.current.userInterfaceIdiom == .pad {
                itemView = UIImageView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
            } else {
                itemView = UIImageView(frame: CGRect(x: 0, y: 0, width: 70, height: 70))
            }
        }
        
        let indexnew = index % self.serviceLocal.count
        let localObj = self.serviceLocal[indexnew]
        
        itemView.setCornerRadius(radius: 35)
        
        lblSelectedService?.text = localObj.serviceName
        print("Label3 :\(localObj.serviceName)")
        itemView.clipsToBounds = false
        itemView.layer.shadowColor = UIColor.lightGray.cgColor
        itemView.layer.shadowOpacity = 0.4
        itemView.layer.shadowOffset = CGSize.zero
        itemView.layer.shadowRadius = 5
        itemView.layer.shadowPath = UIBezierPath(roundedRect: itemView.bounds, cornerRadius: 35).cgPath
        itemView.image = localObj.serviceImageUnSelected
        return itemView
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if (option == .spacing) {
            return value * 1.18
        }
        return value
    }

    func carousel(_ carousel: iCarousel, shouldSelectItemAt index: Int) -> Bool {
        return true
        
    }
    
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        
    }
}
