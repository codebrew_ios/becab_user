//
//  main.swift
//  Buraq24
//
//  Created by Apple on 14/12/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit

UIApplicationMain(
    CommandLine.argc,
    UnsafeMutableRawPointer(CommandLine.unsafeArgv)
        .bindMemory(
            to: UnsafeMutablePointer<Int8>.self,
            capacity: Int(CommandLine.argc)),
    NSStringFromClass(UserInactivity.self),
    NSStringFromClass(AppDelegate.self)
    
)
