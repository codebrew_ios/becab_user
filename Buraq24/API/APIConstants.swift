//
//  APIConstants.swift
//  Buraq24
//
//  Created by MANINDER on 16/08/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import Foundation

internal struct APIBasePath {
    
    //Live
    static let basePath = "http://139.59.81.135:9013"
    //Local
    //static let basePath = "http://192.168.100.127:9002"

    static let AppStoreURL = "https://itunes.apple.com/app/id1467144828"
  
   //static let googleApiKey = "AIzaSyD6xLHr8Q98Ldfl0UuAOsa8Vvx2i2qiXTM" 
    
    static let googleApiKey = "AIzaSyC6xosf0PuuRmqPL8qw1kbLFpPr-qYygQw"
    static let stripePublishableKey = "pk_test_qARpjw55LlA6Gv6pUQbmxbYB00f6qQvNNB"
    
    static let privacyUrl = "http://139.59.81.135:9002/PPolicy"
    static let termsOfUse = "http://139.59.81.135:9002/termsOfuse"
    static let driverAgreement = "http://139.59.81.135:9002/driverAgreement"
    static let aboutUs = "http://139.59.81.135:9002/AboutUs/1"

}

internal struct APITypes {
    
    //Login Signup
    
    static let sendOtp = "sendOtp"
    static let verifyOTP = "verifyOTP"
    static let addName = "addName"
    static let logOut = "logout"
    static let updateData = "updateData"
    static let eContacts = "eContacts"
    static let contactUs = "contactus"
    static let editProfile = "profileUpdate"
    static let changeNotification = "settingUpdate"
    
    //Service APIs
    static let homeAPI = "homeApi"
    static let requestAPI = "Request"
    static let cancelRequestAPI = "Cancel"
    static let ongoingRequestAPI = "Ongoing"
    static let rate = "Rate"
    
    //Buy EToken
    static let eTokens = "ETokens"
    static let paginate = "ETokens/Paginate"
    static let buyEToken = "eToken/buy"
    static let eTokenDetails = "payment/details"
    static let bookingHistory = "order/history"
    static let orderDetails = "order/details"
    static let companyList = "companies/list"
    static let etokensList = "etokens/list"
    static let etokenPurchasedList = "purchases/list"
    
    static let etokenPurchase = "etoken/purchase"
    static let etokenConfirmReject   = "confirm/order"
    
    static let addUserCard = "addUserCard"
    static let getUserCardList = "getUserCardList"
    static let deleteUserCard = "deleteUserCard"
    static let checkPickup = "checkPickup"
    
}

internal struct Routes{
    static let user = "/user/"
    static let commonRoutes = "/common/"
    
    struct SubRoute {
        static let service = "service/"
        static let other = "other/"
        static let water = "water/"
    }
}

enum APIConstants:String {
    case success = "success"
    case message = "msg"
    case accessToken = "accessToken"
    case statusCode = "statusCode"
}

enum SocialLoginType : String {
    
    case facebook = "Facebook"
    case google = "Google"
}

enum Keys : String {
    
    case access_token = "access_token"
    case language_id = "language_id"
    case phone_code = "phone_code"
    case phone_number = "phone_number"
    case timezone = "timezone"
    case latitude = "latitude"
    case longitude = "longitude"
    case socket_id = "socket_id"
    case fcm_id = "fcm_id"
    case device_type = "device_type"
    case otp = "otp"
    case name = "name"
    case distance = "distance"
    case category_id = "category_id"
    case payment_type = "payment_type"
    case category_brand_id = "category_brand_id"
    case category_brand_product_id = "category_brand_product_id"
    case product_quantity = "product_quantity"
    case pickup_address = "pickup_address"
    case pickup_latitude = "pickup_latitude"
    case pickup_longitude = "pickup_longitude"
    case token_id = "token_id"
    case defaultCard
    case card_holder_name = "card_holder_name"
    case exp_month = "exp_month"
    case exp_year = "exp_year"
    
    case dropoff_address = "dropoff_address"
    case dropoff_latitude = "dropoff_latitude"
    case dropoff_longitude = "dropoff_longitude"
    
    case order_timings = "order_timings"
    case future = "future"
    case orderId = "order_id"
    case cancelReason = "cancel_reason"
    case ratings = "ratings"
    case comments = "comments"
    case take = "take"
    case organisation_coupon_id = "organisation_coupon_id"
    case organisation_coupon_user_id = "organisation_coupon_user_id"
    case coupon_user_id = "coupon_user_id"
    case skip = "skip"
    case type = "type"
    case message = "message"
    case profile_pic = "profile_pic"
    case notifications = "notifications"
    case order_images = "order_images"
    case product_weight = "product_weight"
    case details = "details"
    case material_details = "material_details"
    case order_distance = "order_distance"
    case status = "status"
    case product_price = "product_price"
    case card_id = "card_id"
    case identification_certificate = "identification_certificate"
    
    // water modules
    
    case organisation_id = "organisation_id"
    case buraq_percentage = "buraq_percentage"
    case bottle_returned_value = "bottle_returned_value"
    case bottle_charge = "bottle_charge"
    case quantity = "quantity"
    case price  = "price"
    case eToken_quantity = "eToken_quantity"
    
    case address = "address"
    case  address_latitude = "address_latitude"
    case  address_longitude  = "address_longitude"
    
    case pickupPersonName = "pickup_person_name"
    case pickupPersonPhone = "pickup_person_phone"
    case invoiceNumber = "invoice_number"
    case deliveryPersonName = "delivery_person_name"
    
    case brandName = "brand_name"
}

enum Validate : String {
    
    case none
    case success = "1"
    case successCode = "200"
    case failure = "0"
    case invalidAccessToken = "401"
    case fbLogin = "3"
    case validation = "400"
    case apiError = "500"
    
    func map(response message : String?) -> String? {
        
        switch self {
        case .success:
            return message
        case .failure :
            return message
        case .invalidAccessToken :
            return message
        default:
            return nil
        }
    }
}

enum Response {
    case success(AnyObject?)
    case failure(String?)
}

typealias OptionalDictionary = [String : Any]?

let reasonString = "CANCELLING_WHILE_REQUESTING"

struct Parameters {
    
    //User Login SignUp Routes
    static let sendOtp : [Keys] = [.language_id , .phone_code , .phone_number , .timezone , .latitude , .longitude , .socket_id , .fcm_id, .device_type ]
    static let verifyOTP : [Keys] = [.otp ]
    static let addName : [Keys] = [.name]
    static let logout : [Keys] = []
    static let homeApi : [Keys] = [ .category_id  , .latitude , .longitude ,.distance ]
    static let checkPickup: [Keys] = [.pickup_latitude, .pickup_longitude]
    static let requestAPi : [Keys] = [
        .category_id,
        .category_brand_id,
        .category_brand_product_id,
        .product_quantity,
        .dropoff_address,
        .dropoff_latitude,
        .dropoff_longitude,
        .pickup_address,
        .pickup_latitude,
        .pickup_longitude,
        .order_timings,
        .future,
        .payment_type,
        .distance,
        .organisation_coupon_user_id,
        .material_details,
        .product_weight,
        .details,
        .order_distance,
        .pickupPersonName,
        .pickupPersonPhone,
        .invoiceNumber,
        .deliveryPersonName,
        .brandName,
        .product_price,
        .card_id
    ]
    static let cancelRequestApi : [Keys] = [ .orderId , .cancelReason]
    static let ongoingApi : [Keys] = []
    static let rateDriver : [Keys] = [.orderId , .ratings , .comments]
    static let eTokens : [Keys] = [.category_id , .latitude , .longitude ,.distance ,.take , .order_timings]
    static let eTokenDetails : [Keys] = [.category_id , .latitude , .longitude ,.distance ,.take , .order_timings]
    static let buyETokens : [Keys] = [.organisation_coupon_id ]
    static let updateData : [Keys] = [.timezone , .latitude , .longitude , .fcm_id]
    static let history : [Keys] = [.skip , .take , .type]
    static let contactUs : [Keys] = [.message]
    static let editProfile : [Keys] = [.name]
    static let orderDetail : [Keys] = [.orderId]
    static let changeNotification : [Keys] = [.notifications]
    static let getCompanyListWater :[Keys] = [.type, .latitude, .longitude, .category_brand_id, .category_brand_product_id,.take,.skip]
    static let getCompanyTokenList :[Keys] = [.organisation_id,.category_brand_id, .skip, .take]
    static let getTokenPurchaseList:[Keys] = [.skip, .take]
    
    static let purchaseEToken :[Keys] = [.organisation_coupon_id,.buraq_percentage, .bottle_returned_value, .bottle_charge, .quantity, .payment_type, .price, .eToken_quantity,.address, .address_latitude, .address_longitude]
    
    static let EtokenOrderAcceptReject :[Keys] = [.orderId, .status]
    static let addUserCard: [Keys] = [.token_id, .card_holder_name, .exp_month, .exp_year]
    static let addCard: [Keys] = [.token_id, .defaultCard]
    static let getUserCardList: [Keys] = []
    static let deleteUserCard: [Keys] = [.card_id]
    
}

struct Headers {
    
    //User Login SignUp Routes
    static let headerBasic : [Keys] = [.language_id]
    static let headerUserInfo : [Keys] = [.language_id , .access_token  ]
    
}

