

import Foundation
import SwiftyJSON
import ObjectMapper

enum ResponseKeys : String {
    case user = "profile"
    case countries = "countries"
    case data = "data"
    case count = "count"
    case stats = "stats"
    case messages = "messages"
    case cardId = "card_id"
    case flag = "flag"
    case msg = "msg"
    case cuisines = "cuisines"
    case userPost = "userPost"
    case billingAddress = "billingAddress"
    case serviceId = "serviceId"
    case serviceMakId = "serviceMakId"
    case usersAddress = "usersAddress"
    case multipleAddress = "multipleAddress"
}

extension LoginEndpoint {
    
    func handle(parameters : JSON) -> Any? {
        
        switch self {
        case .sendOtp(_):
            let obj = Mapper<ApiSucessData<SendOtp>>().map(JSONObject: parameters.dictionaryObject)
            return obj?.object
            
        case .verifyOTP(_) :
            let obj = Mapper<ApiSucessData<LoginDetail>>().map(JSONObject: parameters.dictionaryObject)
            return obj?.object
        case .addName(_) :
            let obj = Mapper<ApiSucessData<UserDetail>>().map(JSONObject: parameters.dictionaryObject)
            return obj?.object
        case .eContacts() :
            let obj = Mapper<ApiSucessData<EmergencyContact>>().map(JSONObject: parameters.dictionaryObject)
            return obj?.array
        case .logOut() , . contactUs(_) :
            return "Success"
        case .updateData(_):
            let obj = Mapper<ApiSucessData<Home>>().map(JSONObject: parameters.dictionaryObject)
            return obj?.object
        case .editProfile(_):
            let obj = Mapper<ApiSucessData<UserDetail>>().map(JSONObject: parameters.dictionaryObject)
            return obj?.object
        case .updateNotifications(_):
            let obj = Mapper<ApiSucessData<UserDetail>>().map(JSONObject: parameters.dictionaryObject)
            return obj?.object
        }
    }
}

extension BookServiceEndPoint {
    
    func handle(parameters : JSON) -> Any? {
        let dictObj = parameters.dictionaryObject
        switch self {
        case .homeApi(_):
            
            let obj = Mapper<ApiSucessData<Home>>().map(JSONObject: dictObj)
            return obj?.object
            
        case .requestApi(_):
            
            let obj = Mapper<ApiSucessData<Order>>().map(JSONObject: dictObj)
            return obj?.object
            
        case .cancelRequest(_):
            
            return "Success"
            
        case .checkPickup(_):
            
            return (dictObj?["result"] as? [String: Any])?["address_restricted"] as? Bool
            
        case .onGoingRequest():
            
            let obj = Mapper<ApiSucessData<Order>>().map(JSONObject: dictObj)
            return obj?.object
            
        case .rateDriver(_) , .buyETokens(_):
            return "Success"
            
        case .eTokens(_):
            
            let obj = Mapper<ApiSucessData<EToken>>().map(JSONObject: dictObj)
            return obj?.object
            
        case .getEtokenDetail(_) :
            
            let obj = Mapper<ApiSucessData<EToken>>().map(JSONObject: dictObj)
            return obj?.object
            
        case .history(_) :
            
            let obj = Mapper<ApiSucessData<Order>>().map(JSONObject: dictObj)
            return obj?.array
            
        case .orderDetails(_) :
            
            let obj = Mapper<ApiSucessData<Order>>().map(JSONObject: dictObj)
            return obj?.object
            
        case .getCompanyListWater(_):
            
            let obj = Mapper<ApiSucessData<companies>>().map(JSONObject: dictObj)
            return  obj?.array
            
        case .getTokenList(_):
            
            let obj = Mapper<ApiSucessData<CompanyTokenListing>>().map(JSONObject: dictObj)
            return obj?.object
            
        case .getTokenPurchaseList(_):
            
            let obj = Mapper<ApiSucessData<PurchasedTokens>>().map(JSONObject: dictObj)
            return obj?.array
            
        case .purchaseEToken(_):
            
            return dictObj
            
        case .EtokenOrderAcceptReject(_):
            return dictObj
            
        case .addUserCard(_):
            return dictObj
            
        case .getUserCardList:
            let obj = Mapper<ApiSucessData<CardDataModal>>().map(JSONObject: dictObj)
            return obj?.array
        case .addCard(_):
            let obj = Mapper<ApiSucessData<CardDataModal>>().map(JSONObject: dictObj)
            return obj?.object
        case .deleteUserCard(_):
            return dictObj
        }
    }
}

func saveJSON(json: JSON, key:String){
    let jsonString = json.rawString()!
    UserDefaults.standard.setValue(jsonString, forKey: key)
    UserDefaults.standard.synchronize()
}


public func loadJSON(key: String) -> JSON {
    let defaults = UserDefaults.standard
    return JSON.init(parseJSON: defaults.value(forKey: key) as? String ?? "")
    // JSON from string must be initialized using .parse()
}
