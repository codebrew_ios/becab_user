
import Foundation
import SwiftyJSON
import NVActivityIndicatorView
import Alamofire
 
typealias Completion = (Response) -> ()

class APIManager : UIViewController {
    
    static let shared = APIManager()
    private lazy var httpClient : HTTPClient = HTTPClient()
    
    func request(with api : Router , images: [UIImage?]? = [] , isLoaderNeeded: Bool? = true , completion : @escaping Completion , header: [String: String] )  {
        
        if !isConnectedToNetwork() {
            Alerts.shared.show(alert: "AppName".localizedString, message: "Validation.InternetNotWorking".localizedString , type: .error )

            return completion(Response.failure("No Internet connection"))
        }

        if isLoaderNeeded ?? true {
            startAnimating( message: nil, messageFont: nil, type: .lineScalePulseOutRapid , color: UIColor.white , padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil)
        }
        httpClient.postRequest(withApi: api, images:images ,success: {[weak self] (data , statusCode) in
            
            guard let response = data else {
                self?.stopAnimating()

                completion(Response.failure(.none))
                return
            }
            
            let json = JSON(response)
            debugPrint(json)
          var responseType = Validate(rawValue: json[APIConstants.success.rawValue].stringValue) ?? .failure
            if statusCode == 200 {
                responseType = Validate.success
            }

            if json[APIConstants.statusCode.rawValue].stringValue == Validate.validation.rawValue {
                responseType = Validate.validation

            }else if json[APIConstants.statusCode.rawValue].stringValue == Validate.apiError.rawValue {
                   responseType = Validate.apiError
                
            }else if json[APIConstants.statusCode.rawValue].stringValue == Validate.invalidAccessToken.rawValue {
                responseType = Validate.invalidAccessToken
            }
            
            if responseType == Validate.success{
                let object : Any?
                object = api.handle( parameters: json )
                self?.stopAnimating()

                completion( Response.success(object as AnyObject))
                return
            }else if  responseType == Validate.validation ||  responseType == Validate.apiError {
                self?.stopAnimating()
                completion(Response.failure( json[APIConstants.message.rawValue].stringValue ))
                
            }
            else if   responseType == Validate.invalidAccessToken {
                UDSingleton.shared.tokenExpired()
            }
            
            }, failure: {[weak self] (message) in
                self?.stopAnimating()
                completion(Response.failure( message ))
                
        }, header: header)
    }

    
    
//    func requestDoc(with api : Router , images: [UIImage?]? = [] , isLoaderNeeded: Bool? = true , completion : @escaping Completion , header: [String: String] )  {
//
//        if !isConnectedToNetwork() {
//            Alerts.shared.show(alert: "AppName".localizedString, message: "Validation.InternetNotWorking".localizedString , type: .error )
//
//            return completion(Response.failure("No Internet connection"))
//        }
//
//        if isLoaderNeeded ?? true {
//            startAnimating( message: nil, messageFont: nil, type: .lineScalePulseOutRapid , color: UIColor.white , padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil)
//        }
//        httpClient.postRequestDoc(withApi: api, images:images ,success: {[weak self] (data , statusCode) in
//
//            guard let response = data else {
//                self?.stopAnimating()
//
//                completion(Response.failure(.none))
//                return
//            }
//
//            let json = JSON(response)
//            debugPrint(json)
//          var responseType = Validate(rawValue: json[APIConstants.success.rawValue].stringValue) ?? .failure
//            if statusCode == 200 {
//                responseType = Validate.success
//            }
//
//            if json[APIConstants.statusCode.rawValue].stringValue == Validate.validation.rawValue {
//                responseType = Validate.validation
//
//            }else if json[APIConstants.statusCode.rawValue].stringValue == Validate.apiError.rawValue {
//                   responseType = Validate.apiError
//
//            }else if json[APIConstants.statusCode.rawValue].stringValue == Validate.invalidAccessToken.rawValue {
//                responseType = Validate.invalidAccessToken
//            }
//
//            if responseType == Validate.success{
//                let object : Any?
//                object = api.handle( parameters: json )
//                self?.stopAnimating()
//
//                completion( Response.success(object as AnyObject))
//                return
//            }else if  responseType == Validate.validation ||  responseType == Validate.apiError {
//                self?.stopAnimating()
//                completion(Response.failure( json[APIConstants.message.rawValue].stringValue ))
//
//            }
//            else if   responseType == Validate.invalidAccessToken {
//                UDSingleton.shared.tokenExpired()
//            }
//
//            }, failure: {[weak self] (message) in
//                self?.stopAnimating()
//                completion(Response.failure( message ))
//
//        }, header: header)
//    }
    
    func tokenExpired(isTokenExpire: Bool) {
       // Alerts.shared.show(alert: .oops, message:  "Sorry, your account has been logged in other device! Please login again to continue." , type: .error)
      //  UserDefaultsManager.shared.tokenExpired()
    }
    
    func isLoaderNeeded(api : Router) -> Bool {
        switch api.route {
        default: return true
        }
    }

  func isConnectedToNetwork() -> Bool {
    guard let reachability = Alamofire.NetworkReachabilityManager()?.isReachable else { return false }
    return reachability ? true: false
    
   }
}

