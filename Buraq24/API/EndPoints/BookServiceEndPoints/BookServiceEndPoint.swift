//
//  BookServiceEndPoint.swift
//  Buraq24
//
//  Created by MANINDER on 17/08/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit
import Alamofire

enum BookServiceEndPoint {
    
    case homeApi(categoryID : Int? )
    case checkPickup(pickupLatitude : Double? , pickupLongitude : Double?)
    case requestApi(objRequest: ServiceRequest, categoryId : Int?, categoryBrandId : Int?, categoryBrandProductId : Int? ,productQuantity : Int? , dropOffAddress : String? , dropOffLatitude : Double? , dropOffLongitude : Double?  , pickupAddress : String? , pickupLatitude : Double? , pickupLongitude : Double? , orderTimings : String? , future : String? , paymentType : String? , distance : Int? , organisationCouponUserId : Int? , materialType : String? , productWeight : Double? , productDetail : String? , orderDistance : Float?, product_price: Int?, card_id: String?    )
    case cancelRequest(orderId : Int? , cancelReason : String?)
    case onGoingRequest()
    case rateDriver(orderId : Int? , rating : Int? , comment : String?)
    
    case eTokens(categoryId : Int?  , distance : Int? , take : Int? , orderTimings : String?)
    case getEtokenDetail(brandId : Int? , productId : Int?)
    case buyETokens(eTokenId : Int?)
    case  history(skip : Int? , take : Int? ,type : Int?)
    case  orderDetails(orderID : Int? )
    case getCompanyListWater(type: String?, latitude:Double?, longitude:Double?, category_brand_id:Int?, category_brand_product_id: Int?, take:Int,skip:Int)
    case getTokenList (organisation_id:Int,category_brand_id:Int, skip:Int, take:Int)
    case getTokenPurchaseList (skip:Int,take:Int)
    case purchaseEToken(organisation_coupon_id:Int?, buraq_percentage:Int?, bottle_returned_value:Int?, bottle_charge:Int?, quantity:Int?, payment_type:String?, price:Int?, eToken_quantity:Int?, address:String?, address_latitude:Double?, address_longitude:Double?)
    case EtokenOrderAcceptReject(order_id:String?, status: String?)
    case addUserCard(token_id: String?, cardHolder: String?, month: String?, year: String?)
    case getUserCardList
    case deleteUserCard(cardId: String?)
    case addCard(stripeToken: String, defaultStatus: Bool)
    
}

extension BookServiceEndPoint : Router {
    
    func searchRequest(isImage: Bool, images: [UIImage?]?, isLoaderNeeded: Bool?, header: [String : String], completion: @escaping Completion) -> DataRequest {
        let request = Alamofire.SessionManager.default.request("" , method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: [:]).responseJSON { response in
            print(response)
        }
        return request
    }
    
    
    func request(isImage: Bool = false , images: [UIImage?]? = [] , isLoaderNeeded: Bool? = true , header: [String: String] , completion: @escaping Completion ) {
        APIManager.shared.request(with: self, images: images, isLoaderNeeded: isLoaderNeeded, completion: completion, header: header)
    }
    
    var route : String  {
        switch self {
            
        case .homeApi(_): return APITypes.homeAPI
        case .checkPickup(_): return APITypes.checkPickup
        case .requestApi(_) : return APITypes.requestAPI
        case .cancelRequest(_) : return APITypes.cancelRequestAPI
        case .onGoingRequest() : return APITypes.ongoingRequestAPI
        case .rateDriver(_) : return APITypes.rate
        case .eTokens(_) : return APITypes.eTokens
        case .getEtokenDetail(_) : return APITypes.eTokenDetails
        case .buyETokens(_) : return APITypes.buyEToken
        case .history(_) : return APITypes.bookingHistory
        case .orderDetails(_) : return APITypes.orderDetails
        case .getCompanyListWater(_) : return APITypes.companyList
        case .getTokenList(_) : return APITypes.etokensList
        case .getTokenPurchaseList(_) : return APITypes.etokenPurchasedList
        case .purchaseEToken(_) : return APITypes.etokenPurchase
        case .EtokenOrderAcceptReject(_): return APITypes.etokenConfirmReject
        case .addUserCard(_): return APITypes.addUserCard
        case .getUserCardList: return APITypes.getUserCardList
        case .deleteUserCard(_): return APITypes.deleteUserCard
        case .addCard(_): return APITypes.addUserCard
        }
    }
    
    var parameters: OptionalDictionary {
        return format()
    }
    
    func format() -> OptionalDictionary {
        
        switch self {
            
        case .homeApi(let categoryId):
            return Parameters.homeApi.map(values: [/categoryId , Locations.lat.getLoc() , Locations.longitude.getLoc(), 2000])
        case .checkPickup(let lat, let lng):
            return Parameters.checkPickup.map(values: [/lat , /lng])
            
        case .requestApi(let objRequest, let categoryId ,  let categoryBrandId, let categoryBrandProductId, let productQuantity, let dropOffAddress , let dropOffLatitude , let dropOffLongitude,   let pickupAddress, let pickupLatitude,let pickupLongitude,let orderTimings, let future,let paymentType , let distance , let organisationCouponUserId  , let materialDetails , let productWeight , let productDetail , let orderDistance, let product_price, let card_id ):
            return Parameters.requestAPi.map(values: [
                categoryId,
                categoryBrandId,
                categoryBrandProductId,
                productQuantity,
                dropOffAddress,
                dropOffLatitude,
                dropOffLongitude,
                pickupAddress,
                pickupLatitude,
                pickupLongitude,
                orderTimings,
                future,
                paymentType,
                distance,
                organisationCouponUserId,
                materialDetails,
                productWeight,
                productDetail,
                orderDistance,
                objRequest.pickupPersonName,
                objRequest.pickupPersonPhone,
                objRequest.invoiceNumber,
                objRequest.deliveryPersonName,
                objRequest.selectedBrand?.brandName,
                product_price,
                card_id
                ])
        case .cancelRequest(let orderId , let cancelReason):
            return Parameters.cancelRequestApi.map(values: [/orderId , /cancelReason])
        case .rateDriver(let orderId , let rateValue , let comment):
            return Parameters.rateDriver.map(values: [/orderId , /rateValue , /comment])
        case .onGoingRequest():
            return Parameters.cancelRequestApi.map(values: [])
        case .eTokens(let categoryId , let distance , let take , let date):
            return Parameters.eTokens.map(values: [categoryId, Locations.lat.getLoc()  , Locations.longitude.getLoc() , distance , take , date])
        case .getEtokenDetail(let brandId , let productId ):
            return Parameters.eTokenDetails.map(values: [  brandId , productId])
        case .buyETokens(let tokenId) :
            return Parameters.buyETokens.map(values: [tokenId])
        case .history(let skip, let take ,let  type):
            return Parameters.history.map(values: [/skip , take ,/type])
        case .orderDetails(let orderId):
            return Parameters.orderDetail.map(values: [/orderId ])
            
        case .getCompanyListWater(let type, let latitude,  let longitude ,let  category_brand_id,let  category_brand_product_id , let take, let skip):
            return Parameters.getCompanyListWater.map(values: [/type, /latitude, /longitude, /category_brand_id, /category_brand_product_id, take, skip])
            
        case .getTokenList(let organisation_id,let category_brand_id ,let skip ,let take):
            return Parameters.getCompanyTokenList.map(values: [/organisation_id, /category_brand_id,/skip, /take])
            
        case .getTokenPurchaseList(let skip,let  take):
            return Parameters.getTokenPurchaseList.map(values: [/skip, /take])
            
            
        case .purchaseEToken(let organisation_coupon_id, let buraq_percentage,     let bottle_returned_value    , let bottle_charge    , let quantity, let  payment_type,let  price, let eToken_quantity, let address, let  address_latitude, let address_longitude ):
            return Parameters.purchaseEToken.map(values: [/organisation_coupon_id,/buraq_percentage,/bottle_returned_value,/bottle_charge, /quantity,/payment_type,/price, /eToken_quantity,/address,/address_latitude,/address_longitude])
            
        case  .EtokenOrderAcceptReject( let order_id, let status):
            return Parameters.EtokenOrderAcceptReject.map(values: [/order_id, /status])
            
        case .addUserCard(let token_id, let cardHolder, let month, let year):
            return Parameters.addUserCard.map(values: [/token_id, /cardHolder, /month, /year])
        case .getUserCardList:
            return Parameters.getUserCardList.map(values:[])
        case .deleteUserCard(let cardId):
            return Parameters.deleteUserCard.map(values:[cardId])
        case .addCard(let token, let defaultCard):
            return Parameters.addCard.map(values: [/token, defaultCard])
            
        }
    }
    
    var method : Alamofire.HTTPMethod {
        switch self {
            
        default:
            return .post
        }
        
    }
    
    var baseURL: String {
        switch self {
        case .homeApi(_) , .checkPickup(_), .requestApi(_)  , .cancelRequest(_) , .onGoingRequest() ,.eTokens(_) , .buyETokens(_) , .rateDriver(_):
            return APIBasePath.basePath + Routes.user  + Routes.SubRoute.service
            
        case .getEtokenDetail(_) , .history(_) , .orderDetails(_):
            return APIBasePath.basePath + Routes.user + Routes.SubRoute.other
            
        case .getCompanyListWater(_), .getTokenList(_), .purchaseEToken(_), .getTokenPurchaseList(_), .EtokenOrderAcceptReject(_):
            return APIBasePath.basePath + Routes.user + Routes.SubRoute.water
            
        case .addUserCard(_), .addCard(_), .getUserCardList, .deleteUserCard(_):
            return APIBasePath.basePath + Routes.user
            
        default:
            return APIBasePath.basePath + Routes.commonRoutes
        }
    }
    
}
