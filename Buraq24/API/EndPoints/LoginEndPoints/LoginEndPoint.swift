//
//  LoginEndPoint.swift
//  Buraq24
//
//  Created by MANINDER on 16/08/18.
//  Copyright © 2018 CodeBrewLabs. All rights reserved.
//

import UIKit
import Alamofire

enum LoginEndpoint {
    case sendOtp(countryCode: String? , phoneNum : String? )
    case verifyOTP(otpCode: String?)
    case addName(name : String?)
     case logOut()
    case updateData(fcmID : String?)
    case eContacts()
    case contactUs(message: String?)
    case editProfile(name : String? )
    case updateNotifications(value : String? )
    
}

//http://192.168.100.45:9006/api-docs/#/User%20Drivers/post_user_service_homeApi
extension LoginEndpoint : Router {
    

    func searchRequest(isImage: Bool, images: [UIImage?]?, isLoaderNeeded: Bool?, header: [String : String], completion: @escaping Completion) -> DataRequest {
        let request = Alamofire.SessionManager.default.request("" , method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: [:]).responseJSON { response in
            print(response)
        }
        return request
    }


    func request(isImage: Bool = false , images: [UIImage?]? = [] , isLoaderNeeded: Bool? = true , header: [String: String] , completion: @escaping Completion ) {
        APIManager.shared.request(with: self, images: images, isLoaderNeeded: isLoaderNeeded, completion: completion, header: header)

    }

    
    var route : String  {
        switch self {
        case .sendOtp(_): return APITypes.sendOtp
        case .verifyOTP(_): return APITypes.verifyOTP
        case .addName(_): return APITypes.addName
        case .logOut() : return APITypes.logOut
        case .updateData(_) : return APITypes.updateData
        case .eContacts() : return APITypes.eContacts
        case .contactUs(_) : return APITypes.contactUs
        case .editProfile(_) : return APITypes.editProfile
        case .updateNotifications(_) : return APITypes.changeNotification
        }
    }

    var parameters: OptionalDictionary {
        return format()
    }

    func format() -> OptionalDictionary {
     
        switch self {
            
        case .sendOtp(let countryCode,  let phoneNumber):
            return Parameters.sendOtp.map(values: [ LanguageCode.English.rawValue , /countryCode , /phoneNumber , Utility.shared.localTimeZoneName , Locations.lat.getLoc() , Locations.longitude.getLoc(), "" , UserDefaultsManager.fcmId, "Ios"])
         case .verifyOTP(let otpCode):
            return Parameters.verifyOTP.map(values: [otpCode])
            
        case .addName(let strName):
            return Parameters.addName.map(values: [/strName])
        
         case .logOut() , .eContacts() :
        return Parameters.logout.map(values: [])
            
        case .updateData(let fcmID):
            return Parameters.updateData.map(values: [Utility.shared.localTimeZoneName , Locations.lat.getLoc() , Locations.longitude.getLoc(), fcmID])
        case .contactUs(let message):
             return Parameters.contactUs.map(values: [ message ])
        case .editProfile(let name  ):
            return Parameters.editProfile.map(values: [ name ])
        
        case .updateNotifications(let changeValue):
            return Parameters.changeNotification.map(values: [/changeValue])
        }
        
    }


    var method : Alamofire.HTTPMethod {
        switch self {

        default:
            return .post
        }
    }

    var baseURL: String {
        switch self {
            
        case .sendOtp(_), .verifyOTP(_), .addName(_) , .editProfile(_) :
            return APIBasePath.basePath + Routes.user
        default:
            return APIBasePath.basePath + Routes.commonRoutes
        }
    }
}


